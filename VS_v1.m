function [Id,Qs,Qd,Vgi,Vsi,Vdi,Y11,Y12,Y21,Y22]=VS_v1(para,Vg,Vs,Vd,f)
%%% Virtual source model, version 1
% todo: include rsf/rdf/cgspar2/cgdpar2
% todo: compare with verilogA implementation
% ---------------------------
% Input:
% Vg   .. gate voltage    (single value)
% Vs   .. source voltage  (single value)
% Vd   .. drain voltage   (single value)
% f    .. frequency       (vector or single value)
% para .. model parameter
% ---------------------------
% output:
% Id   .. current
% Qs   .. charge between source and gate
% Qd   .. charge between drain  and gate
% Vgi  .. internal gate voltage
% Vsi  .. internal source voltage
% Vdi  .. internal drain voltage
% Y11  .. Y-parameter
% Y12  .. Y-parameter
% Y21  .. Y-parameter
% Y22  .. Y-parameter

if (~isfield(para,'rsf'));     para.rsf=0;    end  % finger resistance source
if (~isfield(para,'rdf'));     para.rdf=0;    end  % finger resistance drain
if (~isfield(para,'rsc'));     para.rsc=0;    end  % contact resistance source
if (~isfield(para,'rdc'));     para.rdc=0;    end  % contact resistance drain
if (~isfield(para,'rg'));      para.rg =0;    end  % contact resistance gate
if (~isfield(para,'cgspar'));  para.cgspar=0; end  % parasitic gate source capacitance
if (~isfield(para,'cgdpar'));  para.cgdpar=0; end  % parasitic gate drain capacitance
if (~isfield(para,'cdspar'));  para.cdspar=0; end  % parasitic source drain capacitance
if (~isfield(para,'cgspar2')); para.cgspar2=0;end  % parasitic gate source capacitance 2
if (~isfield(para,'cgdpar2')); para.cgdpar2=0;end  % parasitic gate drain capacitance 2
if (~isfield(para,'type'));    para.type=1;   end  % 1..n-type, -1 p-type
if (~isfield(para,'w'));       para.w=1e-6;   end  % [m] gate width
if (~isfield(para,'lg'));      para.lg=1;     end  % [m] gate length
if (~isfield(para,'mu'));      para.mu=1;     end  % [Vs/m] low field mobility
if (~isfield(para,'vxo'));     para.vxo=1;    end  % [m/s] virtal source velocity
if (~isfield(para,'cinv'));    para.cinv=1;   end  % [F/m^2] inversion capacitance
if (~isfield(para,'vth'));     para.vth=1;    end  % [V] threshold voltage
if (~isfield(para,'alpha'));   para.alpha=1;  end  % fitting factor
if (~isfield(para,'beta'));    para.beta=1;   end  % fitting factor
if (~isfield(para,'dibl'));    para.dibl=0;   end  % fitting factor for DIBL effect
if (~isfield(para,'nss'));     para.nss=1;    end  % fitting factor for Subthreshold swing
if (~isfield(para,'me'));      para.me=1;     end  % effective mass

if (nargin<5); f=0;end

for k = 1:length(Vg)
    for l = 1:length(Vd)
        x0(1) = Vs; %S
        x0(2) = Vd(l); %D

        if para.rsf~=0 || para.rsc~=0 || para.rdf~=0 || para.rdc~=0
            x = fsolve(@(x) contact_resistance(x,Vg(k),Vs,Vd(l),para),x0,...
                optimset('Display','off','TolFun',1e-12,'TolX',1e-12));
        else
            x = x0;
        end

        [Id(k,l),Qs(k,l),Qd(k,l)] = vs_intr(Vg(k),x(1),x(2),para,true);
        Vgi(k,l)  = Vg(k);
        Vsi(k,l)  = x(1);
        Vdi(k,l)  = x(2);

        if max(f)==0
            Y11(k,l)=0;
            Y12(k,l)=0;
            Y21(k,l)=0;
            Y22(k,l)=0;
        else

            [Id,Qs(k,l),Qd(k,l)] = vs_intr(Vg(k),x(1),x(2),para,false);
            Vgi(k,l) = Vg(k);
            Vsi(k,l) = x(1);
            Vdi(k,l) = x(2);

            dv=1e-6;
            [Id1,Qs1,Qd1] = vs_intr(Vg(k)+dv,x(1),x(2),para,false);

            gm  = (Id1-Id)/dv;
            cm  = 0;
            cgs = (Qs1-Qs)/dv;
            cgd = (Qd1-Qd)/dv;

            [Id1] = vs_intr(Vg(k),x(1),x(2)+dv,para,true);
            gds   = (Id1-Id)/dv;
            cds   = 0;

            rs  = para.rsc;
            rd  = para.rdc;
            rg  = para.rg;

            % frequency
            w   = 2*pi*f;
            cgg = cgs+cgd;

            % Y-parameter 
            alpha = cgg*gds + cgd*gm;
            beta  = cgg*cds + cgd*(cgs-cm);
            gamma = rs*rd+rs*rg+rd*rg;
            a     = 1 + rs*(gm + gds) + rd*gds - w^2*gamma*beta;
            b     = rs*(cgs+cds-cm) + rd*(cgd+cds) + rg*cgg + gamma*alpha;

            Y11(k,l) = 1./(a+1i*w*b)*(      1i*w*(cgg    + alpha*(rs+rd)) - w^2*(rs+rd)*beta);
            Y12(k,l) = 1./(a+1i*w*b)*(    - 1i*w*(cgd    + alpha* rs    ) + w^2* rs    *beta);
            Y21(k,l) = 1./(a+1i*w*b)*(gm  - 1i*w*(cgd    + alpha* rs    ) + w^2* rs    *beta);
            Y22(k,l) = 1./(a+1i*w*b)*(gds + 1i*w*(cds+cgd+ alpha*(rs+rg)) - w^2*(rs+rg)*beta);

            Y11(k,l) = Y11(k,l) + 1i*w*(para.cgspar+para.cgdpar);
            Y12(k,l) = Y12(k,l) - 1i*w*(            para.cgdpar);
            Y21(k,l) = Y21(k,l) - 1i*w*(            para.cgdpar);
            Y22(k,l) = Y22(k,l) + 1i*w*(para.cdspar+para.cgdpar);
        end    
    end
end    

end

function F = contact_resistance(x,Vg,Vs,Vd,para)
%%% contact resistance
%    x(1) = VSi    ; %Vsi
%    x(2) = VDi    ; %Vdi
% equations:
%  I:  Vsi -Vs - Rcs*Id                     = 0
%  II: (Vd - Vs) - (Vdi-Vsi) - (Rcs+Rcd)*Id = 0
    
    Id = vs_intr(Vg,x(1),x(2),para,true);
    F = [x(1) - Vs - (para.rsc)*Id,...
        (Vd-Vs)-(x(2)-x(1)) - (para.rsc+para.rdc)*Id];
end


function [I,Qs,Qd] = vs_intr(Vg,Vs,Vd,para,onlycurrent)

    q      = 1.60219e-19;
    Vt     = 1.38066e-23/1.60219e-19*300;

    vgi    = Vg; %Vgi
    vsi    = Vs; %Vsi
    vdi    = Vd; %Vdi
    
    vgsraw = para.type*(vgi-vsi);
    vgdraw = para.type*(vgi-vdi);
    
    if (vgsraw>=vgdraw)
        vdsi = para.type*(vdi-vsi);
        vgsi = vgsraw;
        dir  = 1;
    else
        vdsi = para.type*(vsi-vdi);
        vgsi = vgdraw;
        dir  = -1;
    end
    alpha_vt  = para.alpha*Vt;
    vth       = para.vth-para.dibl*vdsi;
    Ff        = 1./(1+exp((vgsi-(vth-alpha_vt/2))/alpha_vt));
    nss_vt    = para.nss*Vt;
    Qxo       = para.cinv*nss_vt*log(1+exp((vgsi-(vth-alpha_vt*Ff))/nss_vt));
    vdsats    = para.vxo*para.lg/para.mu;                                      
    vdsat     = vdsats*(1-Ff)+Vt*Ff;
    vds_vdsat = vdsi/vdsat;
    Fs        = vds_vdsat/(1+vds_vdsat^para.beta)^(1/para.beta);
    I         = para.w*Qxo*para.vxo*Fs*dir*para.type;
   
    if onlycurrent
        Qs=0;
        Qd=0;
        return
    end
    % channel charge calc
    Qch    = Qxo*para.lg*para.w;
    
    % balllistic charge
    m0     = 9.11e-31;
    kqb    = 2*q*vdsi/(para.me*m0)/(para.vxo*para.vxo);  % 3.7566e12 = 9*q*acc^2*Ep^2/h_bar^2
    sq_kqb = sqrt(kqb);
    qsB    = Qch*(asinh(sq_kqb)/sq_kqb-(sqrt(kqb+1)-1)/kqb);
    qdB    = Qch*((sqrt(kqb+1)-1)/kqb);                    
    
    % drift diffusion charge
    vgt    = Qxo/para.cinv;
    vdsatq = sqrt(Ff*alpha_vt^2+vgt^2);
    udsi   = vdsi/vdsatq;
    Fsatq  = udsi/(power(1+power(udsi,para.beta),1/para.beta));  
    eta    = 1-Fsatq;                                                     
    eta2   = eta*eta;
    eta3   = eta2*eta;
    qsDD   = Qch*(6+12*eta+8*eta2+4*eta3)/(15*(1+2*eta+eta2));    
    qdDD   = Qch*(4+8*eta+12*eta2+6*eta3)/(15*(1+2*eta+eta2));
    
    % entire charge for source and drain
    Fsatq2 = Fsatq*Fsatq;
    Qs = (qsDD*(1-Fsatq2) + qsB*Fsatq2);
    Qd = (qdDD*(1-Fsatq2) + qdB*Fsatq2);
    
end

