function [Id,Qs,Qd,Vgi,Vsi,Vdi,Y11,Y12,Y21,Y22]=VS_v3(para,Vg,Vs,Vd,f)
%%% Virtual source model, version 1
% todo: include rsf/rdf/cgspar2/cgdpar2
% todo: compare with verilogA implementation
% ---------------------------
% Input:
% Vg   .. gate voltage    (single value)
% Vs   .. source voltage  (single value)
% Vd   .. drain voltage   (single value)
% f    .. frequency       (vector or single value)
% para .. model parameter
% ---------------------------
% output:
% Id   .. current
% Qs   .. charge between source and gate
% Qd   .. charge between drain  and gate
% Vgi  .. internal gate voltage
% Vsi  .. internal source voltage
% Vdi  .. internal drain voltage
% Y11  .. Y-parameter
% Y12  .. Y-parameter
% Y21  .. Y-parameter
% Y22  .. Y-parameter

if (~isfield(para,'rsf'));     para.rsf=0;         end  % [Ohm]finger resistance source
if (~isfield(para,'rdf'));     para.rdf=0;         end  % [Ohm]finger resistance drain
if (~isfield(para,'rsc'));     para.rsc=0;         end  % [Ohm*um]contact resistance source
if (~isfield(para,'rdc'));     para.rdc=0;         end  % [Ohm*um]contact resistance drain
if (~isfield(para,'rg'));      para.rg =0;           end  % [Ohm/um] contact resistance gate
if (~isfield(para,'cgspar'));  para.cgspar=0;    end  % parasitic gate source capacitance
if (~isfield(para,'cgdpar'));  para.cgdpar=0;    end  % parasitic gate drain capacitance
if (~isfield(para,'cdspar'));  para.cdspar=0;    end  % parasitic source drain capacitance
if (~isfield(para,'cgspar2')); para.cgspar2=0;  end  % parasitic gate source capacitance 2
if (~isfield(para,'cgdpar2')); para.cgdpar2=0; end  % parasitic gate drain capacitance 2
if (~isfield(para,'type'));    para.type=1;       end  % 1..n-type, -1 p-type
if (~isfield(para,'geomod'));  para.geomod=1;   end  % 0..Bulk, 1..Top Gate, 2..GAA
if (~isfield(para,'w'));       para.w=0;           end  % [m] gate width
if (~isfield(para,'lch'));     para.lch=1;         end  % [m] channel length
if (~isfield(para,'dens') && para.geomod>0);      para.dens=1;       end  % [1/um] tube/wire density
if (~isfield(para,'u') && para.geomod==3 );       para.u=1;       end  % [m] gate length per Fin 
if (~isfield(para,'r_semi') && (para.geomod==1 || para.geomod==2));       para.r_semi=1; end  % [m] tube/wire circumferenc
if (~isfield(para,'lambda0')); para.lambda0=1;  end  % [m] mean free path
if (~isfield(para,'cq'));      para.cq=1;            end  % [F/m^2] qunatum capacitance
if (~isfield(para,'tox'));     para.tox=1;          end  % [m] gate oxide thickness
if (~isfield(para,'eps_ox'));  para.eps_ox=1;    end  % [] gate oxide relative permittivity
if (~isfield(para,'vth0'));    para.vth=1;      end  % [V] threshold voltage
if (~isfield(para,'dvthq'));   para.dvthq=0;      end  % [V] threshold voltage
if (~isfield(para,'alpha'));   para.alpha=3.5;    end  % fitting factor
if (~isfield(para,'beta'));    para.beta=1.8;      end  % fitting factor
if (~isfield(para,'beta_ch')); para.beta_ch=-1;end  % fitting factor
if (~isfield(para,'dibl'));    para.dibl=1;       end  % fitting factor for DIBL effect
if (~isfield(para,'nss'));     para.nss=1;        end  % fitting factor for Subthreshold swing
if (~isfield(para,'meff'));    para.meff=1;        end  % effective mass
if (~isfield(para,'fd'));      para.fd=1;         end  % charge factor
if (~isfield(para,'lfl'));     para.lfl=1;        end  % low field length
if (~isfield(para,'va'));      para.va=0;          end  % Early voltage
if (~isfield(para,'gamma'));   para.gamma=1;      end  % mobility reduction parameter
if (~isfield(para,'rmet'));    para.rmet=0;      end  % [Ohm*um] metallic tubes resistance model
if (~isfield(para,'vinjb'));   para.vinjb=1;      end  % [m/s] ballistic injection velocity

if (~isfield(para,'t_e'));     para.t_e=300;       end  % [K] metallic tubes resistance model
if (~isfield(para,'vthg'));    para.vthg=-5;  end  % s-shape output characteristic (hidden feature)
if (~isfield(para,'nd'));      para.nd=0;   end  % s-shape output characteristic
if (~isfield(para,'qcl'));     para.qcl=1;   end  % quantum capacitance limit

if (nargin<5); f=0;end

for k = 1:length(Vg)
    for l = 1: length(Vd)
        x0(1) = Vs; %S
        x0(2) = Vd(l); %D

        if para.rsf~=0 || para.rsc~=0 || para.rdf~=0 || para.rdc~=0
            x = fsolve(@(x) contact_resistance(x,Vg(k),Vs,Vd(l),para),x0,...
                optimset('Display','off','TolFun',1e-12,'TolX',1e-12));
        else
            x = x0;
        end

        [Id(k,l),Qs(k,l),Qd(k,l)] = vs_intr(Vg(k),x(1),x(2),para,false);
        Vgi  = Vg(k);
        Vsi  = x(1);
        Vdi  = x(2);

        if max(f)==0
            Y11=0;
            Y12=0;
            Y21=0;
            Y22=0;
        else

            [Id(k,l),Qs(k,l),Qd(k,l)] = vs_intr(Vg(k),x(1),x(2),para,false);
            Vgi = Vg(k);
            Vsi = x(1);
            Vdi = x(2);

            dv=1e-6;
            [Id1,Qs1,Qd1] = vs_intr(Vg(k)+dv,x(1),x(2),para,false);

            gm  = (Id1-Id(k,l))/dv;
            cm  = 0;
            cgs = (Qs1-Qs(k,l))/dv;
            cgd = (Qd1-Qd(k,l))/dv;

            [Id1] = vs_intr(Vg(k),x(1),x(2)+dv,para,true);
            gds   = (Id1-Id(k,l))/dv;
            cds   = 0;

            if (para.geomod==0)
              width = para.w*1e6;
            elseif (para.geomod==3)
              width = para.dens*para.w*1e6 * para.u*1e6;  % density (per um) times total width (m) times effective gate with per Fin  
            else 
              width = para.dens*para.w*1e6;  % density (per um) times total width (m)
            end   
        
            rs  = para.rsc/width;
            rd  = para.rdc/width;
            rg  = para.rg;

            % frequency
            w   = 2*pi*f;
            cgg = cgs+cgd;

            % Y-parameter 
            alpha = cgg*gds + cgd*gm;
            beta  = cgg*cds + cgd*(cgs-cm);
            gamma = rs*rd+rs*rg+rd*rg;
            a     = 1 + rs*(gm + gds) + rd*gds - w^2*gamma*beta;
            b     = rs*(cgs+cds-cm) + rd*(cgd+cds) + rg*cgg + gamma*alpha;

            Y11(k,l) = 1./(a+1i*w*b)*(      1i*w*(cgg    + alpha*(rs+rd)) - w^2*(rs+rd)*beta);
            Y12(k,l) = 1./(a+1i*w*b)*(    - 1i*w*(cgd    + alpha* rs    ) + w^2* rs    *beta);
            Y21(k,l) = 1./(a+1i*w*b)*(gm  - 1i*w*(cgd    + alpha* rs    ) + w^2* rs    *beta);
            Y22(k,l) = 1./(a+1i*w*b)*(gds + 1i*w*(cds+cgd+ alpha*(rs+rg)) - w^2*(rs+rg)*beta);

            Y11(k,l) = Y11(k,l) + 1i*w*(para.cgspar+para.cgdpar)*width;
            Y12(k,l) = Y12(k,l) - 1i*w*(            para.cgdpar)*width;
            Y21(k,l) = Y21(k,l) - 1i*w*(            para.cgdpar)*width;
            Y22(k,l) = Y22(k,l) + 1i*w*(para.cdspar+para.cgdpar)*width;
        end
    end
end    

end

function F = contact_resistance(x,Vg,Vs,Vd,para)
%%% contact resistance
%    x(1) = VSi    ; %Vsi
%    x(2) = VDi    ; %Vdi
% equations:
%  I:  Vsi -Vs - Rcs*Id                     = 0
%  II: (Vd - Vs) - (Vdi-Vsi) - (Rcs+Rcd)*Id = 0
    if (para.geomod==0)
        width = para.w*1e6;
    else if (para.geomod==3)
            width = para.dens*para.w*1e6 * para.u*1e6;  % density (per um) times total width (m) times effective gate with per Fin  
        else 
            width = para.dens*para.w*1e6;  % density (per um) times total width (m)
        end    
    end

    Id = vs_intr(Vg,x(1),x(2),para,true);
    F = [x(1) - Vs - (para.rsc/width+para.rsf*para.w*1e6)*Id,...
        (Vd-Vs)-(x(2)-x(1)) - ((para.rsc+para.rdc)/width+(para.rsf+para.rdf)*para.w*1e6)*Id];
end


function [I,Qs,Qd] = vs_intr(Vg,Vs,Vd,para,onlycurrent)
    
    q      = 1.60219e-19;
    eps0   = 8.8542e-12;
    kB     = 1.38066e-23;
    T      = 300;
    Vt     = kB/q*T;  

    vgi    = Vg; %Vgi
    vsi    = Vs; %Vsi
    vdi    = Vd; %Vdi
    
    % width calc
    if (para.geomod==0) % Bulk Material with classic capacitance calculation
        width = para.w;
        scal_factor  = 1e6;
        cox   = para.eps_ox*eps0 / para.tox;
        p     = 0;
    end
    if (para.geomod==1) % Nanowire/tube to planar gate
        width = para.dens*para.w*1e6;  % density (per um) times total width (m)
        scal_factor  = 1;
        spacing = 1e-6/para.dens;
        h     = para.r_semi+para.tox;
        cox   = 2*pi*para.eps_ox*eps0/(log(spacing/(pi*para.r_semi) * sinh(pi*(h+sqrt(h^2-rcnt^2))/spacing))); % adjusted Hanson formula
        p     = para.qcl *para.vinjb/10e5;
    end
    if (para.geomod==2) % Nanowire/tube cylindrical gate
        width = para.dens*para.w*1e6;  % density (per um) times total width (m)
        scal_factor  = 1;
        cox   = 2*pi*para.eps_ox*eps0/(log((para.r_semi+para.tox)/para.r_semi)); % 
        p     = para.qcl *para.vinjb/10e5; 
    end
    if (para.geomod==3) % Multi-gate structures (DG, FinFET)
        width = para.u*para.dens*para.w*1e6;  % [para.u --> Surface width] (um) times density (per um) times total width (m)
        scal_factor  = 1e6;
        cox   = para.eps_ox*eps0/para.tox;    % normal plate capacitance
        p     = 0;
    end
    
    % mu scaling
    v_t     = para.vinjb;
    mu_eff  = v_t * para.lambda0 * q / (2*kB*para.t_e);  % scattering
    mu_B    = v_t * para.lch * q ./ (2*kB*para.t_e);     % ballistic
    mu_app  = mu_eff .* mu_B ./ (mu_eff+mu_B);
    
    % vx0 scaling
    Dn   = v_t * para.lambda0 / 2;
    vinj = v_t * (Dn/para.lfl) / (v_t + (Dn/para.lfl));
    
    vgsraw = para.type*(vgi-vsi);
    vgdraw = para.type*(vgi-vdi);
    
    if (vgsraw>=vgdraw)
        vdsi = para.type*(vdi-vsi);
        vgsi = vgsraw;
        dir  = 1;
    else
        vdsi = para.type*(vsi-vdi);
        vgsi = vgdraw;
        dir  = -1;
    end
    alpha_vt  = para.alpha*Vt;
    vth       = para.type*para.vth0-para.dibl*vdsi;
    nss_vt    = para.nss*Vt;
    Ff        = 1./(1+exp((vgsi-(vth-alpha_vt*0.5))/alpha_vt));
        
    % channel charge
    cinv      = (cox*para.cq)/(cox+para.cq); 
    
    % mobility reduction for high voltages
    if (para.gamma>0)
        vgeff    = nss_vt*log(1+exp((vgsi-(vth-alpha_vt*Ff))/nss_vt));
        mob_red  = 1 - exp(-1./(para.gamma*vgeff));
    else
        mob_red  = 1;
    end
    
    Qxo     = cinv * nss_vt*log(1+exp(mob_red*(vgsi -(vth-alpha_vt*Ff))/nss_vt));
    vdsats  = vinj * para.lch/mu_app; 
    vdsatq  = p * Qxo/cinv;
    vdsat   = sqrt((vdsats*(1-Ff)+Vt*Ff)^2 + (vdsatq*(1-Ff)+Vt*Ff)^2);
%     vdsat   = vdsats*(1-Ff)+Vt*Ff;
    if (para.va~=0)
        lp  = 1+(vdsi-vdsat)/para.va; % Early effect
    else 
        lp  = 1;
    end   
    vds_vdsat = vdsi/vdsat;
    Fs        = vds_vdsat./(1+vds_vdsat^para.beta)^(1/para.beta) .* lp;
    if (para.nd~=0)
        Fs2       = min([1e9,exp((vdsi-para.vthg)/(Vt*para.nd))])/(1+min([1e9,exp((vdsi-para.vthg)/(Vt*para.nd))]));
    else 
        Fs2 = 1;
    end    
        
    if (para.rmet>0) 
        Imet = vdsi./(para.rmet/width/scal_factor);
    else
        Imet = 0;
    end 
    
    I      = width.*Qxo.*vinj.*Fs.*Fs2.*dir.*para.type + Imet*para.type; % 
   
    if onlycurrent
        Qs = 0;
        Qd = 0;
        return
    end
    
    % channel charge calc
    Ff    = 1./(1+exp((vgsi-(vth-alpha_vt/2)+para.dvthq)/alpha_vt));
    Qch   = para.lch*width*cinv*nss_vt*log(1+exp(mob_red*(vgsi-(vth-alpha_vt*Ff)+para.dvthq)/nss_vt));
    
    % balllistic charge
    m0     = 9.11e-31;
    if (vdsi==0) 
      qsB    = 0.5*Qch;
      qdB    = 0.5*Qch;
    else
      kqb    = 2*q*vdsi./(para.meff*m0)./(vinj.*vinj);  % 3.7566e12 = 9*q*acc^2*Ep^2/h_bar^2
      sq_kqb = sqrt(kqb);
      qsB    = Qch.*(asinh(sq_kqb)/sq_kqb-(sqrt(kqb+1)-1)/kqb);
      qdB    = Qch.*((sqrt(kqb+1)-1)/kqb);                    
    end
    
    % drift diffusion charge
    vgt    = Qxo/cinv;
    vdsatq = sqrt(Ff*alpha_vt^2+vgt^2);
    udsi   = para.fd*vdsi./vdsatq;
    if para.beta_ch<0
      beta_ch = para.beta;
    else
      beta_ch = para.beta_ch;
    end
    Fsatq  = udsi./(power(1+power(udsi,beta_ch),1/beta_ch));
    eta    = 1-Fsatq;
    eta2   = eta.*eta;
    eta3   = eta2.*eta;
    qsDD   = Qch.*(6+12*eta+8*eta2+4*eta3)./(15*(1+2*eta+eta2));    
    qdDD   = Qch.*(4+8*eta+12*eta2+6*eta3)./(15*(1+2*eta+eta2));
    
    % entire charge for source and drain
    Fsatq2 = Fsatq.*Fsatq;
    Qs = (qsDD.*(1-Fsatq2) + qsB.*Fsatq2);
    Qd = (qdDD.*(1-Fsatq2) + qdB.*Fsatq2);
    
end

