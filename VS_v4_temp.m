function [Id,Qs,Qd,YP,SP,ZP,Vgi,Vsi,Vdi]=VS_v4(para,Vg,Vs,Vd,f)
%%% Virtual source model, version 1
% todo: include rsf/rdf/cgspar2/cgdpar2
% todo: compare with verilogA implementation
% ---------------------------
% Input:
% Vg   .. gate voltage    (single value)
% Vs   .. source voltage  (single value)
% Vd   .. drain voltage   (single value)
% f    .. frequency       (vector or single value)
% para .. model parameter
% ---------------------------
% output:
% Id   .. current
% Qs   .. charge between source and gate
% Qd   .. charge between drain  and gate
% Vgi  .. internal gate voltage
% Vsi  .. internal source voltage
% Vdi  .. internal drain voltage
% Y11  .. Y-parameter
% Y12  .. Y-parameter
% Y21  .. Y-parameter
% Y22  .. Y-parameter

if (~isfield(para,'rsf'));     para.rsf=0;         end  % [Ohm]finger resistance source
if (~isfield(para,'rdf'));     para.rdf=0;         end  % [Ohm]finger resistance drain
if (~isfield(para,'rsc'));     para.rsc=0;         end  % [Ohm*um]contact resistance source
if (~isfield(para,'rdc'));     para.rdc=0;         end  % [Ohm*um]contact resistance drain
if (~isfield(para,'rsc_t'));   para.rsc_t=0;       end  % [Ohm]contact resistance per tube source
if (~isfield(para,'rdc_t'));   para.rdc_t=0;       end  % [Ohm]contact resistance per tube drain
if (~isfield(para,'rg'));      para.rg =0;         end  % [Ohm/um] contact resistance gate
if (~isfield(para,'cgspar'));  para.cgspar=0;      end  % [F/um] parasitic gate source capacitance
if (~isfield(para,'cgdpar'));  para.cgdpar=0;      end  % [F/um] parasitic gate drain capacitance
if (~isfield(para,'cdspar'));  para.cdspar=0;      end  % [F/um] parasitic source drain capacitance
if (~isfield(para,'cgspar2')); para.cgspar2=0;     end  % [F/um] parasitic gate source capacitance 2
if (~isfield(para,'cgdpar2')); para.cgdpar2=0;     end  % [F/um] parasitic gate drain capacitance 2
if (~isfield(para,'type'));    para.type=1;        end  % 1..n-type, -1 p-type
if (~isfield(para,'geomod'));  para.geomod=1;      end  % 0..Bulk, 1..Top Gate, 2..GAA
if (~isfield(para,'w'));       para.w=0;           end  % [m] gate wg
if (~isfield(para,'lch'));     para.lch=1;         end  % [m] channel length
if (~isfield(para,'dens'));    para.dens=1;        end  % [1/um] tube/wire density
if (~isfield(para,'u'));       para.u=1;           end  % [m] effective gate width per Fin 
if (~isfield(para,'r_semi'));  para.r_semi=1;      end  % [m] tube/wire circumferenc
if (~isfield(para,'lambda0')); para.lambda0=1;     end  % [m] mean free path
if (~isfield(para,'cq'));      para.cq=1;          end  % [F/m^2] qunatum capacitance
if (~isfield(para,'cq_t'));    para.cq_t=1;        end  % [F/m] qunatum capacitance for 1D channels
if (~isfield(para,'tox'));     para.tox=1;         end  % [m] gate oxide thickness
if (~isfield(para,'eps_ox'));  para.eps_ox=1;      end  % [-] gate oxide relative permittivity
if (~isfield(para,'vth0'));    para.vth0=1;        end  % [V] threshold voltage
if (~isfield(para,'dvthq'));   para.dvthq=0;       end  % [V] threshold voltage
if (~isfield(para,'alpha'));   para.alpha=3.5;     end  % [-] fitting factor
if (~isfield(para,'beta'));    para.beta=1.8;      end  % [-] fitting factor
if (~isfield(para,'beta_ch')); para.beta_ch=-1;    end  % [-] fitting factor
if (~isfield(para,'dibl'));    para.dibl=1;        end  % [-] fitting factor for DIBL effect
if (~isfield(para,'nss'));     para.nss=1;         end  % [-] fitting factor for Subthreshold swing
if (~isfield(para,'meff'));    para.meff=1;        end  % [-] effective mass
if (~isfield(para,'fb'));      para.fb=1;          end  % [-] fitting factor
if (~isfield(para,'lfl'));     para.lfl=1;         end  % [m] low field length
if (~isfield(para,'va'));      para.va=0;          end  % [V] Early voltage
if (~isfield(para,'gamma'));   para.gamma=1;       end  % [-] mobility reduction parameter
if (~isfield(para,'vball'));   para.vball=1;       end  % [m/s] ballistic injection velocity
if (~isfield(para,'dvthq'));   para.dvthq=0;       end

if (~isfield(para,'temp'));    para.temp=300;      end  % [K] semicondcutor temperatur
if (~isfield(para,'vthd'));    para.vthd=-5;       end  % s-shape output characteristic (hidden feature)
if (~isfield(para,'nd'));      para.nd=0;          end  % s-shape output characteristic
if (~isfield(para,'pqcl'));    para.pqcl=0;        end  % quantum capacitance limit

if (~isfield(para,'rmet'));    para.rmet=0;        end  % [Ohm*m] metallic tubes resistance Rmet,cnt/met_tube density
if (~isfield(para,'amt0'));    para.amt0=0;        end  % [-] saturation parameter of metallic CNTs

if (nargin<5); f=0;end

% normalize:
para.dens    = para.dens   *1e6;  % tube/wire/fin density from /um to /m
para.rsc     = para.rsc    *1e-6; % contact resistance changed from Ohm*um to Ohm*m
para.rdc     = para.rdc    *1e-6; % contact resistance changed from Ohm*um to Ohm*m
para.rsf     = para.rsf    *1e6;  % finger resistance changed from Ohm/um to Ohm/m
para.rdf     = para.rdf    *1e6;  % finger resistance changed from Ohm/um to Ohm/m
para.cgspar  = para.cgspar *1e6;  % parasitic capacitance from F/um to F/m
para.cgdpar  = para.cgdpar *1e6;  % parasitic capacitance from F/um to F/m
para.cdspar  = para.cdspar *1e6;  % parasitic capacitance from F/um to F/m


for k = 1:length(Vg)
    for l = 1: length(Vd)
        x0(1) = Vs; %S
        x0(2) = Vd(l); %D

        if (para.geomod==0 && (para.rsc~=0 || para.rdc~=0)) || (para.geomod>0 && (para.rsc_t~=0 || para.rdc_t~=0)) || para.rsf~=0 || para.rdf~=0
            x = fsolve(@(x) contact_resistance(x,Vg(k),Vs,Vd(l),para),x0,...
                optimset('Display','off','TolFun',1e-12,'TolX',1e-12));
        else
            x = x0;
        end

        [Id(k,l),Qs(k,l),Qd(k,l)] = vs_intr(Vg(k),x(1),x(2),para,false);
        Vgi  = Vg(k);
        Vsi  = x(1);
        Vdi  = x(2);

        if max(f)==0
            Y11=0;
            Y12=0;
            Y21=0;
            Y22=0;
        else
            for fIndex = 1:length(f)
                
                w=1i.*2.*pi.*f(fIndex);

                deltaV=1e-3;
                [Id1,Qs1,Qd1] = vs_intr(Vg(k)+deltaV,x(1),x(2),para,false);

                biasdata_vgs1=Vg(k)-deltaV/2;
                biasdata_vgs2=Vg(k)+deltaV/2;
                biasdata_vds1=x(2)-deltaV/2;
                biasdata_vds2=x(2)+deltaV/2;

                [IS_vgs1]=vs_intr(biasdata_vgs1,x(1),x(2),para,false);
                [IS_vgs2]=vs_intr(biasdata_vgs2,x(1),x(2),para,false);
                [IS_vds1]=vs_intr(Vg(k),x(1),biasdata_vds1,para,false);
                [IS_vds2]=vs_intr(Vg(k),x(1),biasdata_vds2,para,false);
                [~,Q_s_vgs1,Q_d_vgs1]=vs_intr(biasdata_vgs1,x(1),x(2),para,false);
                [~,Q_s_vgs2,Q_d_vgs2]=vs_intr(biasdata_vgs2,x(1),x(2),para,false);
                [~,Q_s_vds1,Q_d_vds1]=vs_intr(Vg(k),x(1),biasdata_vds1,para,false);
                [~,Q_s_vds2,Q_d_vds2]=vs_intr(Vg(k),x(1),biasdata_vds2,para,false);

                gm=(IS_vgs2-IS_vgs1)/deltaV;
                gds=(IS_vds2-IS_vds1)/deltaV;

                %dQs/dVgs
                dqsdvgs=abs((Q_s_vgs2-Q_s_vgs1)/deltaV);
                %dQs/dVds
            %     dqsdvds=abs((Q_s_vds2-Q_s_vds1)/deltaV);
                dqsdvds=0;
                %dQd/dVgs
                dqddvgs=abs((Q_d_vgs2-Q_d_vgs1)/deltaV);
                %dQd/dVds
                dqddvds=abs((Q_d_vds2-Q_d_vds1)/deltaV);

                yint(1,1)=0+w*(dqsdvgs+dqddvgs);
                yint(1,2)=0-w*(dqddvds+dqsdvds);
                yint(2,1)=gm-w*dqddvgs;
                yint(2,2)=gds+w*dqddvds;

                zint=y2z(yint);

                % add contact resistances %
                if (para.geomod==0)
                  rs  = para.rsc/para.w;
                  rd  = para.rdc/para.w;
                else
                  rs  = para.rsc_t/(para.dens*para.w);
                  rd  = para.rdc_t/(para.dens*para.w);  
                end 
                ze1(1,1)=zint(1,1)+rs;
                ze1(1,2)=zint(1,2)+rs;
                ze1(2,1)=zint(2,1)+rs;
                ze1(2,2)=zint(2,2)+rs+rd;

                ye1=z2y(ze1);

                ye2=ye1+[w*(para.cgspar+para.cgdpar)*para.w,-w*para.cgdpar*para.w;-w*para.cgdpar*para.w,w*para.cgdpar*para.w];

                Rsf = para.rsf;
                Rdf = para.rdf;
                Rg  = para.rg;

                z=y2z(ye2)+[Rsf+Rg,Rsf;Rsf,Rsf+Rdf];

                y=z2y(z);
                Y11=y(1,1)+w*(para.cgspar2+para.cgdpar2);
                Y12=y(1,2)-w*para.cgdpar2;
                Y21=y(2,1)-w*para.cgdpar2;
                Y22=y(2,2)+w*(para.cdspar*para.w+para.cgdpar2);
                zel=y2z(y);
                Z11=zel(1,1);
                Z12=zel(1,2);
                Z21=zel(2,1);
                Z22=zel(2,2);
                s=y2s(y);
                S11=s(1,1);
                S12=s(1,2);
                S21=s(2,1);
                S22=s(2,2);

                YP{l}.freq(1,fIndex) = f(fIndex);
                YP{l}.Y11(k,fIndex) = Y11;
                YP{l}.Y12(k,fIndex) = Y12;
                YP{l}.Y21(k,fIndex) = Y21;
                YP{l}.Y22(k,fIndex) = Y22;
                ZP{l}.freq(1,fIndex) = f(fIndex);
                ZP{l}.Z11(k,fIndex) = Z11;
                ZP{l}.Z12(k,fIndex) = Z12;
                ZP{l}.Z21(k,fIndex) = Z21;
                ZP{l}.Z22(k,fIndex) = Z22;
                SP{l}.freq(1,fIndex) = f(fIndex);
                SP{l}.S11(k,fIndex) = S11;
                SP{l}.S12(k,fIndex) = S12;
                SP{l}.S21(k,fIndex) = S21;
                SP{l}.S22(k,fIndex) = S22;
            end


%             gm  = (Id1-Id(k,l))/dv;
%             cm  = 0;
%             cgs = (Qs1-Qs(k,l))/dv;
%             cgd = (Qd1-Qd(k,l))/dv;
% 
%             [Id1] = vs_intr(Vg(k),x(1),x(2)+dv,para,true);
%             gds   = (Id1-Id(k,l))/dv;
%             cds   = 0;
% 
%             if (para.geomod==0)
%               rs  = para.rsc/para.w;
%               rd  = para.rdc/para.w;
%             
%             else
%               rs  = para.rsc_t/(para.dens*para.w);
%               rd  = para.rdc_t/(para.dens*para.w);
%               
%             end   
%         
%             rg  = para.rg;
% 
%             % frequency
%             w   = 2*pi*f;
%             cgg = cgs+cgd;
% 
%             % Y-parameter 
%             alpha = cgg*gds + cgd*gm;
%             beta  = cgg*cds + cgd*(cgs-cm);
%             gamma = rs*rd+rs*rg+rd*rg;
%             a     = 1 + rs*(gm + gds) + rd*gds - w^2*gamma*beta;
%             b     = rs*(cgs+cds-cm) + rd*(cgd+cds) + rg*cgg + gamma*alpha;
% 
%             Y11(k,l) = 1./(a+1i*w*b)*(      1i*w*(cgg    + alpha*(rs+rd)) - w^2*(rs+rd)*beta);
%             Y12(k,l) = 1./(a+1i*w*b)*(    - 1i*w*(cgd    + alpha* rs    ) + w^2* rs    *beta);
%             Y21(k,l) = 1./(a+1i*w*b)*(gm  - 1i*w*(cgd    + alpha* rs    ) + w^2* rs    *beta);
%             Y22(k,l) = 1./(a+1i*w*b)*(gds + 1i*w*(cds+cgd+ alpha*(rs+rg)) - w^2*(rs+rg)*beta);
% 
%             Y11(k,l) = Y11(k,l) + 1i*w*(para.cgspar+para.cgdpar)*para.w;
%             Y12(k,l) = Y12(k,l) - 1i*w*(            para.cgdpar)*para.w;
%             Y21(k,l) = Y21(k,l) - 1i*w*(            para.cgdpar)*para.w;
%             Y22(k,l) = Y22(k,l) + 1i*w*(para.cdspar+para.cgdpar)*para.w;
        end
    end
end    

end

function F = contact_resistance(x,Vg,Vs,Vd,para)
%%% contact resistance
%    x(1) = VSi    ; %Vsi
%    x(2) = VDi    ; %Vdi
% equations:
%  I:  Vsi -Vs - Rcs*Id  = 0
%  II: Vd  -Vdi - Rcd*Id = 0

    if (para.geomod==0)
        % contact resistance in Ohm*m
        rsc = para.rsc/para.w;
        rdc = para.rdc/para.w;
    else
        % contact resistance in Ohm/tube
        rsc = para.rsc_t/(para.dens*para.w);
        rdc = para.rdc_t/(para.dens*para.w);
    end
    rs = rsc + para.rsf*para.w;
    rd = rdc + para.rdf*para.w;
    
    
    Id = vs_intr(Vg,x(1),x(2),para,true);
    F  = [x(1) - Vs - rs*Id,...
          Vd - x(2) - rd*Id];
end


function [I,Qs,Qd] = vs_intr(Vg,Vs,Vd,para,onlycurrent)
    
    q      = 1.60219e-19;
    eps0   = 8.8542e-12;
    m0     = 9.11e-31;
    kB     = 1.38066e-23;
    T      = para.temp;
    Vt     = kB/q*T;  

    vgi    = Vg; %Vgi
    vsi    = Vs; %Vsi
    vdi    = Vd; %Vdi
    
    % channel capacitance
    if (para.geomod==0) % Bulk Material with classic capacitance calculation
        cox = para.eps_ox*eps0 / para.tox;
        cq  = para.cq;

    elseif (para.geomod==1) % Nanowire/tube to planar gate
        pt  = para.dens;  % tubes/wires per m
        h   = para.r_semi+para.tox;
        cox = 2*pi*para.eps_ox*eps0*pt/log(sinh(pi*pt*(h+sqrt(h^2-para.r_semi^2)))/(pi*pt*para.r_semi)); % adjusted Hanson formula
        cq  = pt*para.cq_t;

    elseif (para.geomod==2) % Nanowire/tube cylindrical gate
        pt  = para.dens;  % tubes/wires per m
        h   = para.r_semi+para.tox;
        cox = 2*pi*para.eps_ox*eps0*pt/(log(h/para.r_semi)); % 
        cq  = pt*para.cq_t;
        
    elseif (para.geomod==3) % Multi-gate structures (DG, FinFET)
        pt  = para.dens;  % tubes/wires per m
        cox = para.eps_ox*eps0*para.u*pt/para.tox;    % normal plate capacitance
        cq  = pt*para.cq_t;
        
    end
    Cch = (cox*cq)/(cox+cq); 
    
    % mu scaling
    mu_eff  = para.vball * para.lambda0 / (2*Vt); % scattering
    mu_ball = para.vball * para.lch     / (2*Vt); % ballistic
    mu_app  = mu_eff .* mu_ball ./ (mu_eff+mu_ball);
    
    % vx0 scaling
    vx   = para.vball * para.lambda0 / (para.lambda0 + 2*para.lfl);
    
    % switch between n/p type
    vgsraw = para.type*(vgi-vsi);
    vgdraw = para.type*(vgi-vdi);
    if (vgsraw>=vgdraw)
        vdsi = para.type*(vdi-vsi);
        vgsi = vgsraw;
        dir  = 1;
    else
        vdsi = para.type*(vsi-vdi);
        vgsi = vgdraw;
        dir  = -1;
    end
    
    alpha_vt  = para.alpha*Vt;
    vth       = para.type*para.vth0 - para.dibl*vdsi;
    nss_vt    = para.nss*Vt;
    Ff        = 1./(1+exp((vgsi-(vth-alpha_vt*0.5))/alpha_vt));
        
    % mobility reduction for high voltages
    if (para.gamma>0)
        vgeff    = nss_vt*log(1+exp((vgsi-(vth-alpha_vt*Ff))/nss_vt));
        f_deg  = 1 - exp(-1./(para.gamma*vgeff));
    else
        f_deg  = 1;
    end
    
    Qxo     = Cch * nss_vt*log(1+exp(f_deg*(vgsi -(vth-alpha_vt*Ff))/nss_vt));
    vdsats  = vx * para.lch/mu_app; 
    vdsatq  = para.pqcl * Qxo/Cch;
    vdsat   = (vdsats+vdsatq)*(1-Ff)+Vt*Ff;
    
    if (para.va~=0)
        Fa = 1+Vt*log(1+exp((vdsi-vdsat)/Vt))/para.va; % Early effect
    else 
        Fa = 1;
    end
    
    vds_vdsat = vdsi/vdsat;
    Fs        = vds_vdsat./(1+vds_vdsat^para.beta)^(1/para.beta);
        
    if (para.nd~=0)
        Fd = 1/(1+exp(-(vdsi-para.vthd)/(Vt*para.nd)));
    else 
        Fd = 1;
    end  
    
    I = para.w.*Qxo.*vx.*Fs.*Fa.*Fd.*dir.*para.type; % 


    if (para.rmet>0)
        rm = para.rmet + para.amt0*sqrt(vdsi.^2 + Vt^2);
        I = I + vdsi./(rm/para.w);
    end
    
    if onlycurrent
        Qs = 0;
        Qd = 0;
        return
    end
    
    % channel charge calc
%     Qch   = para.lch*para.w*Qxo;
    Qch   = para.lch*para.w*Cch*nss_vt*log(1+exp(f_deg*(vgsi-(vth-alpha_vt*Ff)+para.dvthq)/nss_vt));
%     Qch   = para.lch*para.w*Cch*nss_vt*log(1+exp(1*(vgsi-(vth-alpha_vt*Ff)+para.dvthq)/nss_vt));

    % balllistic charge
    if (vdsi==0) 
      qsB    = 0.5*Qch;
      qdB    = 0.5*Qch;
    else
      kqb    = 2*q*vdsi./(para.meff*m0)./(vx.*vx);  % 3.7566e12 = 9*q*acc^2*Ep^2/h_bar^2
      sq_kqb = sqrt(kqb);
      qsB    = Qch.*(asinh(sq_kqb)/sq_kqb-(sqrt(kqb+1)-1)/kqb);
      qdB    = Qch.*((sqrt(kqb+1)-1)/kqb);                    
    end
    
    % drift diffusion charge
    vgt    = Qxo/Cch;
    vdsatq = sqrt(Ff*alpha_vt^2+vgt^2);
    udsi   = vdsi./vdsatq;
    if para.beta_ch<0
      beta_ch = para.beta;
    else
      beta_ch = para.beta_ch;
    end
    Fsatq  = para.fb*udsi./(power(1+power(udsi,beta_ch),1/beta_ch));
    eta    = 1-Fsatq;
    eta2   = eta.*eta;
    eta3   = eta2.*eta;
    qsDD   = Qch.*(6+12*eta+8*eta2+4*eta3)./(15*(1+2*eta+eta2));    
    qdDD   = Qch.*(4+8*eta+12*eta2+6*eta3)./(15*(1+2*eta+eta2));
    
    % entire charge for source and drain
    Fsatq2 = Fsatq.*Fsatq;
    Qs = (qsDD.*(1-Fsatq2) + qsB.*Fsatq2);
    Qd = (qdDD.*(1-Fsatq2) + qdB.*Fsatq2);
    
end

