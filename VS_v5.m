function [Id,Qs,Qd,Vgi,Vsi,Vdi,Y11,Y12,Y21,Y22]=VS_v5(para,Vg,Vs,Vd,f)
%%% Virtual source model, version 1
% todo: include rsf/rdf/cgspar2/cgdpar2
% todo: compare with verilogA implementation
% ---------------------------
% Input:
% Vg   .. gate voltage    (single value)
% Vs   .. source voltage  (single value)
% Vd   .. drain voltage   (single value)
% f    .. frequency       (vector or single value)
% para .. model parameter
% ---------------------------
% output:
% Id   .. current
% Qs   .. charge between source and gate
% Qd   .. charge between drain  and gate
% Vgi  .. internal gate voltage
% Vsi  .. internal source voltage
% Vdi  .. internal drain voltage
% Y11  .. Y-parameter
% Y12  .. Y-parameter
% Y21  .. Y-parameter
% Y22  .. Y-parameter

% if (~isfield(para,'rsf'));     para.rsf=0;         end  % [Ohm]finger resistance source
% if (~isfield(para,'rdf'));     para.rdf=0;         end  % [Ohm]finger resistance drain
% if (~isfield(para,'rsc'));     para.rsc=0;         end  % [Ohm*um]contact resistance source
% if (~isfield(para,'rdc'));     para.rdc=0;         end  % [Ohm*um]contact resistance drain
% if (~isfield(para,'rsc_t'));   para.rsc_t=0;       end  % [Ohm]contact resistance per tube source
% if (~isfield(para,'rdc_t'));   para.rdc_t=0;       end  % [Ohm]contact resistance per tube drain
% if (~isfield(para,'rg'));      para.rg =0;         end  % [Ohm/um] contact resistance gate
% if (~isfield(para,'cgspar'));  para.cgspar=0;      end  % [F/um] parasitic gate source capacitance
% if (~isfield(para,'cgdpar'));  para.cgdpar=0;      end  % [F/um] parasitic gate drain capacitance
% if (~isfield(para,'cdspar'));  para.cdspar=0;      end  % [F/um] parasitic source drain capacitance
% if (~isfield(para,'cgspar2')); para.cgspar2=0;     end  % [F/um] parasitic gate source capacitance 2
% if (~isfield(para,'cgdpar2')); para.cgdpar2=0;     end  % [F/um] parasitic gate drain capacitance 2
if (~isfield(para,'rcmod'));   para.rcmod=0;       end  % Contact mode:- 0: user-defined series resistance; 1: transmission line model with diameter dependence             - 2: transmission line model without diameter dependence
if (~isfield(para,'rs0'));     para.rs0=0;         end  % para.rs0:    user-defined series resistance (Ohm per CNT, one-sided)
% if (~isfield(para,'type'));    para.type=1;        end  % 1..n-type, -1 p-type
if (~isfield(para,'geomod'));  para.geomod=1;      end  % 0..Bulk, 1..Top Gate, 2..GAA
if (~isfield(para,'w'));       para.w=0;           end  % [m] gate wg
if (~isfield(para,'lch'));     para.lch=1;         end  % [m] channel length
if (~isfield(para,'Lc'));      para.Lc=1;          end  % [m] contact length
if (~isfield(para,'Lsd'));     para.Lsd=1;         end  % [m] source/drain extension length
if (~isfield(para,'Hg'));      para.Hg=1;          end  % [m] gate metal height
if (~isfield(para,'dens'));    para.dens=1;        end  % [1/um] tube/wire density
if (~isfield(para,'u'));       para.u=1;           end  % [m] effective gate width per Fin 
if (~isfield(para,'r_semi'));  para.r_semi=1;      end  % [m] tube/wire circumferenc
if (~isfield(para,'lambda0')); para.lambda0=1;     end  % [m] mean free path
% if (~isfield(para,'mu0'));     para.mu0=0.135;     end  % [m^2 V^-1 s^-1] mobility fitting parameter 
% if (~isfield(para,'lmu'));     para.lmu=48.6e-9;   end  % [m] mobility fitting parameter
% if (~isfield(para,'cmu'));     para.cmu=1.13;      end  % [m] mobility fitting parameter
if (~isfield(para,'kspa'));    para.kspa=1;        end  % spacer dielectric constant
% if (~isfield(para,'cq'));      para.cq=1;          end  % [F/m^2] qunatum capacitance
% if (~isfield(para,'cq_t'));    para.cq_t=1;        end  % [F/m] qunatum capacitance for 1D channels
if (~isfield(para,'tox'));     para.tox=1;         end  % [m] gate oxide thickness
if (~isfield(para,'eps_ox'));  para.eps_ox=1;      end  % [-] gate oxide relative permittivity
% if (~isfield(para,'vth0'));    para.vth=1;         end  % [V] threshold voltage
% if (~isfield(para,'dvthq'));   para.dvthq=0;       end  % [V] threshold voltage
% if (~isfield(para,'alpha'));   para.alpha=3.5;     end  % [-] fitting factor
% if (~isfield(para,'beta'));    para.beta=1.5;      end  % [-] fitting factor
% if (~isfield(para,'beta_ch')); para.beta_ch=-1;    end  % [-] fitting factor
% if (~isfield(para,'dibl'));    para.dibl=1;        end  % [-] fitting factor for DIBL effect
% if (~isfield(para,'nss'));     para.nss=1;         end  % [-] fitting factor for Subthreshold swing
% if (~isfield(para,'meff'));    para.meff=1;        end  % [-] effective mass
% if (~isfield(para,'fb'));      para.fb=1;          end  % [-] fitting factor
% if (~isfield(para,'lfl'));     para.lfl=1;         end  % [m] low field length
% if (~isfield(para,'va'));      para.va=0;          end  % [V] Early voltage
% if (~isfield(para,'gamma'));   para.gamma=1;       end  % [-] mobility reduction parameter
% if (~isfield(para,'rmet'));    para.rmet=0;        end  % [Ohm*m] metallic tubes resistance Rmet,cnt/met_tube density
% if (~isfield(para,'vball'));   para.vball=1;       end  % [m/s] ballistic injection velocity
% if (~isfield(para,'dvthq'));   para.dvthq=0;       end
if (~isfield(para,'Vfb'));     para.Vfb=0;         end  % [V] Flat-band voltage
if (~isfield(para,'Efsd'));    para.Efsd=0;        end  % [eV] Fermi-level at the source/drain minus to the Ec determined by the doping density
if (~isfield(para,'temp'));    para.temp=300;      end  % [K] semicondcutor temperature 
% if (~isfield(para,'vthd'));    para.vthd=-5;       end  % s-shape output characteristic (hidden feature)
if (~isfield(para,'nd'));      para.nd=0;          end  % s-shape output characteristic
% if (~isfield(para,'pqcl'));    para.pqcl=0;        end  % quantum capacitance limit
if (~isfield(para,'SDTmod'));  para.SDTmod=1;      end %- 0: SDT off, 1: SDT on, 2: SDT on, with a lower limit of surface potential no CB-VB-CB tunneling
if (~isfield(para,'BTBTmod')); para.BTBTmod=1;     end % 0: without BTBT, 1: with BTBT
if (~isfield(para,'nd'));      para.nd=0;          end
if (nargin<5); f=0;end
para.dcnt    = 2*para.r_semi;

% normalize:
para.dens    = para.dens   *1e6;  % tube/wire/fin density from /um to /m
% para.rsc     = para.rsc    *1e-6; % contact resistance changed from Ohm*um to Ohm*m
% para.rdc     = para.rdc    *1e-6; % contact resistance changed from Ohm*um to Ohm*m
% para.rsf     = para.rsf    *1e6;  % finger resistance changed from Ohm/um to Ohm/m
% para.rdf     = para.rdf    *1e6;  % finger resistance changed from Ohm/um to Ohm/m
% para.cgspar  = para.cgspar *1e6;  % parasitic capacitance from F/um to F/m
% para.cgdpar  = para.cgdpar *1e6;  % parasitic capacitance from F/um to F/m
% para.cdspar  = para.cdspar *1e6;  % parasitic capacitance from F/um to F/m

    m0          = 9.11e-31;     % electron mass
    kB          = 1.38e-23;     % Boltzmann constant
    q           = 1.6e-19;      % electron charge
    h           = 6.626e-34;    % Planck constant
    h_bar       = h/2/pi;       % reduced Planck constant
    acc         = 0.142e-9;     % carbon-carbon distance
    Vp          = 3;            % CNT tight binding parameter
    vF          = 1e6;          % Fermi velocity
    vt          = para.temp*kB/q;  % thermal voltage
    Eg          = 2*acc*Vp/para.dcnt;    % band gap [eV]
    
for k = 1:length(Vg)
    for l = 1: length(Vd)
        x0(1) = Vs; %S
        x0(2) = Vd(l); %D

        if (para.geomod==1)
            x = fsolve(@(x) contact_resistance(x,Vg(k),Vs,Vd(l),para),x0,...
                optimset('Display','off','TolFun',1e-12,'TolX',1e-12));
        else
            x = x0;
        end

        [Id(k,l),Qs(k,l),Qd(k,l)] = vs_intr(Vg(k),x(1),x(2),para,false);
        Vgi  = Vg(k);
        Vsi  = x(1);
        Vdi  = x(2);

%         if max(f)==0
%             Y11=0;
%             Y12=0;
%             Y21=0;
%             Y22=0;
%         else

%             [Id(k,l),Qs(k,l),Qd(k,l)] = vs_intr(Vg(k),x(1),x(2),para,false);
%             Vgi = Vg(k);
%             Vsi = x(1);
%             Vdi = x(2);

%             dv=1e-6;
%             [Id1,Qs1,Qd1] = vs_intr(Vg(k)+dv,x(1),x(2),para,false);

%             gm  = (Id1-Id(k,l))/dv;
%             cm  = 0;
%             cgs = (Qs1-Qs(k,l))/dv;
%             cgd = (Qd1-Qd(k,l))/dv;
% 
%             [Id1] = vs_intr(Vg(k),x(1),x(2)+dv,para,true);
%             gds   = (Id1-Id(k,l))/dv;
%             cds   = 0;

%             if (para.geomod==0)
%               rs  = para.rsc/para.w;
%               rd  = para.rdc/para.w;
%             
%             else
%               rs  = para.rsc_t/(para.dens*para.w);
%               rd  = para.rdc_t/(para.dens*para.w);
%               
%             end  

            nsd     = CNTCarrierApprox(para.dcnt,para.Efsd+Eg/2,para.temp);  % source/drain doping density
            Rc      = RcCNT(para.Lc,para.dcnt,para.rcmod);
            Rext    = RextCNT(para.Lsd,nsd,para.dcnt);
                if para.rcmod == 0
                    rs  = para.rs0/(para.dens*para.w);
                else
                    rs  = (Rc+Rext)/(para.dens*para.w); 
                end
        
%             rg  = para.rg;
%             rd = rs;
%             % frequency
%             w   = 2*pi*f;
%             cgg = cgs+cgd;

% %             Y-parameter 
%             alpha = cgg*gds + cgd*gm;
%             beta  = cgg*cds + cgd*(cgs-cm);
%             gamma = rs*rd+rs*rg+rd*rg;
%             a     = 1 + rs*(gm + gds) + rd*gds - w^2*gamma*beta;
%             b     = rs*(cgs+cds-cm) + rd*(cgd+cds) + rg*cgg + gamma*alpha;

%             Y11(k,l) = 1./(a+1i*w*b)*(      1i*w*(cgg    + alpha*(rs+rd)) - w^2*(rs+rd)*beta);
%             Y12(k,l) = 1./(a+1i*w*b)*(    - 1i*w*(cgd    + alpha* rs    ) + w^2* rs    *beta);
%             Y21(k,l) = 1./(a+1i*w*b)*(gm  - 1i*w*(cgd    + alpha* rs    ) + w^2* rs    *beta);
%             Y22(k,l) = 1./(a+1i*w*b)*(gds + 1i*w*(cds+cgd+ alpha*(rs+rg)) - w^2*(rs+rg)*beta);
% 
%             Y11(k,l) = Y11(k,l) + 1i*w*(para.cgspar+para.cgdpar)*para.w;
%             Y12(k,l) = Y12(k,l) - 1i*w*(            para.cgdpar)*para.w;
%             Y21(k,l) = Y21(k,l) - 1i*w*(            para.cgdpar)*para.w;
%             Y22(k,l) = Y22(k,l) + 1i*w*(para.cdspar+para.cgdpar)*para.w;
%         end
    end
end    

end

function F = contact_resistance(x,Vg,Vs,Vd,para)
%%% contact resistance
%    x(1) = VSi    ; %Vsi
%    x(2) = VDi    ; %Vdi
% equations:
%  I:  Vsi -Vs - Rcs*Id  = 0
%  II: Vd  -Vdi - Rcd*Id = 0
    acc         = 0.142e-9;     % carbon-carbon distance
    Vp          = 3;            % CNT tight binding parameter
    Eg      = 2*acc*Vp/para.dcnt;    % band gap [eV]
    nsd     = CNTCarrierApprox(para.dcnt,para.Efsd+Eg/2,para.temp);  % source/drain doping density
    Rc      = RcCNT(para.Lc,para.dcnt,para.rcmod);
    Rext    = RextCNT(para.Lsd,nsd,para.dcnt);
    if para.rcmod == 0
        rs  = para.rs0/(para.dens*para.w);
    else
        rs  = (Rc+Rext)/(para.dens*para.w); 
    end
%     if (para.geomod==0)
%         % contact resistance in Ohm*m
%         rsc = para.rsc/para.w;
%         rdc = para.rdc/para.w;
%     else
%         % contact resistance in Ohm/tube
%         rsc = para.rsc_t/(para.dens*para.w);
%         rdc = para.rdc_t/(para.dens*para.w);
%     end
%     rs = rsc + para.rsf*para.w;
%     rd = rdc + para.rdf*para.w;
    
    
    Id = vs_intr(Vg,x(1),x(2),para,true);
    F  = [x(1) - Vs - rs*Id,...
          Vd - x(2) - rs*Id];
end


function [Id,Qs,Qd] = vs_intr(Vg,Vs,Vd,para,onlycurrent) 
    m0          = 9.11e-31;     % electron mass
    kB          = 1.38e-23;     % Boltzmann constant
    q           = 1.6e-19;      % electron charge
    h           = 6.626e-34;    % Planck constant
    h_bar       = h/2/pi;       % reduced Planck constant
    acc         = 0.142e-9;     % carbon-carbon distance
    Vp          = 3;            % CNT tight binding parameter
    vF          = 1e6;          % Fermi velocity
    vt          = para.temp*kB/q;  % thermal voltage
    % Bias
    vds0    = abs(Vd-Vs);
    vgs0    = Vg-min(Vs,Vd);
    
    % Virtual source related internal parameters
    Ncnt    = round(para.w*para.dens);    % number of CNTs in the device
    Eg      = 2*acc*Vp/para.dcnt;    % band gap [eV]
    kch     = 1;                % CNT dielectric constant
    Lof_sce = para.tox/3;            % SCE fitting parameter
    cqa     = 0.64*1e-9;        % CNT quantum capacitance param 1
    cqb     = 0.1*1e-9;         % CNT quantum capacitance param 2
    vB0     = 7.6263e5;            % ballistic velocity for CNT = 1.2 nm
    d0      = 1.2e-9;           % reference CNT diameter
%     mfpv    = 1.2159e-08;           % carrier mean free path for velocity calculation
    vtha    = 0.0;              % fitting parameter to match VS current and DSDT
        
    %%%% VS parameters %%%%
    % Inversion capacitance
    if para.geomod == 1     % GAA
        cox     = CapGAA(para.dcnt,para.tox,para.eps_ox)*Ncnt;
    elseif para.geomod == 2 % top-gate with screening
        cox     = CapTopGate(1/para.dens,para.dcnt,para.tox,para.eps_ox,para.w,1);
    elseif para.geomod == 3 % top-gate w/o screening
        cox     = CapTopGate(1/para.dens,para.dcnt,para.tox,para.eps_ox,para.w,0);
    else
        cox     = CapGAA(para.dcnt,para.tox,para.eps_ox)*Ncnt;
    end
    cqeff   = (cqa*sqrt(Eg)+cqb)*Ncnt;    % effecitve quantum capacitance
    Cinv    = cox*cqeff/(cox+cqeff);
    mu      = CNTMobility(para.lch,para.dcnt); % Mobility
    % SCE parameters
    nsd     = CNTCarrierApprox(para.dcnt,para.Efsd+Eg/2,para.temp);  % source/drain doping density
    lam     = Scalelength_GAA(kch,para.eps_ox,para.dcnt,para.tox);    % scale length
    eta     = exp(-(para.lch+Lof_sce*2)/lam/2);
    n0      = 1./(1-2.*eta);          % SS factor at Vd = 0
    dibl    = eta;                  % drain-induced barrier lowering
    dvth    = -2*(para.Efsd+Eg/2)*eta;   % vt roll-off
    vB      = vB0*sqrt(para.dcnt/d0);    % ballistic velocity
    vxo     = vB*para.lambda0/(para.lambda0+2*para.lch);        % virtual source velocity
    % Series resistance
%     Rc      = RcCNT(para.Lc,para.dcnt,para.rcmod);
%     Rext    = RextCNT(para.Lsd,nsd,para.dcnt);
%     if para.rcmod == 0
%         rs  = para.rs0/Ncnt;
%     else
%         rs  = (Rc+Rext)/Ncnt; 
%     end
    vth0    = Eg/2+dvth+para.Vfb+vtha;   % Threshold voltage
    alpha   = 3.5;      % VS fitting parameter
    beta    = 1.8;      % VS fitting parameter
    nd      = 0;        % drain bias dependent SS factor
    
    %%%% Tunneling parameter %%%%
    Lof_sdt = (0.0263*para.kspa+0.056)*para.tox;
%     Lof_sdt = para.tox/2;    % SDT fitting parameter, gate length offset
    phi_on  = 0.05;      % SDT fitting parameter, Vt adjustment
    N_tun   = 100;      % resolution of energy integral
    ScatFac = para.lambda0/(para.lambda0+para.lch);
    lam_btb = 2.5e-9 + 1.1e-9/12*(para.kspa-4); % BTBT fitting parameter
    
    
    %%%% Calculate current %%%%
    vgsi    = vgs0;
    vdsi    = vds0;
    vgs     = vgs0;
    vds     = vds0;
    
    %%%% virtual source current %%%%
    vth     = vth0-dibl*vds;
    Ff      = 1./(1+exp((vgs-(vth-alpha*vt/2))./(alpha*vt)));
    nss     = n0+nd*vds;
    Qxo     = Cinv*nss*vt*log(1+exp((vgs-(vth-alpha*vt*Ff))/nss/vt));
%     vx      = (Ff+(1-Ff)/(1+rs*Cinv*(1+2*dibl)*vxo))*vxo;
    vdsats = vxo*para.lch/mu;
    vdsat   = vdsats.*(1-Ff)+vt*Ff;
    Fs      = vds./vdsat./(1+(vds./vdsat).^beta).^(1/beta);
    Ivs    = Qxo.*vxo.*Fs;
    
    %%%% source-to-drain tunneling %%%%
    if para.SDTmod ~= 0
        % surface potential
        if para.SDTmod == 1
            if Qxo == 0
                psys    = (vgs-(vth-alpha*vt*Ff))/nss+Eg/2-phi_on;
            else
                psys    = log(Qxo/(Cinv*nss*vt))*vt+Eg/2-phi_on;
            end
        else    % psys is clamped at occurrence of CB-VB-CB tunneling
            Qmin    = exp((para.Efsd-Eg+phi_on)/vt);
            psys    = log(Qxo/(Cinv*nss*vt)+Qmin)*vt+Eg/2-phi_on;
        end
        Isdt    = SDT( Eg,para,psys,Lof_sdt,vds,lam,vt,N_tun );
        Isdt    = Isdt*ScatFac*Ncnt;
    else
        Isdt    = 0;
    end
    
    %%%% band-to-band tunneling %%%%
    if vds <= Eg || para.BTBTmod == 0
        Ibtbt   = 0;
    else
        Ew      = vds-Eg;
        dE      = Ew/N_tun;
        Ei      = dE/2:dE:Ew; 
        prefac  = Eg*q/(h_bar*vF);
        zeta    = -1-2*Ei/Eg;
        tb      = -lam_btb*pi*(zeta+sqrt(zeta.^2-1));
        Ti      = exp(-prefac*tb);
        Efs     = para.Efsd+vds;
        Efd     = para.Efsd;
        igrid   = Ti.*(1./(1+exp((Ei-Efs)/vt))-1./(1+exp(((Ei-Efd))/vt)));
        Ibtbt   = 4*q^2/h*sum(igrid)*dE*ScatFac*Ncnt;
    end
    
    Id  = Ivs+Isdt+Ibtbt;
    
    if onlycurrent
        Qs = 0;
        Qd = 0;
        return
    end
    
    %%%% Charge model %%%%
    % Internal Parameters
    avt     = alpha*vt;
    nss     = n0+nd*vdsi;       % SS factor
    vth     = vth0-dibl*vdsi;   % threshold voltage
    Ff      = 1./(1+exp((vgsi-(vth-avt/2))./avt));
    Qcha    = Cinv*nss*vt*log(1+exp(((vgsi-(vth-avt*Ff))/nss/vt)));
    
    % Charge correction due to 1-D quantum capacitance
    alphab  = 3.5;
    bvt     = alphab*vt;
    nb      = 1.5;
    vthb    = vth+0.2*Eg+0.16;
    Ffb     = 1/(1+exp((vgsi-(vthb-bvt/2))/bvt));
    cqinf   = 8*q/(3*acc*pi*Vp)*Ncnt;  % cqinf = 3.2e-10 [F/m]
    cinvb   = cox*cqinf/(cox+cqinf);
    Qchb    = (Cinv-cinvb)*nb*vt*log(1+exp((vgsi-(vthb-bvt*Ffb))/nb/vt));
    Qch     = -para.lch*(Qcha-Qchb);
    
    % Charge version vdsat
    vgt     = -Qch/Cinv/para.lch;
    Vdsatq  = sqrt(Ff*avt^2+vgt^2);
    Fsatq   =(vdsi/Vdsatq)/((1+(vdsi/Vdsatq)^beta)^(1/beta));
    
    % Drift-Diffusion, Tsividis, 2nd Ed., Eq. (7.4.19) and (7.4.20)
    eta     = 1-Fsatq;
    QsDD    = Qch*(6+12*eta+8*eta^2+4*eta^3)/(15*(1+eta)^2);
    QdDD    = Qch*(4+8*eta+12*eta^2+6*eta^3)/(15*(1+eta)^2);
    
    % Quasi-Ballistic transport, assume parabolic potential profile
    zeta    = 1.0;  % fitting parameter
    meff    = 2/9*Eg/q*(h_bar/acc/Vp)^2/m0;     % CNT effective mass
    if vdsi == 0
        QsB     = Qch/2;
        QdB     = Qch/2;
    else
        kqb     = 2*q*vdsi*zeta/(meff*m0*vxo^2);
        QsB     = Qch*(asinh(sqrt(kqb))/sqrt(kqb)-(sqrt(kqb+1)-1)/kqb);
        QdB     = Qch*((sqrt(kqb+1)-1)/kqb);
    end
    
    Fsatq2  = Fsatq^2;
    Qs      = QsDD*(1-Fsatq2) + QsB*Fsatq2;
    Qd      = QdDD*(1-Fsatq2) + QdB*Fsatq2;
    Qg      = -(Qs+Qd);
    
    %%%% Parasitic Capacitance %%%%
    if para.geomod == 2
        scmod   = 1;    % fringe capacitance w/ charge screening
    else
        scmod   = 0;    % fringe capacitance w/o charge screening
    end
    Cf      = CapFringe(para.Lsd,para.tox,para.dcnt,1/para.dens,para.w,para.kspa,scmod);  % fringe capacitance
    Cg2c    = CapC2C(para.Lsd,para.lch,para.Hg,para.w,para.kspa);     % gate-to-contact parasitic capacitance
    Cp      = 2*(Cf+Cg2c);
end
    

% source-to-drain tunneling function
function [ isdt ] = SDT( Eg,para,psys,Lof,vds,lam,vt,N_tun )
    h       = 6.626e-34;
    h_bar   = h/2/pi;
    vF      = 1e6;
    q       = 1.6e-19;
    
    Lh      = para.lch/2+Lof;
    
    % top of the barrier; to avoid Et < -para.Efsd
    Et0     = Eg/2-psys;
    aEt     = 0.05;
    Et      = (Et0+para.Efsd) + aEt*log(1+exp(-(Et0+para.Efsd)/aEt))-para.Efsd;
    
    Eitop   = min(Et,0.4);
    dE      = (Eitop+para.Efsd)/N_tun;
    Ei      = -para.Efsd+dE/2:dE:Eitop;
    
    prefac  = Eg*q/(h_bar*vF);
    tbs     = SDTProb( Et,-para.Efsd,Ei,Lh,Eg,lam );
    tbd     = SDTProb( Et,-para.Efsd-vds,Ei,Lh,Eg,lam );
    Ti      = exp(-prefac*(tbs+tbd));
    Efs     = 0;
    Efd     = -vds;
    igrid   = Ti.*(1./(1+exp((Ei-Efs)/vt))-1./(1+exp(((Ei-Efd))/vt)));
    isdt    = 4*q^2/h*sum(igrid)*dE;
end

% SDTProb: SDT probability; Ec = a*exp(x/lambda)+b
% Et == Ec(x=0); El == Ec(x=L)
function y = SDTProb( Et,El,Ei,L,Eg,lambda )
    a       = -(Et-El)/(exp(L/lambda)-1);   
    b       = Et-a;
    x0      = 1-2*(a+b-Ei)/Eg;
    x0(Et > Ei+Eg) = -1;
    x1      = 1;
    zeta    = 2*(b-Ei)/Eg-1;
    y       = (SDTProbInt(x1,zeta)-SDTProbInt(x0,zeta))*lambda;
end

% SDTProbInt: integrate sqrt(1-x^2)/(x+zeta)   
function y = SDTProbInt(x,zeta)
    rb      = sqrt(zeta.^2-1).*sqrt(1-x.^2)./(zeta.*x+1);
    z       = zeta.*x+1;
    theta   = atan(rb);
    theta(z==0) = -pi/2;
    theta(z>0)  = theta(z>0)-pi;
    y       = sqrt(zeta.^2-1).*theta+ zeta.*asin(x) + sqrt(1 - x.^2);
end

