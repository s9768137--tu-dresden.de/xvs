function [Vth,index] = calc_Vth(Vg,Id,method)

if method == 1 % Extrapolation in the linear region method
    gm = gradient(Id)./gradient(Vg);
    [x,maxGmIndex] = max(abs(gm));
    Vth = Vg(maxGmIndex) - Id(maxGmIndex)./gm(maxGmIndex);
    C = Id(maxGmIndex) - Vg(maxGmIndex).*gm(maxGmIndex);
    index = NaN;
elseif method == 2 % Maximum second-derivative method
    gm = gradient(Id)./gradient(Vg);
    gm_dash = gradient(gm)./gradient(Vg);
    [val,index] = max(gm_dash);
    Vth = Vg(index);
end

end
