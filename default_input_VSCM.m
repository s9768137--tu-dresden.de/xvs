function param_VSCM = default_input_VSCM()

if ~exist('param_name','var')
    param_name = 'param_VSCM';
end

clear(param_name);

eval([param_name '.type=1;']);
eval([param_name '.geomod=1;']);
eval([param_name '.w=1e-6;']);
eval([param_name '.lch=100e-9;']);
eval([param_name '.dens=1;']);
eval([param_name '.u=1;']);
eval([param_name '.r_semi=2;']);
eval([param_name '.lambda0=0.2;']);
eval([param_name '.cq=1e-18;']);
eval([param_name '.cq_t=1e-9;']);
eval([param_name '.tox=10e-9;']);
eval([param_name '.eps_ox=3;']);
eval([param_name '.vth0=0.4;']);
eval([param_name '.dvthq=0;']);
eval([param_name '.alpha=3.5;']);
eval([param_name '.beta=1.8;']);
eval([param_name '.beta_ch=-1;']);
eval([param_name '.dibl=1;']);
eval([param_name '.nss=1;']);
eval([param_name '.meff=1;']);
eval([param_name '.fb=1;']);
eval([param_name '.lfl=1e-9;']);
eval([param_name '.va=0;']);
eval([param_name '.gamma=1;']);
eval([param_name '.rmet=0;']);
eval([param_name '.vball=1;']);
eval([param_name '.dvthq=0;']);

eval([param_name '.temp=300;']);
eval([param_name '.vthd=-5;']);
eval([param_name '.nd=0;']);
eval([param_name '.pqcl=0;']);

eval([param_name '.rsf=0;']);
eval([param_name '.rdf=0;']);
eval([param_name '.rsc=0;']);
eval([param_name '.rdc=0;']);
eval([param_name '.rsc_t=0;']);
eval([param_name '.rdc_t=0;']);
eval([param_name '.rg=0;']);
eval([param_name '.cgspar=0;']);
eval([param_name '.cgdpar=0;']);
eval([param_name '.cdspar=0;']);
eval([param_name '.cgspar2=0;']);
eval([param_name '.cgdpar2=0;']);

end

