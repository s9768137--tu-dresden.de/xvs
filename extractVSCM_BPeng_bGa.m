%% Initialize path 

% clear all
% cd 'D:\mannamalai\Projects\CM_Parameters_130nm_CNTFET\from_others\wolf\Evaluation'
hGraph = sethGraphPara;
load('BPeng_bGa_VS_v4.mat');

%%
pa = 'D:\mannamalai\GitLab\CompactModels\VS';
cd(pa);
constantMat = load('constant.mat');
constant = constantMat.constant;

%% Read Experimental or simulation data

compactModelDir = pwd;

pa = 'D:\mannamalai\Projects\CNT_charge_model\simulations\COOS';
cd(pa);

clear vgs id_vgs ivElpa Qch_vgs

name   = 'CNTFET_BPeng_bGa';
VDA = [0.05,0.5,1.0];
transport = 'dd';
for VIndex = 1:length(VDA)
    VdsString = regexp(sprintf('%1.2f',abs(VDA(VIndex))) ,'\.','split');

    OUTPUT_name         = [name,'_Vds',VdsString{1},'p',VdsString{2},'V'];

    ivElpaFile = [name,'/',OUTPUT_name,'/',OUTPUT_name,'_',transport,'_iv.elpa'];
    ivElpa(VIndex) = read_elpa(ivElpaFile);
    
    capFile = [name,'/',OUTPUT_name,'/',OUTPUT_name,'_capacitance.elpa'];
    capElpa = read_elpa(capFile);
    
    vgs{VIndex} = ivElpa(VIndex).V_g;
    id_vgs{VIndex} = ivElpa(VIndex).I_d*1e6;
    Qch_vgs{VIndex} = abs(ivElpa(VIndex).Q_ch)*1e18;
end

cd(compactModelDir);

clear Vg Vd Id
for VIndex = 1:length(VDA)
    Vg(:,VIndex) = ivElpa(VIndex).V_g;
    Id(:,VIndex) = ivElpa(VIndex).I_d;
    Qch(:,VIndex) = abs(ivElpa(VIndex).Q_ch);
    Vd(:,VIndex) = repmat(VDA(VIndex),size(Vg(:,VIndex),1),1);
end 

%% Preliminary Parameter
PARA.type       = 1;
PARA.geomod     = 1;
PARA.lch        = 100e-9;
PARA.w          = 20e-9;
PARA.r_semi     = 0.65e-9;
PARA.dens       = 50;
PARA.tox        = 5e-9;
PARA.eps_ox     = 16; % HfO2
PARA.cq_t       = 0.01;
PARA.meff       = 0.4;
PARA.lambda0    = 6e-6;
PARA.lfl        = 1e-9;
PARA.vball      = 1e7;
PARA.gamma      = 0;
PARA.vth0       = 0.2;
PARA.dibl       = 0.053;      
PARA.alpha      = 3.5;
PARA.beta       = 1.8;
PARA.Rmet       = 0;
PARA.va         = 0;
PARA.rsf        = 0;
PARA.rdf        = 0;
PARA.rg         = 0; % Need y-parameter measurements or simulation data from COOS
PARA.beta_ch    = 1.8;
PARA.nd         = 5;
PARA.vthd       = 0.3;
PARA.fb         = 0.1;

%% parasitic cpacitance from COOS simulations
PARA.cgspar     = capElpa.C_g_s/PARA.w*1e-6;
PARA.cgdpar     = capElpa.C_g_d/PARA.w*1e-6;
PARA.cdspar     = capElpa.C_d_s/PARA.w*1e-6;

%
PARA.cgspar2 = 0;
PARA.cgdpar2 = 0;


%% Calculate threshold voltage at low Vds
PARA.vth0 = calc_Vth(ivElpa(1).V_g,ivElpa(1).I_d);

%% Calculate subthreshold slope and extract nss parameter
PARA.nss = calc_nss(ivElpa(1).V_g,ivElpa(1).I_d);

%% Calculate dibl parameter for all Vds
for VIndex = 1:length(VDA)
    Vth(VIndex) = calc_Vth(ivElpa(VIndex).V_g,ivElpa(VIndex).I_d);
end
PARA.dibl = (Vth(end) - Vth(1))/(VDA(end) - VDA(1));

%% Extract contact resistance from Y-fucntion method

%% contiunuing Y-function method
% find the linear region from the plot and give the input to Y-function
VdIndex = length(VDA);
figure()
hold on
h=plot(Vg(:,1:VdIndex),Id(:,1:VdIndex)*1e6);  set(h,'linewidth',2);
xlabel('V_G (V)','FontName','Helvetica');
ylabel('I_D (\muA)','FontName','Helvetica');
set(gca,'FontSize',18,'FontName','Helvetica','FontWeight','Bold');
% call y-function method
[param,Rc_YFMpp] = YFMpp_fun(Vg(:,1:VdIndex),Vd(:,1:VdIndex),Id(:,1:VdIndex)); %13-19 <-- just 4 transfer curves
%%
PARA.rsc_t = Rc_YFMpp;
PARA.rdc_t = Rc_YFMpp;

%% Extract parasitic capacitance from COOS simulation or Y-parameter data

% Cgs_par = capElpa.C_g_s;
% Cgd_par = capElpa.C_g_d;

%% Extract Cq from parasitic and Cox

if (PARA.geomod==0) % Bulk Material with classic capacitance calculation
    cox = PARA.eps_ox*constant.eps0 / PARA.tox;
elseif (PARA.geomod==1) % Nanowire/tube to planar gate
    pt  = PARA.dens;  % tubes/wires per m
    h   = PARA.r_semi+PARA.tox;
    cox = 2*pi*PARA.eps_ox*constant.eps0*pt/log(sinh(pi*pt*(h+sqrt(h^2-PARA.r_semi^2)))/(pi*pt*PARA.r_semi)); % adjusted Hanson formula
elseif (PARA.geomod==2) % Nanowire/tube cylindrical gate
    pt  = PARA.dens;  % tubes/wires per m
    h   = PARA.r_semi+PARA.tox;
    cox = 2*pi*PARA.eps_ox*constant.eps0*pt/(log(h/PARA.r_semi)); % 
elseif (PARA.geomod==3) % Multi-gate structures (DG, FinFET)
    pt  = PARA.dens;  % tubes/wires per m
    cox = PARA.eps_ox*constant.eps0*PARA.u*pt/PARA.tox;    % normal plate capacitance
end

% PARA.cq_t = cox*0.01;

%% Extract values for Vgs dependent parameters for Ids

ref1=[];ref2=[];ref3=[];ref=[];

% for j = 1:length(VG)
%     ref1 = [ref1;ones(length(vds{j}(1:2:end)),1)*(VG(j)+0.2)];
%     ref2 = [ref2;vds{j}(1:2:end)];
%     ref3 = [ref3;id{j}(1:2:end)];
% end
for j = 1:length(VDA)
   vgsvec = vgs{j}(1:1:end);
   idvec  = id_vgs{j}(1:1:end);
   ref1 = [ref1;vgsvec(vgsvec>-1)];
   ref2 = [ref2;ones(length(vgsvec(vgsvec>-1)),1)*VDA(j)];
   ref3 = [ref3;idvec(vgsvec>-1)];
end

ref(:,1) = ref1;
ref(:,2) = ref2;
ref(:,3) = ref3;


%% 
fitFlag = 1; % fit flag

lowBoundReFit   = 0.7;
highBoundReFit  = 1.3;

% fit of current parameters 
LBounds = zeros(1,1);
UBounds = zeros(1,1);
params  = zeros(1,1);
fit = [];
n = 0;
% PARA.vth0 = 0.55;
n=n+1; fit.para{n}='vth0';      params(n) = PARA.vth0;      LBounds(n)=0.3;             UBounds(n)=0.6;
n=n+1; fit.para{n}='dibl';       params(n) = PARA.dibl;       LBounds(n)=PARA.dibl*0.75;   UBounds(n)=PARA.dibl*1.25;
n=n+1; fit.para{n}='nss';       params(n) = PARA.nss;       LBounds(n)=PARA.nss*0.75;   UBounds(n)=PARA.nss*1.25;
n=n+1; fit.para{n}='cq_t';       params(n) = PARA.cq_t;       LBounds(n)=0;   UBounds(n)=1e-6;
n=n+1; fit.para{n}='meff';     params(n) = PARA.meff;     LBounds(n)=1e-2;               UBounds(n)=1;
n=n+1; fit.para{n}='alpha';     params(n) = PARA.alpha;     LBounds(n)=2;  UBounds(n)=4;
n=n+1; fit.para{n}='beta';     params(n) = PARA.beta;     LBounds(n)=1;  UBounds(n)=2;
n=n+1; fit.para{n}='gamma';     params(n) = PARA.gamma;     LBounds(n)=1e-3;  UBounds(n)=1;
n=n+1; fit.para{n}='lambda0';   params(n) = PARA.lambda0;   LBounds(n)=10e-9;         UBounds(n)=1e-7;
n=n+1; fit.para{n}='lfl';       params(n) = PARA.lfl;       LBounds(n)=1e-10;           UBounds(n)=1e-8;
n=n+1; fit.para{n}='vball';     params(n) = PARA.vball;     LBounds(n)=1e5;             UBounds(n)=10e5;
n=n+1; fit.para{n}='rsc_t';       params(n) = PARA.rsc_t;       LBounds(n)=Rc_YFMpp/2;    UBounds(n)=Rc_YFMpp*2;
% n=n+1; fit.para{n}='rdc';       params(n) = PARA.rdc;       LBounds(n)=PARA.rdc*0.5;    UBounds(n)=PARA.rdc*1.5;

p0        = (params-LBounds)./(UBounds-LBounds);

%% lsq curve fit - current
ydata    = [ref(:,3)];
xdata    = 0*ydata;
opts     = optimset('Display','iter','TolFun',1e-4,'TolX',1e-4,'DiffMinChange',1e-3);
[p,res]  = lsqcurvefit(@(p0,xdata)fit_VSCM(p0,xdata,LBounds,UBounds,PARA,ref,fit,fitFlag),p0,xdata,ydata,LBounds*0,1+0*UBounds,opts);

%
params_calc = p.*(UBounds-LBounds) + LBounds;

for n=1:length(fit.para)
  PARA.(fit.para{n})  = params_calc(n);
end

% correction for contact resistance - get an average of the extracted and
% assign it to the source and drain Rc for same values
% rc = (PARA.rsc + PARA.rdc)/2;
% PARA.rsc = rc;
PARA.rdc_t = PARA.rsc_t;

%% Generate Id with the obtained parameters
Id_VS = [];
VDA = [0.05,0.5,1.0];
for VIndex = 1:length(VDA)
    Id_VS(:,VIndex) = VS_v4(PARA,ivElpa(VIndex).V_g,0,VDA(VIndex));
end

% Transfer characteristics linear
h = figure(2);
clf(h);
plot(Vg,Id*1e6,'ok');
hold on
plot(Vg,Id_VS*1e6,'-r');
% legend(plotList,{'VSCM at V_{DS} = 0.5 V','BSIM3 at V_{DS} = 0.5 V','VSCM at V_{DS} = 1 V','BSIM3 at V_{DS} = 1 V'},...
%         'Location','best');
preProcess(h,'$V_{GS}\,/\mathrm{V}$','$I_{D}\,/\mathrm{\mu A}$');
%%
plotFolder = '..\..\Report\fig\DC_AC_char';
plotFileName = 'Id_trans_180nm_MOSFET_VSCM_BSIM';
if saveFlag
    postProcess(h,plotFolder,plotFileName);
end

%% Transfer characteristics log scale
h = figure(3);
clf(h);
semilogy(Vg,Id,'ok');
hold on
semilogy(Vg,Id_VS,'-r');
preProcess(h,'$V_{GS}\,/\mathrm{V}$','$I_{D}\,/\mathrm{A}$');
axis([0 1.5 1e-14 1e-4]);
%%
plotFolder = '..\..\Report\fig\DC_AC_char';
plotFileName = 'Id_trans_180nm_MOSFET_VSCM_BSIM';
if saveFlag
    postProcess(h,plotFolder,plotFileName);
end

%% Transconductance
h = figure(4);
clf(h);
plot(Vg(1:end-1,:),diff(Id)./diff(Vg),'ok');
hold on
plot(Vg(1:end-1,:),diff(Id_VS)./diff(Vg),'-r');
ylimVal = ylim;
ylim([0 ylimVal(2)]);

%% Get Qch values from simulation
compactModelDir = pwd;

pa = 'D:\mannamalai\Projects\CNT_charge_model\simulations\COOS';
cd(pa);

clear vgs Qch_vgs

name   = 'CNTFET_BPeng_bGa';
VDA = [0.05,0.5,1.0];
transport = 'dd';
for VIndex = 1:length(VDA)
    VdsString = regexp(sprintf('%1.2f',abs(VDA(VIndex))) ,'\.','split');

    OUTPUT_name         = [name,'_Vds',VdsString{1},'p',VdsString{2},'V'];

    ivElpaFile = [name,'/',OUTPUT_name,'/',OUTPUT_name,'_',transport,'_iv.elpa'];
    ivElpa(VIndex) = read_elpa(ivElpaFile);
    
    capFile = [name,'/',OUTPUT_name,'/',OUTPUT_name,'_capacitance.elpa'];
    capElpa = read_elpa(capFile);
    
    vgs{VIndex} = ivElpa(VIndex).V_g;
    Qch_vgs{VIndex} = abs(ivElpa(VIndex).Q_ch)*1e18;
end

cd(compactModelDir);

%% 
VDA = [0.05,0.5,1.0];
clear Vg Vd Qch
for VIndex = 1:length(VDA)
    Vg(:,VIndex) = ivElpa(VIndex).V_g;
    Qch(:,VIndex) = abs(ivElpa(VIndex).Q_ch);
    Vd(:,VIndex) = repmat(VDA(VIndex),size(Vg(:,VIndex),1),1);
end 


%% Extract values for Vgs dependent parameters for Qch

ref1=[];ref2=[];ref3=[];ref=[];

% for j = 1:length(VG)
%     ref1 = [ref1;ones(length(vds{j}(1:2:end)),1)*(VG(j)+0.2)];
%     ref2 = [ref2;vds{j}(1:2:end)];
%     ref3 = [ref3;id{j}(1:2:end)];
% end
for j = 1:length(VDA)
   vgsvec = vgs{j}(1:1:end);
   qchvec  = Qch_vgs{j}(1:1:end);
   ref1 = [ref1;vgsvec(vgsvec>-1)];
   ref2 = [ref2;ones(length(vgsvec(vgsvec>-1)),1)*VDA(j)];
   ref3 = [ref3;qchvec(vgsvec>-1)];
end

ref(:,1) = ref1;
ref(:,2) = ref2;
ref(:,3) = ref3;

%
fitFlag = 2; % fit flag for current and charge: 1-current, 2-charge

lowBoundReFit   = 0.8;
highBoundReFit  = 1.2;

% fit of current parameters 
LBounds = zeros(1,1);
UBounds = zeros(1,1);
params  = zeros(1,1);
fit = [];
n = 0;
% PARA.vth0 = 0.55;
n=n+1; fit.para{n}='cq_t';        params(n) = PARA.cq_t;        LBounds(n)=PARA.cq_t*lowBoundReFit;    UBounds(n)=PARA.cq_t*highBoundReFit;
n=n+1; fit.para{n}='alpha';     params(n) = PARA.alpha;     LBounds(n)=PARA.alpha*lowBoundReFit;  UBounds(n)=PARA.alpha*highBoundReFit;
n=n+1; fit.para{n}='beta';     params(n) = PARA.beta;     LBounds(n)=PARA.beta*lowBoundReFit;  UBounds(n)=PARA.beta*highBoundReFit;
n=n+1; fit.para{n}='gamma';     params(n) = PARA.gamma;     LBounds(n)=PARA.gamma*lowBoundReFit;  UBounds(n)=PARA.gamma*highBoundReFit;
n=n+1; fit.para{n}='fb';        params(n) = PARA.fb;        LBounds(n)=1e-2;    UBounds(n)=2;
n=n+1; fit.para{n}='dvthq';     params(n) = PARA.dvthq;     LBounds(n)=-0.5; UBounds(n)=0.5;

p0        = (params-LBounds)./(UBounds-LBounds);

% lsq curve fit - charge
ydata    = [ref(:,3)];
xdata    = 0*ydata;
opts     = optimset('Display','iter','TolFun',1e-6,'TolX',1e-6,'DiffMinChange',1e-4);
[p,res]  = lsqcurvefit(@(p0,xdata)fit_VSCM(p0,xdata,LBounds,UBounds,PARA,ref,fit,fitFlag),p0,xdata,ydata,LBounds*0,1+0*UBounds,opts);

params_calc = p.*(UBounds-LBounds) + LBounds;

for n=1:length(fit.para)
  PARA.(fit.para{n})  = params_calc(n);
end
%%
Qs_VS = [];
Qd_VS = [];

for VIndex = 1:length(VDA)
    [dummy,Qs_VS(:,VIndex),Qd_VS(:,VIndex)] = VS_v4(PARA,ivElpa(VIndex).V_g,0,VDA(VIndex));
end

Qch_VS = Qs_VS + Qd_VS;

h = figure(5);
clf(h);
plot(Vg,Qch,'ok');
hold on
plot(Vg,Qch_VS,'-r');

%
h = figure(6);
clf(h);
plot(Vg(1:end-1,:),diff(Qch)./diff(Vg),'ok');
hold on
plot(Vg(1:end-1,:),diff(Qch_VS)./diff(Vg),'-r');
ylimVal = ylim;
ylim([0 ylimVal(2)]);

%% Fit both current and charge simultaneously


ref1=[];ref2=[];ref3=[];ref4=[];ref=[];

% for j = 1:length(VG)
%     ref1 = [ref1;ones(length(vds{j}(1:2:end)),1)*(VG(j)+0.2)];
%     ref2 = [ref2;vds{j}(1:2:end)];
%     ref3 = [ref3;id{j}(1:2:end)];
% end
for j = 1:length(VDA)
   vgsvec = vgs{j}(1:1:end);
   idvec  = id_vgs{j}(1:1:end);
   qchvec  = Qch_vgs{j}(1:1:end);
   ref1 = [ref1;vgsvec(vgsvec>-1)];
   ref2 = [ref2;ones(length(vgsvec(vgsvec>-1)),1)*VDA(j)];
   ref3 = [ref3;idvec(vgsvec>-1)];
   ref4 = [ref4;qchvec(vgsvec>-1)];
end

ref(:,1) = [ref1;ref1];
ref(:,2) = [ref2;ref2];
ref(:,3) = [ref3;ref4];

%%
fitFlag = 3;


lowBoundReFit   = 0.7;
highBoundReFit  = 1.3;

% fit of current parameters 
LBounds = zeros(1,1);
UBounds = zeros(1,1);
params  = zeros(1,1);
fit = [];
n = 0;
% PARA.vth0 = 0.55;
n=n+1; fit.para{n}='vth0';      params(n) = PARA.vth0;      LBounds(n)=0.35;                         UBounds(n)=0.45;
n=n+1; fit.para{n}='dibl';      params(n) = PARA.dibl;      LBounds(n)=1e-3;                        UBounds(n)=1;
n=n+1; fit.para{n}='nss';       params(n) = PARA.nss;       LBounds(n)=1;                           UBounds(n)=1.2;
n=n+1; fit.para{n}='cq_t';      params(n) = PARA.cq_t;      LBounds(n)=1e-13;                           UBounds(n)=1e-9;
n=n+1; fit.para{n}='meff';      params(n) = PARA.meff;      LBounds(n)=1e-2;                        UBounds(n)=0.07;
n=n+1; fit.para{n}='alpha';     params(n) = PARA.alpha;     LBounds(n)=3.1;                         UBounds(n)=3.9;
n=n+1; fit.para{n}='beta';      params(n) = PARA.beta;      LBounds(n)=1.5;                         UBounds(n)=2.5;
n=n+1; fit.para{n}='gamma';     params(n) = PARA.gamma;     LBounds(n)=1e-3;                        UBounds(n)=1;
n=n+1; fit.para{n}='lambda0';   params(n) = PARA.lambda0;   LBounds(n)=10e-9;                       UBounds(n)=200e-9;
n=n+1; fit.para{n}='lfl';       params(n) = PARA.lfl;       LBounds(n)=1e-10;                       UBounds(n)=10e-9;
n=n+1; fit.para{n}='vball';     params(n) = PARA.vball;     LBounds(n)=1e5;                         UBounds(n)=1.1e7;
n=n+1; fit.para{n}='rsc_t';     params(n) = PARA.rsc_t;     LBounds(n)=Rc_YFMpp/2;                  UBounds(n)=Rc_YFMpp;
n=n+1; fit.para{n}='vthd';        params(n) = PARA.vthd;    LBounds(n)=-1;                        UBounds(n)=1;
n=n+1; fit.para{n}='beta_ch';      params(n) = PARA.beta_ch;LBounds(n)=1.2;                         UBounds(n)=2.1;
n=n+1; fit.para{n}='fb';        params(n) = PARA.fb;        LBounds(n)=1e-2;                        UBounds(n)=0.5;

p0        = (params-LBounds)./(UBounds-LBounds);

% lsq curve fit - charge
ydata    = [ref(:,3)];
xdata    = 0*ydata;
opts     = optimset('Display','iter','TolFun',1e-4,'TolX',1e-4,'DiffMinChange',1e-3);
[p,res]  = lsqcurvefit(@(p0,xdata)fit_VSCM(p0,xdata,LBounds,UBounds,PARA,ref,fit,fitFlag),p0,xdata,ydata,LBounds*0,1+0*UBounds,opts);

params_calc = p.*(UBounds-LBounds) + LBounds;

for n=1:length(fit.para)
  PARA.(fit.para{n})  = params_calc(n);
end

PARA.rdc_t = PARA.rsc_t;

%% Generate Id with the obtained parameters


Id_VS = [];
Qs_VS = [];
Qd_VS = [];

for VIndex = 1:length(VDA)
    [Id_VS(:,VIndex),Qs_VS(:,VIndex),Qd_VS(:,VIndex)] = VS_v4(PARA,ivElpa(VIndex).V_g,0,VDA(VIndex));
end

Qch_VS = Qs_VS + Qd_VS;

% Transfer characteristics linear scale
h = figure(2);
clf(h);
clear plotList;
plotList(1:length(VDA)) = plot(Vg,Id*1e6,'ok');
hold on
plotList(length(VDA)+1:2*length(VDA)) = plot(Vg,Id_VS*1e6,'-r');
legend([plotList(1),plotList(length(VDA)+1)],{'Sim.','VSCM'},'Location','best');
annotation(h,'arrow',[0.72 0.72],[0.16 0.72]);
annotation(h,'textbox',[0.4 0.72 0.4 0.07],'String','V_{ds}/V = 0.05,0.5,1.0','LineStyle','none','FitBoxToText','off');
preProcess(h,'$V_{gs}\,/\mathrm{V}$','$I_{D}\,/\mathrm{\mu A}$');

% Transfer characteristics log scale
h = figure(3);
clf(h);
plotList(1:length(VDA)) = semilogy(Vg,Id,'ok');
hold on
plotList(length(VDA)+1:2*length(VDA)) = semilogy(Vg,Id_VS,'-r');
legend([plotList(1),plotList(length(VDA)+1)],{'Sim.','VSCM'},'Location','best');
annotation(h,'arrow',[0.75 0.75],[0.7 0.9]);
annotation(h,'textbox',[0.34 0.85 0.4 0.07],'String','V_{ds}/V = 0.05,0.5,1.0','LineStyle','none','FitBoxToText','off');
preProcess(h,'$V_{gs}\,/\mathrm{V}$','$I_{D}\,/\mathrm{A}$');
axis([0 1.5 1e-14 1e-4]);


% Transconductance
h = figure(4);
clf(h);
plotList(1:length(VDA)) = plot(Vg(1:end-1,:),diff(Id)./diff(Vg)*1e6,'ok');
hold on
plotList(length(VDA)+1:2*length(VDA)) = plot(Vg(1:end-1,:),diff(Id_VS)./diff(Vg)*1e6,'-r');
legend([plotList(1),plotList(length(VDA)+1)],{'Sim.','VSCM'},'Location','best');
ylimVal = ylim;
ylim([0 ylimVal(2)]);
annotation(h,'arrow',[0.7 0.7],[0.18 0.73]);
annotation(h,'textbox',[0.36 0.75 0.4 0.07],'String','V_{ds}/V = 0.05,0.5,1.0','LineStyle','none','FitBoxToText','off');
preProcess(h,'$V_{gs}\,/\mathrm{V}$','$g_{m}\,/\mathrm{\mu S}$');
%
% Charge
h = figure(5);
clf(h);
plotList(1:length(VDA)) = plot(Vg,Qch*1e18,'ok');
hold on
plotList(length(VDA)+1:2*length(VDA)) = plot(Vg,Qch_VS*1e18,'-r');
legend([plotList(1),plotList(length(VDA)+1)],{'Sim.','VSCM'},'Location','best');
annotation(h,'arrow',[0.72 0.72],[0.75 0.25]);
annotation(h,'textbox',[0.5 0.15 0.4 0.07],'String','V_{ds}/V = 0.05,0.5,1.0','LineStyle','none','FitBoxToText','off');
preProcess(h,'$V_{gs}\,/\mathrm{V}$','$Q_{ch}\,/\mathrm{aC}$');

% Charge - semilog
h = figure(6);
clf(h);
plotList(1:length(VDA)) = semilogy(Vg,Qch*1e18,'ok');
hold on
plotList(length(VDA)+1:2*length(VDA)) = semilogy(Vg,Qch_VS*1e18,'-r');
legend([plotList(1),plotList(length(VDA)+1)],{'Sim.','VSCM'},'Location','best');
annotation(h,'arrow',[0.72 0.72],[0.75 0.25]);
annotation(h,'textbox',[0.5 0.15 0.4 0.07],'String','V_{ds}/V = 0.05,0.5,1.0','LineStyle','none','FitBoxToText','off');
preProcess(h,'$V_{gs}\,/\mathrm{V}$','$Q_{ch}\,/\mathrm{aC}$');

% capacitance
h = figure(7);
clf(h);
plotList(1:length(VDA)) = plot(Vg(1:end-1,:),diff(Qch)./diff(Vg)*1e18,'-.ok');
hold on
plotList(length(VDA)+1:2*length(VDA)) = plot(Vg(1:end-1,:),diff(Qch_VS)./diff(Vg)*1e18,'-r');
legend([plotList(1),plotList(length(VDA)+1)],{'Sim.','VSCM'},'Location','best');
ylimVal = ylim;
ylim([0 ylimVal(2)]);
annotation(h,'arrow',[0.52 0.52],[0.9 0.35]);
annotation(h,'textbox',[0.4 0.3 0.4 0.07],'String','V_{ds}/V = 0.05,0.5,1.0','LineStyle','none','FitBoxToText','off');
preProcess(h,'$V_{gs}\,/\mathrm{V}$','$C_{gg,i}\,/\mathrm{aF}$');

% Intrinsic fT
h = figure(8);
clf(h);
gm      = diff(Id)./diff(Vg);
gm_VS   = diff(Id_VS)./diff(Vg);
Cggi    = diff(Qch)./diff(Vg);
Cggi_VS = diff(Qch_VS)./diff(Vg);
plotList(1:length(VDA)) = plot(Vg(1:end-1,:),gm./(2*pi*Cggi)*1e-9,':ok');
hold on
plotList(length(VDA)+1:2*length(VDA)) = plot(Vg(1:end-1,:),gm_VS./(2*pi*Cggi_VS)*1e-9,'-r');
legend([plotList(1),plotList(length(VDA)+1)],{'Sim.','VSCM'},'Location','best');
% ylimVal = ylim;
ylim([0 350]);
annotation(h,'arrow',[0.8 0.8],[0.16 0.8]);
annotation(h,'textbox',[0.5 0.85 0.4 0.07],'String','V_{ds}/V = 0.05,0.5,1.0','LineStyle','none','FitBoxToText','off');
preProcess(h,'$V_{gs}\,/\mathrm{V}$','$f_{T,i}\,/\mathrm{GHz}$');

%% Post process images

figDir = 'D:\mannamalai\Projects\CNT_charge_model\report\fig\v1';

figFileName = 'Id_trans_bGa_Bpeng_100nm_CNTFET_VSCM';
postProcess(figure(2),figDir,figFileName);

figFileName = 'Id_trans_log_bGa_Bpeng_100nm_CNTFET_VSCM';
postProcess(figure(3),figDir,figFileName);

figFileName = 'gm_bGa_Bpeng_100nm_CNTFET_VSCM';
postProcess(figure(4),figDir,figFileName);

figFileName = 'Qch_vs_Vgs_bGa_Bpeng_100nm_CNTFET_VSCM';
postProcess(figure(5),figDir,figFileName);

figFileName = 'Cggi_vs_Vgs_bGa_Bpeng_100nm_CNTFET_VSCM';
postProcess(figure(7),figDir,figFileName);
%%
figFileName = 'fTi_vs_Vgs_bGa_Bpeng_100nm_CNTFET_VSCM';
postProcess(figure(8),figDir,figFileName);


%% Save parameter in MAT file 

save('BPeng_bGa_VS_v4.mat','PARA');

%% fit gm and Cgg

ref1=[];ref2=[];ref3=[];ref4=[];ref=[];
gm  = [];
Cgg = [];

% for j = 1:length(VG)
%     ref1 = [ref1;ones(length(vds{j}(1:2:end)),1)*(VG(j)+0.2)];
%     ref2 = [ref2;vds{j}(1:2:end)];
%     ref3 = [ref3;id{j}(1:2:end)];
% end
for j = 1:length(VDA)
   vgsvec = vgs{j}(1:1:end);
   idvec  = id_vgs{j}(1:1:end);
   qchvec  = Qch_vgs{j}(1:1:end);
   ref1 = [ref1;vgsvec(vgsvec>-1)];
   ref2 = [ref2;ones(length(vgsvec(vgsvec>-1)),1)*VDA(j)];
   ref3 = [ref3;idvec(vgsvec>-1)];
   ref4 = [ref4;qchvec(vgsvec>-1)];
   gm = [gm;total_diff(idvec(vgsvec>-1),vgsvec(vgsvec>-1))];
   Cgg = [Cgg;total_diff(qchvec(vgsvec>-1),vgsvec(vgsvec>-1))];
end

gm(gm < 1e-3) = 1e-6;
Cgg(Cgg < 1e-3) = 1e-6;

ref(:,1) = [ref1;ref1];
ref(:,2) = [ref2;ref2];
ref(:,3) = [gm;Cgg];

%%

fitFlag = 4;


lowBoundReFit   = 0.7;
highBoundReFit  = 1.3;

% fit of current parameters 
LBounds = zeros(1,1);
UBounds = zeros(1,1);
params  = zeros(1,1);
fit = [];
n = 0;
% PARA.vth0 = 0.55;
n=n+1; fit.para{n}='vth0';      params(n) = PARA.vth0;      LBounds(n)=0.2;                         UBounds(n)=0.35;
n=n+1; fit.para{n}='dibl';      params(n) = PARA.dibl;      LBounds(n)=1e-3;              UBounds(n)=1;
n=n+1; fit.para{n}='nss';       params(n) = PARA.nss;       LBounds(n)=1;               UBounds(n)=1.5;
n=n+1; fit.para{n}='cq_t';      params(n) = PARA.cq_t;      LBounds(n)=0;                           UBounds(n)=1e-9;
n=n+1; fit.para{n}='meff';      params(n) = PARA.meff;      LBounds(n)=1e-2;                        UBounds(n)=0.5;
n=n+1; fit.para{n}='alpha';     params(n) = PARA.alpha;     LBounds(n)=1;                           UBounds(n)=4;
n=n+1; fit.para{n}='beta';      params(n) = PARA.beta;      LBounds(n)=1.5;                         UBounds(n)=2.4;
n=n+1; fit.para{n}='gamma';     params(n) = PARA.gamma;     LBounds(n)=1e-3;                        UBounds(n)=1;
n=n+1; fit.para{n}='lambda0';   params(n) = PARA.lambda0;   LBounds(n)=10e-9;                       UBounds(n)=100e-9;
n=n+1; fit.para{n}='lfl';       params(n) = PARA.lfl;       LBounds(n)=1e-10;                       UBounds(n)=1e-9;
n=n+1; fit.para{n}='vball';     params(n) = PARA.vball;     LBounds(n)=1e5;                         UBounds(n)=1e7;
n=n+1; fit.para{n}='rsc_t';     params(n) = PARA.rsc_t;     LBounds(n)=Rc_YFMpp/2;                    UBounds(n)=Rc_YFMpp*2;
% n=n+1; fit.para{n}='va';        params(n) = PARA.va;        LBounds(n)=1e-2;                           UBounds(n)=10;
% n=n+1; fit.para{n}='nd';        params(n) = PARA.nd;        LBounds(n)=0;                           UBounds(n)=10;
n=n+1; fit.para{n}='fb';        params(n) = PARA.fb;        LBounds(n)=1e-2;                        UBounds(n)=2;
n=n+1; fit.para{n}='dvthq';     params(n) = PARA.dvthq;     LBounds(n)=-0.5;                        UBounds(n)=0.5;

p0        = (params-LBounds)./(UBounds-LBounds);

% lsq curve fit - charge
ydata    = [ref(:,3)];
xdata    = 0*ydata;
opts     = optimset('Display','iter','TolFun',1e-4,'TolX',1e-4,'DiffMinChange',1e-3);
[p,res]  = lsqcurvefit(@(p0,xdata)fit_VSCM(p0,xdata,LBounds,UBounds,PARA,ref,fit,fitFlag),p0,xdata,ydata,LBounds*0,1+0*UBounds,opts);

params_calc = p.*(UBounds-LBounds) + LBounds;

for n=1:length(fit.para)
  PARA.(fit.para{n})  = params_calc(n);
end

PARA.rdc_t = PARA.rsc_t;

