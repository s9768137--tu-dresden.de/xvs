function funVal = fit_VSCM(params01,xdata,LBounds, UBounds,para,ref,fit,fitFlag,freq)

% Objective function defined as sum of square distance to
% the measured values from the experiments
% weighted since it is more important that the upper curves fit
% parameters are in [0,1] and will be mapped in lower and upper bounds
% first

params = params01.*(UBounds-LBounds)+LBounds;
% update variable parameters

for n=1:length(fit.para)
  para.(fit.para{n})  = params(n);
end

para.rdc_t = para.rsc_t;

% ID = zeros(size(ref,1),1);
% qs = zeros(size(ref,1),1);
% qd = zeros(size(ref,1),1);

Vg = unique(ref(:,1));
Vd = unique(ref(:,2));

if fitFlag == 1
    ID_large=[];
    for i=1:length(Vd)
        [ID] = VS_v4(para,Vg,0,Vd(i),freq);
        ID_large = [ID_large;ID(:,i)];
    end
    funVal = ID_large*1e3;
elseif fitFlag == 2
    gm_large=[];
    for i=1:length(Vd)
        [ID] = VS_v4(para,Vg,0,Vd(i),freq);
        gm(:,i) = gradient(ID)./gradient(Vg);
        gm_large = [gm_large;gm(:,i)];
    end
    funVal = gm_large*1e3;
elseif fitFlag == 3
    clear YP SP ZP;
    gm_large=[]; Cgs_large=[]; Cgd_large=[]; Cds_large=[];
    for i=1:length(Vd)
        [ID,~,~,YP(i),SP(i),ZP] = VS_v4(para,Vg,0,Vd(i),freq);
        gm(:,i) = gradient(ID)./gradient(Vg);
        gm_large = [gm_large;gm(:,i)];
        [~,~,Cgs(:,i),Cgd(:,i),Cds(:,i)] = calc_freq_fom_fit(YP{1,i},SP{1,i},ZP,Vd(i));
        Cgs_large = [Cgs_large;Cgs(:,i)];
        Cgd_large = [Cgd_large;Cgd(:,i)];
        Cds_large = [Cds_large;Cds(:,i)];
    end
    funVal = [gm_large*1e3; Cgs_large*1e15; Cgd_large*1e15; Cds_large*1e15];
elseif fitFlag == 4
    clear YP SP ZP;
    Cgs_large=[]; Cgd_large=[]; Cds_large=[];
    for i=1:length(Vd)
        [ID,~,~,YP(i),SP(i),ZP] = VS_v4(para,Vg,0,Vd(i),freq);
        [~,~,Cgs(:,i),Cgd(:,i),Cds(:,i)] = calc_freq_fom_fit(YP{1,i},SP{1,i},ZP,Vd(i));
        Cgs_large = [Cgs_large;Cgs(:,i)];
        Cgd_large = [Cgd_large;Cgd(:,i)];
        Cds_large = [Cds_large;Cds(:,i)];
    end
    funVal = [Cgs_large*1e15; Cgd_large*1e15; Cds_large*1e15];
end

end