function funVal = fit_CCAM_ADS(params01,xdata,LBounds,UBounds,para,Vg,VDA,freq,fit,fitFlag)

params = params01.*(UBounds-LBounds)+LBounds;
% update variable parameters

for n=1:length(fit.para)
  para.(fit.para{n})  = params(n);
end

para.rdcs = para.rscs;

set_ADS_conf;
ADS_conf.va_dir  = 'D:\\mannamalai\\GitLab\\CompactModels\\VS\\';
ADS_conf.va_file = 'VS_v4'; 
ADS_conf.va_fun  = 'mosfet'; % compact model function name

ID = [];

if fitFlag == 1 % DC characteristics
    for VIndex = 1:length(VDA)
        sim = run_DC_sim(para,Vg,VDA(VIndex),'verilog',ADS_conf);
        ID = [ID;sim.id];
    end
    funVal = ID*1e6;
elseif fitFlag == 2 % DC characteristics
    for VIndex = 1:length(VDA)
        sim = run_DC_sim(para,Vg,VDA(VIndex),'verilog',ADS_conf);
        ID = [ID;sim.id];
    end
    funVal = (gradient(ID)./gradient(Vg))*1e6;
elseif fitFlag == 3 % AC characteristics
    ADS_conf.fstep = 1;
    ADS_conf.freq1 = freq;
    ADS_conf.freq2 = freq;
    fT_large=[];fmax_large=[];
    [YP,SP,ZP]= run_YP_sim_fit(para,Vg,VDA,'verilog',ADS_conf);
    [ft,fmax] = calc_freq_fom_fit(YP,SP,ZP,VDA);
    fT_large=[]; fmax_large=[];
    for VIndex = 1:length(VDA)
        fT_large = [fT_large;ft(:,VIndex)];
        fmax_large = [fmax_large;fmax(:,VIndex)];
    end

    funVal = real([fT_large*1e-9;fmax_large*1e-9]);
elseif fitFlag == 4
    ADS_conf.fstep = 1;
    ADS_conf.freq1 = freq;
    ADS_conf.freq2 = freq;
    fT_large=[];fmax_large=[];
    [YP,SP,ZP]= run_YP_sim_fit(para,Vg,VDA,'verilog',ADS_conf);
    [ft,fmax] = calc_freq_fom_fit(YP,SP,ZP,VDA);
    fT_large=[]; fmax_large=[];
    for VIndex = 1:length(VDA)
        fT_large = [fT_large;ft(:,VIndex)];
        fmax_large = [fmax_large;fmax(:,VIndex)];
    end

    funVal = real([fT_large*1e-9]);
elseif fitFlag == 5
    ADS_conf.fstep = 1;
    ADS_conf.freq1 = freq;
    ADS_conf.freq2 = freq;
    fT_large=[];fmax_large=[];
    [YP,SP,ZP]= run_YP_sim_fit(para,Vg,VDA,'verilog',ADS_conf);
    [ft,fmax] = calc_freq_fom_fit(YP,SP,ZP,VDA);
    fT_large=[]; fmax_large=[];
    for VIndex = 1:length(VDA)
        fT_large = [fT_large;ft(:,VIndex)];
        fmax_large = [fmax_large;fmax(:,VIndex)];
    end

    funVal = real([fmax_large*1e-9]);
end

end