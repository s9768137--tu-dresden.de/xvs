\documentclass[fleqn,a4paper,twoside,11pt,openany]{scrartcl}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}      
\usepackage[protrusion=true,expansion=true]{microtype} % for better looks
\usepackage{graphicx}               % include images
%\usepackage{wrapfig}               % wrapping figures
\usepackage{caption}
\usepackage{amsmath}                % include formulas
\usepackage{amssymb}
\usepackage{mathptmx}
\usepackage{siunitx}	            % writing units with good style use \SI{}{} or \si{}
\usepackage[toc,header]{appendix}   % add appendix, see end of doc
%\usepackage{epstopdf}               % include .eps format for images
\usepackage{geometry}               % set layout
	\geometry{a4paper, top=20mm, left=30mm, right=25mm, bottom=17mm,
			headsep=10mm, footskip=12mm}			
\newcommand{\ra}[1]{\renewcommand{\arraystretch}{#1}}
\sloppy
\usepackage[section]{placeins}
% font style
\usepackage{lmodern}
%\renewcommand*\familydefault{\sfdefault} %% Only if the base font of the document is to be sans serif

% one and a half line spacing --> 1.3
\linespread{1.3}
\setlength\parindent{10pt}

%label
\setkomafont{labelinglabel}{~~\bfseries}

% tables 
\usepackage{longtable}
\usepackage{multirow}
\usepackage{booktabs}               % writing tables with good style
\newcolumntype{L}[1]{>{\raggedright\arraybackslash}p{#1}}
\newcolumntype{R}[1]{>{\raggedleft\arraybackslash}p{#1}}
\renewcommand{\tabcolsep}{10pt}

% enumerate all equations
\newcommand{\myequation}{\begin{equation}}
\newcommand{\myendequation}{\end{equation}}
\let\[\myequation
\let\]\myendequation

% labeling style
\setkomafont{labelinglabel}{~~\bfseries}

\usepackage{hyperref}		% for links
\usepackage{listings}		% for code
\lstset{
	basicstyle = \small\sffamily,
	breaklines = true,
	breakatwhitespace= true,
	frame = none,
	xleftmargin=1cm,
	escapeinside={*(}{)*}   % if you want to add LaTeX within your code
}


% Insert title, titlehead, subject and author here
\titlehead{\centering \textsc{\Large Technische Universität Dresden} \\
\vspace{2cm}
Chair for Electron Devices and Integrated Circuits
}
\subject{Manual - v01}
\title{Virtual Source Compact Model}
\author{Florian Wolf}
\date{\today} % sets date to date of compilation


\begin{document}


\maketitle
\tableofcontents{}


\newpage 

\section{Virtual Source model equations}

\begin{figure}[ht]
	\centering
	\includegraphics[width=0.7\textwidth]{fig/compact_model.pdf}
	\caption{Large signal equivalent circuit for the VS model}
\end{figure}

% ---------------------------
\subsection{Current}

The current of the 'Virtual Source Compact Model' (VSCM) depends on  the intrinsic voltages $V_\textup{GiSi}$ and $V_\textup{DiSi}$ and can be described in the following form \cite{khakifirooz2009simple}:
\begin{equation}
    I_\textup{vs} = W \bar{Q}_\textup{ix0} v_\textup{x0}  F_\textup{s}
\end{equation}
where $\bar{Q}_\textup{ix0}$ is the channel charge per area and $v_\textup{x0}$ is the carrier velocity at the virtual source point $x_0$. The width of the transistor is given by $W$. The channel charge is modeled with
\begin{align}
    \bar{Q}_\textup{ix0} &= \bar{C}_\textup{inv} n_\textup{ss} \phi_\textup{t} \ln \left[ 1 + \exp \left( \dfrac{V_\textup{GiSi} - V_\textup{th}' + \alpha \phi_\textup{t} F_\textup{f}}{n_\textup{ss} \phi_\textup{t}} \right) \right] \\
    V_\textup{th}' &= V_\textup{th} - \delta V_\textup{DiSi} \\
    F_\textup{f} &= \dfrac{1}{1 + \exp \left(  \dfrac{V_\textup{GiSi} - V_\textup{th}' + 0.5 \alpha \phi_\textup{t} }{\alpha \phi_\textup{t}} \right) }\\
\phi_\textup{t} &= \frac{k_\textup{B}T}{q}
\end{align}
where $\bar{C}_\textup{inv}$ is the inversion capacitance per area, $n_\textup{ss}$ a parameter for the subthreshold slope, $\phi_\textup{t}$ the thermal voltage, $ V_\textup{T0}$ a parameter for the threshold voltage, $\alpha$ a fitting parameter and $\delta$ a parameter for the DIBL effect. The drain-source voltage dependence is modeled with $F_\textup{s}$ in a well known manner
\begin{align}
    F_\textup{s} &= \dfrac{u_\textup{ds}}{\left( 1 + u_\textup{ds}^\beta \right)^{1/\beta} } \\
    u_\textup{ds} &= \dfrac{V_\textup{DiSi}}{V_\textup{D,sat}} \\
    V_\textup{D,sat} &= V_\textup{D,sat,s} (1-F_\textup{f}) + \phi_\textup{t} F_\textup{f} \\
    V_\textup{D,sat,s} &= \dfrac{v_\textup{x0} L_\textup{g}}{\mu}
\end{align}
where $\mu$  is the low field channel mobility and $\beta$ a fitting parameter. $L_\textup{g}$ is the gate length.


% ---------------------------
\subsection{Charge}


The complete channel charge $Q_\textup{ch}$ follows from the integration over the channel area and gives for $\bar{Q_\textup{i}}(x)$ constant:
\begin{equation}
    Q_\textup{ch} = W L_\textup{g} \bar{Q}_\textup{ix0}
\end{equation}
The split into drain and source related charge \cite{wei2012virtual}
\begin{align}
    Q_\textup{D} &= Q_\textup{d,DD} (1-F_\textup{sat,q}) + Q_\textup{d,B} F_\textup{sat,q} \\ 
    Q_\textup{S} &= Q_\textup{s,DD} (1-F_\textup{sat,q}) + Q_\textup{s,B} F_\textup{sat,q} 
\end{align}
with charges either described in a classical drift-diffusion approach ($Q_\textup{s/d,DD}$) or in a ballistic approach ($Q_\textup{s/d,B}$).
$F_\textup{satq}$ allows a smooth transition.
\begin{align}
    Q_\textup{s,B} &= Q_\textup{ch} \left[ \dfrac{sinh^{-1} \sqrt{k_\textup{q}}}{\sqrt{k_\textup{q}}} - \dfrac{\sqrt{k_\textup{q}+1} - 1}{k_\textup{q}}\right] \\
    Q_\textup{d,B} &= Q_\textup{ch} \left[ \dfrac{\sqrt{k_\textup{q}+1} - 1}{k_\textup{q}}\right]    
\end{align}
with
\begin{equation}
    k_\textup{q} = \dfrac{2 q V_\textup{DSi}}{m^* v_\textup{x0}^2}
\end{equation}
where the effective mass $m^*$ is a fitting parameter and $q$ the electron charge. 
These equations are introduced in \cite{wei2012virtual}. 
The 'classic' drift-diffusion charge is modeled with empirical polynomial functions as follows \cite{tsividis2011operation}:
\begin{align}
    Q_\textup{s,DD} &= Q_\textup{ch} \dfrac{6 + 12 \eta + 8 \eta^2 + 4 \eta^3}{15 (1+\eta)^2} \\
    Q_\textup{d,DD} &= Q_\textup{ch} \dfrac{4 + 8 \eta + 12 \eta^2 + 6 \eta^3}{15 (1+\eta)^2}    
\end{align}
where 
\begin{align}
    \eta &= 1 -F_\textup{sat,q} \\
    F_\textup{sat,q} &= \dfrac{V_\textup{DSi}/V_\textup{sat,q}}{\left[ 1 + (V_\textup{DSi}/V_\textup{sat,q})^\beta \right]^{1/\beta}} \\
    V_\textup{sat,q} &= \sqrt{F_\textup{f} (\alpha \phi_\textup{t})^2 + (\bar{Q}_\textup{ix0}/\bar{C}_\textup{inv})^2}
\end{align}



% ******************************************
\section{Parameter extraction strategy}


The following steps should be used to extract the parameter values for the virtual source model:
\begin{enumerate}
    \item W is the channel width, $L_\textup{ch}$ the effective channel length (design parameters)
    \item $n_\textup{ss} = \dfrac{S}{\phi_\textup{t} \textup{ln}(10)}$ with the "sub-threshold swing" (sub-threshold slope) $S = \left( \dfrac{\textup{d log}(I_\textup{D})}{d V_\textup{GS}} \right)^{-1}$
    \item $V_\textup{T0}$ is the strong inversion threshold voltage ($ = V_\textup{th}^\textup{DD}$)
    \item parasitic capacitances can be extracted from y-parameters (Bulk is included in Source):
    \begin{itemize}
        \item $C_\textup{gd} = \dfrac{\Im\{Y_\textup{21}\}}{2 \pi f} \bigg|_{V_\textup{GS} = 0}$
        \item $C_\textup{gs} = \dfrac{\Im\{Y_\textup{11}\}}{2 \pi f} \bigg|_{V_\textup{GS} = 0} - C_\textup{gd}$
        \item $C_\textup{ds} = \dfrac{\Im\{Y_\textup{22}\}}{2 \pi f} \bigg|_{V_\textup{GS} = 0} - C_\textup{gd}$
    \end{itemize}
    \item $\bar{C}_\textup{inv}$ is the effective gate-to-channel capacitance per unit
area in strong inversion calculated as $\bar{C}_\textup{inv} = (2\pi f \Im\{Z_\textup{in}\})^{-1} - (C_\textup{gd} + C_\textup{gs})$
    \item $\delta = \dfrac{V_\textup{th}^\textup{DD} - V_\textup{th}^\textup{low}}{V_\textup{DD}-V_\textup{low}}$ is the factor accounting for the DIBL effect 
    \item fit of $v_\textup{x0}$, $R_\textup{S/D}$, $\mu$, $m^*$ (and $\beta$) using 'lsqcurvefit'
\end{enumerate}

while $\alpha$ is fixed to 3.5 and $\beta$ is approximately 1.8 for nFET and 1.4 for pFET.


% ------------------------------------------------------
\section{Parameters}

\begin{center}
    \begin{longtable}{c | c | c | c | L{5cm}}
    \toprule
     Parameter & Name & Unit & Default & Comment \\
     \midrule
     \midrule
        $W$             & w & \si{\meter} & 1.83e-6 & total transistor width\\
        $L_\textup{g}$  & lg & \si{\meter} & 35e-9 & channel length \\        
            -     & type & - & 1 & 1 for n-type, -1 for p-type \\
        $\bar{C}_\textup{inv}$ & cinv & \si{\farad\per\square\meter} & 0.001 & inversion capacitance \\
        $V_\textup{th}$ & vth & \si{\volt} & 0.3 & threshold voltage @ $V_\textup{DS} = 0 V$\\
        $\delta$        & dibl &\si{\volt\per\volt} & 0.12 & DIBL effect parameter \\
        $n_\textup{ss}$ & nss & - & 1 & normalized sub-threshold slope\\
        $\alpha$        & alpha & - & 3.5 & \\
        $\mu$           & mu & \si{\square\meter\per\volt\per\second} & 0.025 & low field mobility \\
        $\beta$         & beta & - & 1.8 &  \\
        $v_\textup{x0}$ & vxo & \si{\meter\per\second} & 1.4e5 & virtual source velocity \\
        $R_\textup{sc}$  & rsc & \si{\ohm} & 75 &  source contact resistance \\
        $R_\textup{dc}$  & rdc & \si{\ohm} & 75 & drain contact resistance \\
        $R_\textup{g}$  & rg & \si{\ohm} & 75 & total gate resistance \\   
        $R_\textup{sf}$  & rsf & \si{\ohm} & 1 & source finger resistance \\   
        $R_\textup{df}$  & rdf & \si{\ohm} & 1 & drain finger resistance \\   
        $C_\textup{gs,par}$ & cgspar & \si{\farad} & 2.2e-14 & parasitic gate source capacitance 1\\
        $C_\textup{gd,par}$ & cgdpar & \si{\farad} & 2.2e-14 & parasitic gate drain capacitance 1\\
        $C_\textup{gs,par2}$ & cgspar2 & \si{\farad} & 2.2e-14 & parasitic gate source capacitance 2\\
        $C_\textup{gd,par2}$ & cgdpar2 & \si{\farad} & 2.2e-14 & parasitic gate drain capacitance 2\\
        $C_\textup{ds,par}$ & cdspar & \si{\farad} & 2e-15 & parasitic drain source capacitance\\  
     \bottomrule
    \end{longtable}
\end{center}



%\newpage
%------------------------------------------------------------
%--- References
%------------------------------------------------------------
\addcontentsline{toc}{section}{Bibliography}
\bibliographystyle{unsrt}
% Bibliography file
\bibliography{manual}
  
  
\end{document}

