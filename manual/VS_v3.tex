\documentclass[fleqn,a4paper,twoside,11pt,openany]{scrartcl}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}      
\usepackage[protrusion=true,expansion=true]{microtype} % for better looks
\usepackage{graphicx}               % include images
%\usepackage{wrapfig}               % wrapping figures
\usepackage{caption}
\usepackage{amsmath}                % include formulas
\usepackage{amssymb}
\usepackage{mathptmx}
\usepackage{upgreek}				% writing upright greek letters
\usepackage{siunitx}	            % writing units with good style use \SI{}{} or \si{}
\usepackage[toc,header]{appendix}   % add appendix, see end of doc
%\usepackage{epstopdf}               % include .eps format for images
\usepackage{geometry}               % set layout
	\geometry{a4paper, top=20mm, left=30mm, right=25mm, bottom=17mm,
			headsep=10mm, footskip=12mm}			
\newcommand{\ra}[1]{\renewcommand{\arraystretch}{#1}}
\sloppy
\usepackage[section]{placeins}
% font style
\usepackage{lmodern}
%\renewcommand*\familydefault{\sfdefault} %% Only if the base font of the document is to be sans serif

% one and a half line spacing --> 1.3
\linespread{1.3}
\setlength\parindent{10pt}

%label
\setkomafont{labelinglabel}{~~\bfseries}

% tables 
\usepackage{longtable}
\usepackage{multirow}
\usepackage{booktabs}               % writing tables with good style
\newcolumntype{L}[1]{>{\raggedright\arraybackslash}p{#1}}
\newcolumntype{R}[1]{>{\raggedleft\arraybackslash}p{#1}}
\newcolumntype{C}[1]{>{\centering\arraybackslash}p{#1}}
\renewcommand{\tabcolsep}{10pt}

% enumerate all equations
\newcommand{\myequation}{\begin{equation}}
\newcommand{\myendequation}{\end{equation}}
\let\[\myequation
\let\]\myendequation

% math non-italic subscripts
\newcommand{\s}[1]{_{\textup{#1}}}

% labeling style
\setkomafont{labelinglabel}{~~\bfseries}

\usepackage{hyperref}		% for links
\usepackage{listings}		% for code
\lstset{
	basicstyle = \small\sffamily,
	breaklines = true,
	breakatwhitespace= true,
	frame = none,
	xleftmargin=1cm,
	escapeinside={*(}{)*}   % if you want to add LaTeX within your code
}


% Insert title, titlehead, subject and author here
\titlehead{\centering \textsc{\Large Technische Universität Dresden} \\
\vspace*{1cm}
Chair for Electron Devices and Integrated Circuits
\vspace{1cm}
}
\subject{Manual - v03}
\title{Virtual Source Compact Model}
\author{Dipl.-Ing. Sven Mothes \\ Florian Wolf}
\date{\today} % sets date to date of compilation


\begin{document}


\maketitle
\tableofcontents{}


\newpage 

\section{Virtual Source model equations}

\begin{figure}[ht]
	\centering
	\includegraphics[width=0.8\textwidth]{fig/VSCM.png}
	\caption{Large signal equivalent circuit for the VS model}
\end{figure}

% ---------------------------
\subsection{Current}

It is important to note that the equations sometimes differ depending on the channel structure.
In this document nanowire/-tube transistor variables for discrete channel sections are denoted with a tilde (e.g. $\tilde{Q}\s{ix0}$).
Variables for sheet or bulk materials that have a continuous channel are denoted with a bar (e.g. $\bar{Q}\s{ix0}$).
The discrete and continuous channels are depicted in a schematic diagram in figure \ref{fig:channelTypes}.

\begin{figure}[ht]
    	\centering
        \includegraphics[width=0.6\textwidth]{fig/channel.png} 
    \caption{Schematic diagrams of the two VSCM channel types.}
    \label{fig:channelTypes}
\end{figure}

The current of the VSCM depends on the intrinsic voltages $V\s{GS,i}$ and $V\s{DS,i}$ and can be described in the following form for a continuous channel \cite{khakifirooz2009simple}:
\begin{equation}
    I\s{vs} = W\s{ch} \bar{Q}\s{ix0} v\s{x0}  F\s{s}' \label{eq:curr}
\end{equation}
where $\bar{Q}\s{ix0}$ is the channel charge per area at the point $x_0$ and $v\s{x0}$ is the carrier velocity at the same position. 
The VS point $x_0$ marks the top of the conduction band.
The width of the semiconductor region $W\s{ch}$ depends on the channel structure and the function $F\s{s}'$ models the drain dependency of the current.
For discrete semiconductor sections in the form of nanowires or -tubes the channel width $W\s{ch}$ is replaced by the number wires/tubes $n\s{ta}$ (for $W\s{ch}$ and $n\s{ta}$ see sec. \ref{sec:cinv_calc}):
\begin{equation}
    I\s{vs} = n\s{ta} \tilde{Q}\s{ix0} v\s{x0}  F\s{s}'. \label{eq:curr2}
\end{equation}
The channel charge at $x_0$ depends on the intrinsic gate-source voltage ($V\s{GS,i}$) and is modeled for continuous channel materials with
\begin{equation}
    \bar{Q}\s{ix0} = \bar{C}\s{inv} n\s{ss} \upphi\s{t} \ln \left[ 1 + \exp \left( f\s{deg} \dfrac{V\s{GS,i} - V\s{th}' + \upalpha \upphi\s{t} F\s{f}}{n\s{ss} \upphi\s{t}} \right) \right] \label{eq:charge} 
\end{equation}
and for discrete channels
\begin{equation}
    \tilde{Q}\s{ix0} = \tilde{C}\s{inv} n\s{ss} \upphi\s{t} \ln \left[ 1 + \exp \left( f\s{deg} \dfrac{V\s{GS,i} - V\s{th}' + \upalpha \upphi\s{t} F\s{f}}{n\s{ss} \upphi\s{t}} \right) \right] . \label{eq:charge2} 
\end{equation}
The threshold voltage $V'\s{th}$, the function $F\s{f}$ and the thermal voltage $\upphi\s{t}$ are independent of the channel type
\begin{align}
    V'\s{th} &= V\s{th0} - \updelta V\s{DS,i} \\
    F\s{f} &= \dfrac{1}{1 + \exp \left( \dfrac{V\s{GS,i} - V\s{th}' + 0.5 \upalpha \upphi\s{t} }{\upalpha \upphi\s{t}} \right) }   \\
    \upphi\s{t} &= \frac{k\s{B}T}{q} . 
\end{align}
$\bar{C}\s{inv}$ is the inversion capacitance per area, $\tilde{C}\s{inv}$ is the inversion capacitance per length and tube/wire.
Their calculation is shown in section \ref{sec:cinv_calc}.
Additionally, the mobility degradation factor $f\s{deg}$ will be determined depending on the point of operation.
The thermal voltage is calculated using the temperature $T = \SI{300}{\kelvin}$, the Boltzmann constant $k\s{B}$ and the electron charge $q$.  
The model parameters are the normalized subthreshold slope $ n\s{ss}$,  a fitting parameter $ \upalpha$, the threshold voltage $ V\s{th0}$ at $V\s{DS}=\SI{0}{}$ and a parameter for the DIBL effect $ \updelta$.
$V\s{GS,i}$ and $V\s{DS,i}$ are the intrinsic voltages.


% ---------------------------
\subsubsection{Mobility degradation for small dimension FET}

According to Wong et al. \cite{wong1987modeling} the main reasons for mobility degradation in bulk silicon are surface roughness scattering, series resistances and finite inversion layer thickness. 
In the VSCM the series resistances are included as external parasitic elements.
However, both surface roughness scattering and finite inversion layer effects are not considered in the original model \cite{khakifirooz2009simple}.

These mechanisms are modeled in this work with a reduction of gate-source voltage control over the channel charge, if high gate-source voltages are applied to the transistor.
The mobility degradation factor is defined as
\[ f\s{deg} = 1 - \exp \left(- \dfrac{ 1}{\upgamma V\s{g,eff}} \right) \]
where $\upgamma > 0$ ($[\upgamma] = \si{\per\volt}$) is a fitting parameter.
If $\upgamma = 0$ the compact model will disregard the mobility degradation equations. 
The effective gate voltage is calculated with
\[ V\s{g,eff} = n\s{ss} \upphi\s{t} \ln \left(1 + \exp \left[ \dfrac{V\s{GS,i} - (V\s{th}' - \upalpha \upphi\s{t} F\s{f}) }{n\s{ss} \upphi\s{t}} \right] \right) \]
The exponential function will approach zero for very small voltages, e.g. below threshold, thus $f\s{deg}$ equals one and there is no control loss.


% ---------------------------
\subsubsection{Drain-source voltage dependency}

The drain-source voltage dependency is modeled with the function $F\s{s}$ in a well-known manner \cite{tsividis2011operation} \cite{khakifirooz2009simple}
\begin{align}
    F\s{s} &= \dfrac{x\s{ds}}{\left( 1 + x\s{ds}^{ \upbeta} \right)^{1/\upbeta} } \label{eq:Fs}\\
    x\s{ds} &= \dfrac{V\s{DS,i}}{V\s{D,sat}} 
\end{align}
where $ \upbeta$ is a fitting parameter and the saturation voltage $V\s{D,sat}$ is modified in this work to also handle transistor operation close to the QCL
\[  V\s{D,sat} =\sqrt{ [V\s{D,sat,s} (1-F\s{f}) + \upphi\s{t} F\s{f}]^2 + [V\s{D,sat,q} (1-F\s{f}) + \upphi\s{t} F\s{f}]^2} \]
where $V\s{D,sat,s}$ is standard VSCM saturation voltage for the inversion region. 
The saturation voltage $V\s{D,sat,q}$ describes an increase in saturation voltage close to the QCL.
\begin{align}     
    V\s{D,sat,s} &= \dfrac{v\s{x0} L\s{ch}}{\upmu\s{app}} \\
    V\s{D,sat,q} &= p\s{qcl}  n\s{ss} \upphi\s{t} \ln \left(1 + \exp \left[f\s{deg} \dfrac{V\s{GS,i} - (V\s{th}' - \upalpha \upphi\s{t} F\s{f}) }{n\s{ss} \upphi\s{t}} \right] \right) \label{eq:vds,sat,q}\\
    p\s{qcl} &= \dfrac{ v\s{inj,B}}{v\s{F}}
\end{align}
where $\upmu\s{app}$ is the apparent channel mobility. 
The ballistic injection velocity $v\s{inj,B}$ is a model parameter and the fermi velocity $v\s{F}$ is fixed model internally to the value for CNT: \SI{10e7}{\centi\meter\per\second} \cite{mothes2015toward}.
So far there was no need to make it a model parameter.
The effective channel length $ L\s{ch}$ is the the gate length minus the overlap length between gate and highly doped drain or source region respectively.

Additionally, in this work the Early effect is included in the VSCM.
The following factor modulates the output current if the transistor is in saturation \cite{tsividis2011operation}:
\begin{equation}
F\s{early} = 
\begin{cases}
1+\dfrac{V\s{DS,i}-V\s{d,sat}}{ V\s{A}} & \text{for } V\s{DS,i} > V\s{d,sat} \text{ and } V\s{A} \neq 0 \\
1 & \text{for }  V\s{dsi} \le V\s{d,sat} \text{ or } V\s{A} =0 \\
\end{cases}
\end{equation}
where the Early voltage $ V\s{A}$ is a model parameter. 
The drain voltage dependent current modulation is than combined in the factor $F\s{s}'$
\[ F\s{s}' = F\s{s} F\s{early}. \]




% ---------------------------
\subsection{Charge}


The complete channel charge $Q\s{ch}$ follows from the integration over the channel area.
For the assumption of $Q\s{i}(x) = const.$ one gets for continuous channel materials
 \begin{equation}
    Q\s{ch} = W\s{ch} L\s{ch} \bar{Q}\s{ix0}
\end{equation}    
and for discrete structures
 \begin{equation}
    Q\s{ch} = n\s{ta}L\s{ch} \tilde{Q}\s{ix0}
\end{equation}
where the charge is calculated as
\begin{align}
\bar{Q}\s{ix0} &= \bar{C}\s{inv} n\s{ss} \upphi\s{t} \ln \left[ 1 + \exp \left( f\s{deg} \dfrac{V\s{GS,i} - V\s{th}' + \Updelta V\s{th,q} + \upalpha \upphi\s{t} F\s{f}}{n\s{ss} \upphi\s{t}} \right) \right] \\
\tilde{Q}\s{ix0} &= \tilde{C}\s{inv} n\s{ss} \upphi\s{t} \ln \left[ 1 + \exp \left( f\s{deg} \dfrac{V\s{GS,i} - V\s{th}' + \Updelta V\s{th,q} + \upalpha \upphi\s{t} F\s{f}}{n\s{ss} \upphi\s{t}} \right) \right]. 
\end{align}
For a possible threshold voltage displacement the author of this work introduced the parameter $\Updelta V\s{th,q}$.
The split into drain (d) and source (s) related charge follows an equation proposed by Wei et al. \cite{wei2012virtual}
\begin{align}
    Q\s{d} &= Q\s{d,DD} (1-F\s{sat,q}) + Q\s{d,B} F\s{sat,q} \\ 
    Q\s{s} &= Q\s{s,DD} (1-F\s{sat,q}) + Q\s{s,B} F\s{sat,q} 
\end{align}
with charges either described in a classical drift-diffusion approach ($Q\s{s/d,DD}$) or in a ballistic approach ($Q\s{s/d,B}$).
$F\s{sat,q}$ allows a smooth transition.
\begin{align}
    Q\s{s,B} &= Q\s{ch} \left[ \dfrac{sinh^{-1} \sqrt{k\s{q}}}{\sqrt{k\s{q}}} - \dfrac{\sqrt{k\s{q}+1} - 1}{k\s{q}}\right] \\
    Q\s{d,B} &= Q\s{ch} \left[ \dfrac{\sqrt{k\s{q}+1} - 1}{k\s{q}}\right]    
\end{align}
with
\begin{equation}
    k\s{q} = \dfrac{2 q V\s{DS,i}}{m^* v\s{x0}^2}
\end{equation}
where the effective mass is $m^* = m_0 m\s{eff}$ with $m_0$ the electron mass and $ m\s{eff}$ a material dependent design parameter, and $q$ is the electron charge.  
The 'classic' drift-diffusion charge is modeled with empirical polynomial functions that are also found in long channel charge models \cite{tsividis2011operation}:
\begin{align}
    Q\s{s,DD} &= Q\s{ch} \dfrac{6 + 12 \eta + 8 \eta^2 + 4 \eta^3}{15 (1+\eta)^2} \\
    Q\s{d,DD} &= Q\s{ch} \dfrac{4 + 8 \eta + 12 \eta^2 + 6 \eta^3}{15 (1+\eta)^2}    
\end{align}
where 
\begin{align}
    \eta &= 1 -F\s{sat,q} \\
    u\s{ds}&=f\s{d}\dfrac{V\s{DS,i}}{V\s{sat,q}}\\
    F\s{sat,q} &= \dfrac{ u\s{ds}}{\left[ 1 +  u\s{ds}^{ \upbeta\s{ch}} \right]^{1/\upbeta\s{ch}}} \\
    V\s{sat,q} &= \sqrt{F\s{f} (\upalpha \upphi\s{t})^2 + (Q\s{ix0}/C\s{inv})^2}.
\end{align}
In these equation a fitting parameter $ f\s{d}$ has been introduced by the author in order gain an additional degree of freedom for fitting measurements.



%------------------------------------------------
\subsection{Inversion capacitance}
\label{sec:cinv_calc}

The inversion capacitance is defined as a combination of the oxide capacitance and a so-called quantum capacitance in series \cite{lee2015stanford}. 
The quantum capacitance models quantum effects in the substrate (and poly-silicon gates) and also may be used to include additional charges in the transistor.
Hence, it decreases the effective inversion capacitance $C\s{inv}$ to values smaller than the gate oxide capacitance. The equation is independent of the channel type
\[ C\s{inv} = \dfrac{C\s{ox} C\s{q}}{C\s{ox} + C\s{q}} \label{eq:cinv} \]
where the oxide capacitance $C\s{ox}$ is defined by the structure of gate and substrate. 
The quantum capacitance $ C\s{q}$ is a model parameter of the VSCM. 

There are four structures available in the VSCM (fig. \ref{fig:schematicGateStru}).
The first is a classic bulk MOSFET that has a continuous channel and capacitances.
The discrete channels are found in TG FET with NW/tubes, GAA FET structures and FinFET transistors.
They are selected in VSCM with the parameter 'geomod'.
In the case of top gated nanowires or -tubes screening effects between wires/tubes are included into the model.

\begin{figure}[ht]
	\centering
	\includegraphics[width=0.8\textwidth]{fig/Gate_structures.png}
	\caption{Schematic cross section of the different transistor structures.}
	\label{fig:schematicGateStru}
\end{figure}

\subsubsection{Bulk [geomod=0]}

This structure consists of a parallel planar gate and semiconductor. The oxide capacitance follows the basic plate capacitor formula
\[\bar{C}_\textup{ox} = \dfrac{\upepsilon\s{ox} \upepsilon_0}{t_\textup{ox}} \]
where $ \upepsilon\s{ox}$ is the relative permittivity of the gate oxide, $t_\textup{ox}$ is the gate oxide thickness and $\upepsilon_0$ is the vacuum permittivity.
The width of the active region is the same as the total width of the gate $ W\s{g}$
\[W\s{ch} = W\s{g} .\]

\subsubsection{Top gated nano wires (CNT) [geomod=1]}

This structure consists of a planar gate and various discrete cylindrical semiconductors. 
Screening between the wires/tubes needs to be modeled dependent on the distance between individual wires or tubes.
A simple model is proposed by Hanson et al. \cite{hanson2011analysis}
\[\tilde{C}\s{ox} = \dfrac{\upepsilon\s{ox} \upepsilon_0}{\ln\left[\dfrac{w\s{sp}}{\uppi r\s{semi}} \sinh\left(2 \uppi \dfrac{ r\s{semi} + t\s{ox}}{w\s{sp}} \right) \right]}  \]
where $w\s{sp}$ is the spacing between the tubes or wires and $ r\s{semi}$ is the tube/wire radius ($\tilde{C}\s{ox}$ function values are shown in fig. \ref{fig:coxHanson}). 
The radius is a model parameter and the spacing can be calculated from previously used parameters:
\begin{equation}
w\s{sp} = \dfrac{1}{ D}  
\end{equation}
\begin{figure}[ht] 
	\centering
	\includegraphics[width=0.45\textwidth]{fig/hanson.pdf}
	\caption{$\tilde{C}\s{ox}$ for a single tube with $r\s{semi}= \SI{0.75}{\nano\meter}$ calculated with Hanson's formula.}
	\label{fig:coxHanson}
\end{figure}
where $ D$ is the tube/wire density in the channel.
The number of tubes/wires in the active region is calculated as tube/wire density $ D$ multiplied by the total channel width $ W\s{g}$ of the transistor
\[ n\s{ta} = W\s{g} D . \]
Figure \ref{fig:layout_cnt} demonstrates where those parameter values are found in the simplified layout of an example CNTFET.
The density $ D$ is therefore given as the number of tubes that connect source and drain divided by the width of the transistor $ W\s{g}$.

\begin{figure}[ht]
    	\centering
        \includegraphics[width=0.65\textwidth]{fig/CNTFET_TopView.png} 
    \caption{Simplified layout of a CNTFET}
    \label{fig:layout_cnt}
\end{figure}


\subsubsection{Gate all around (GAA) [geomod=2]}

This structure consists of a cylindrical gate and a cylindrical semiconductor. No screening needs to be considered as the semiconductor regions are completely surrounded by the gate metal.
The oxide capacitance is calculated from the formula for a cylindrical capacitor
\[\tilde{C}\s{ox} = \dfrac{\upepsilon\s{ox} \upepsilon_0}{\ln \left[\dfrac{r\s{semi} + t\s{ox}}{ r\s{semi}}\right]}  \]
where $ r\s{semi}$ is the tube/wire radius.
The number of tubes/wires in the active region is calculated as tube/wire density $ D$ multiplied by the total channel width $ W\s{g}$ of the transistor
\[ n\s{ta} =  W\s{g} D  . \]

\subsubsection{FinFET [geomod=3]}

This case can be used for non-planar gate structures such as double gate transistors or FinFET. 
The oxide capacitance is approximated using the plate capacitor equation
\[\bar{C}\s{ox} = \dfrac{\upepsilon\s{ox} \upepsilon_0}{ t\s{ox}} \]
The width of the active region is the density of Fins $ D$ multiplied by the total gate width of the transistor $ W\s{g}$ multiplied by the effective gate width per Fin $ U$ 
\[W\s{ch} = U W\s{g} D .\]
Note that this is a severe simplification, however it allows to model many different geometries.
The effective gate width for one Fin $ U$ is a design parameter.
It is defined as the width of the semiconductor surface covered by the gate as seen in a cross section.



%------------------------------------------------
\subsection{Channel length scaling}
\subsubsection{Virtual Source mobility}

The following scaling equations for the Virtual Source mobility were proposed by Lundstrom et al. \cite{lundstrom2014compact}. 

In the paper, it is assumed that for very short channels the mobility is equal to a ballistic mobility $\upmu\s{B}$ which is depends linearly on the channel length.
With increasing channel lengths the effective mobility $\upmu\s{eff}$ is approached that depends on the mean free path $\uplambda_0$ of the channel material.
Typically, the mean free path is bias dependent and values can be found in literature for most semiconducting materials.
The VSCM mobility is then given by
\[ \dfrac{1}{\upmu\s{app}} = \dfrac{1}{\upmu\s{B}} + \dfrac{1}{\upmu\s{eff}} \]
where 
\begin{align}
    \upmu\s{B} &= \dfrac{v\s{inj,B} L\s{ch} q}{2 k\s{B} T} \\
    \upmu\s{eff} &= \dfrac{v\s{inj,B} \uplambda_0 q}{2 k\s{B} T} 
\end{align}
with $q$ the electron charge, $k\s{B}$ the Boltzmann constant and $T$ the temperature. The mean free path $\uplambda_0$ is assumed a constant model parameter.
The ballistic injection velocity $v\s{inj,B}$ is a fitting parameter that is in the region of the Fermi velocity of the material in question.


% ---------------------------------
\subsubsection{Virtual Source velocity}

Lundstrom et al. \cite{lundstrom2014compact} also discusses the scaling of the Virtual Source velocity:
\begin{align}
    \dfrac{1}{v\s{x0}} &= \dfrac{1}{v\s{inj,B}} + \dfrac{1}{D\s{n} / L\s{lf}} \\
    D\s{n} &= \dfrac{v\s{inj,B} \uplambda_0}{2} 
\end{align}
where the ballistic injection velocity $v\s{inj,B}$ and the mean free path $ \lambda_0$ are already known, $D\s{n}$ is the diffusion length and $L\s{lf}$ is a fitting parameter which describes the low field length in the channel ($L\s{lf} \le L\s{ch}$). 
It is the length of the region where the conduction band has a small slope which indicates small electric fields are present.
The maximum value for the Virtual Source velocity is therefore $v\s{inj,B}$. 



% ------------------------------------------------------
\section{Parameters}

{\centering
	\captionof{table}{Virtual Source model parameters and their default values.}
	\label{tab:VSparaOverview}
\begin{longtable}{c | c | c | c | L{6cm}}
    \toprule
     Parameter & Name & Unit & Default & Comment \\
     \midrule 
     \midrule   \endhead 
            -     & type & - & 1 & 1 for n-type, -1 for p-type \\
            -     & geomod & - & 0 & 0 .. Bulk, 1 .. Top Gate NW, 2 .. GAA NW, 3 .. FinFET \\
     \midrule           
        $W\s{g}$             & w & \si{\meter} & 1.83e-6 & total transistor width\\
        $L\s{ch}$  & lch & \si{\meter} & 35e-9 & channel length \\        
		$U$  		& u & \si{\meter} & 5e-9 & effective Fin width (geomod=3)\\
		$r\s{semi}$  & r\_semi & \si{\meter} & 0.8e-9 & wire/tube radius (geomod=1,2)\\
		$D$  		& dens & \si{\per\micro\meter} & 20 & wire/tube density (geomod=1,2)\\		
     \midrule       
        $\bar{C}\s{q}$ & \multirow{2}{*}{cq} & \si{\farad\per\square\meter} & \multirow{2}{*}{0.0089} & \multirow{2}{*}{quantum capacitance} \\
        	$\tilde{C}\s{q}$ & &  \si{\farad\per\meter} & & \\
        $t\s{ox}$  & tox & \si{\meter} & 4e-9 & gate oxide thickness \\
        $\upepsilon\s{ox}$ & eps\_ox & - & 3.9 & gate oxide relative permittivity \\
        $\Updelta V\s{th,q}$ & dvthq & \si{\volt} & 0.01 & charge threshold displacement \\
        $f\s{d}$ & fd & - & 1 & fitting factor for charge distribution\\   
        $m\s{eff}$       & meff & - & 0.2 & effective mass \\
	 \midrule
	 	$v\s{inj,B}$    & vinjb   & \si{\meter\per\second} & 1e5 & ballistic injection velocity \\
        $\uplambda_0$     & lambda0 & \si{\meter} & 50e-9 & effective mean free path \\
        $L\s{fl}$       & lfl     & \si{\meter} & 50e-9 & low field length \\  
     \midrule   
        $V\s{th}$ & vth0 & \si{\volt} & 0.3 & threshold voltage at $V\s{DS} = \SI{0}{\volt}$\\
        $\updelta$        & dibl &\si{\volt\per\volt} & 0.12 & DIBL effect parameter \\
        $n\s{ss}$ & nss & - & 1 & normalized sub-threshold slope\\
        $V\s{A}$  & va & \si{\volt} & 0 & Early voltage  \\
        $\upgamma$		   & gamma & \si{\per\volt} & 0 & mobility reduction parameter \\
        $\upalpha$        & alpha & - & 3.5 & transfer curve fitting parameter \\
        $\upbeta$         & beta & - & 1.8 & output curve fitting parameter \\
       $\upbeta\s{ch}$         & beta\_ch & - & 1.8 & output charge curve fitting parameter\\
     \midrule   
        $\uprho\s{sc}$  & rsc & \si{\ohm.\micro\meter} & 75 &  source contact resistance \\
        $\uprho\s{dc}$  & rdc & \si{\ohm.\micro\meter} & 75 & drain contact resistance \\
        $R'\s{g}$  & rg & \si{\ohm\per\micro\meter} & 75 & gate finger resistance \\   
        $R'\s{sf}$  & rsf & \si{\ohm\per\micro\meter} & 0 & source finger resistance \\   
        $R'\s{df}$  & rdf & \si{\ohm\per\micro\meter} & 0 & drain finger resistance \\   
        $C'\s{gs,par}$ & cgspar & \si{\farad\per\micro\meter} & 2.2e-14 & parasitic gate source capacitance 1\\
        $C'\s{gd,par}$ & cgdpar & \si{\farad\per\micro\meter} & 2.2e-14 & parasitic gate drain capacitance 1\\
        $C\s{gs,par2}$ & cgspar2 & \si{\farad} & 0 & parasitic gate source capacitance 2\\
        $C\s{gd,par2}$ & cgdpar2 & \si{\farad} & 0 & parasitic gate drain capacitance 2\\
        $C'\s{ds,par}$ & cdspar & \si{\farad\per\micro\meter} & 2e-15 & parasitic drain source capacitance\\  
     \bottomrule
\end{longtable}
}
\vspace{\baselineskip}



% -------------------------------------------
\subsection{VSCM parameter extraction}

In order to use a compact model, first the model parameters have to be extracted.
The aim is to achieve the best representation of the given measurement (simulation) data with the compact model.
It is desired to calculate as many parameter values as possible directly from specific data points, as this usually increases the accuracy.
For those parameters that cannot be calculated directly, curve fitting functions are available in great variety.

In this section a method for extracting the VSCM parameters from measurements is presented.
Note that for extraction of all parameters DC (current characteristics) and AC (S-parameter) measurements are needed over a reasonable range of operation points.
It is important that the charge related model parameters are extracted before the current parameters, because the current is calculated based on the channel charge.
The following procedure is proposed:
\begin{enumerate}
    \item Design parameters should be given with the measurement data. The parameters commonly known are: 'geomod', 'type', $W\s{g}$, $L\s{ch}$, $t\s{ox}$, $\upepsilon\s{ox}$ ($U$, $D$, $r\s{semi}$, $m^*$)
        \item Parasitic capacitances can be extracted from the Y-parameters (bulk contact connected to source):
    \begin{itemize}
        \item $C\s{gd,par1} = \dfrac{\Im\{\underline{Y}_{21}\}}{2 \uppi f} \bigg|_{V\s{GS} = 0}$
        \item $C\s{gs,par1} = \dfrac{\Im\{\underline{Y}_{11}\}}{2 \uppi f} \bigg|_{V\s{GS} = 0} - C\s{gd,par1}$
        \item $C\s{ds,par} = \dfrac{\Im\{\underline{Y}_{22}\}}{2 \uppi f} \bigg|_{V\s{GS} = 0} - C\s{gd,par1}$
    \end{itemize}
     to determine 'par2' values additionally a Poisson equation solver is needed to simulate the device structure.
    \item $C\s{inv}$ is the effective gate-to-channel capacitance per unit
area in strong inversion calculated as $C\s{inv} = (2\uppi f \Im\{\underline{Z}\s{in}\})^{-1} - (C\s{gd,par1} + C\s{gs,par1})$. 
$C\s{ox}$ can be determined as shown in section \ref{sec:cinv_calc}. Then
		\begin{itemize}
			\item $C\s{q} = \dfrac{C\s{ox} C\s{inv}}{C\s{ox} - C\s{inv}}$
			\item Threshold voltage displacement in $C\s{inv}(V\s{GS})$ graph  is modeled with $\Updelta V\s{th,q}$, this can only be determined after threshold voltage is extracted is in 4.
			\item $C\s{inv}(V\s{GS})$ for different drain voltages is adjusted with $f_{d}$ ($C\s{q}$ might change)
		\end{itemize}	
    \item DC measurements for output and transfer characteristics provide the following parameter values 
    	\begin{itemize}
    		\item $V\s{th0}=V\s{th,low}$ is the threshold voltage extracted from the transfer characteristic at a low drain-source voltage $V\s{DS,low}$ (e.g. \SI{0.05}{\volt})
    	    \item $n\s{ss} = \dfrac{S}{\upphi\s{t} \ln(10)}$ with the "sub-threshold swing" (sub-threshold slope) \mbox{$S=\left(\dfrac{\textup{d} (\log_{10}(I\s{D}/\si{\ampere}))}{\textup{d} V\s{GS}}\right)^{-1}$} for $V\s{GS}<V\s{th}$ 
    	    \item $\updelta = \dfrac{V\s{th,high} - V\s{th,low}}{V\s{DS,high}-V\s{DS,low}}$ with the threshold voltages from transfer characteristic at the respective drain-source voltage where $V\s{DS,high}$ should be well above saturation
    	    \item $V\s{A}$ (Early voltage) extracted from output characteristic (linear rise in saturation region, while non-linear rise is attributed to DIBL, etc.)
    	    \item $R\s{S}$ and $R\s{D}$ can be extracted using Y-function method (sec. \ref{sec:yfunc}), but include both finger and contact resistance
    	    \item $g\s{m}$ reduction at high gate-source voltages can be fitted using $\upgamma$
    	    \item the output current shape is modeled with $\upbeta$.
    	\end{itemize} 
    \item Least-square method curve fitting of $v_\textup{inj,B}$, $\uprho_\textup{s/d,c}$ (close to extracted values), $\lambda_0$, $L\s{lf}$, $\upgamma$ (and $\upbeta$) using output- and transfer characteristic at the same time
\end{enumerate}
\noindent
Standard value for $\upalpha$ is 3.5 and for $\upbeta$ approximately 1.8 for n-FET and 1.4 for p-FET.
Note that the sub-threshold swing for common transistors is limited to the minimum of approximately \SI{60}{\milli\volt\per.dec}, which implies $n\s{ss} \ge 1$.
However, tunnel FET have been demonstrated with smaller sub-threshold swings and thus $n\s{ss}$ can become smaller 1.




\newpage
%------------------------------------------------------------
%--- References
%------------------------------------------------------------
\addcontentsline{toc}{section}{Bibliography}
\bibliographystyle{unsrt}
% Bibliography file
\bibliography{manual}
  
  
\end{document}

