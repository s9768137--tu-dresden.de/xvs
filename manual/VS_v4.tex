\documentclass[a4paper,twoside,11pt,openany]{scrartcl}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}      
\usepackage[protrusion=true,expansion=true]{microtype} % for better looks
\usepackage{graphicx}               % include images
%\usepackage{wrapfig}               % wrapping figures
\usepackage{caption}
\usepackage{amsmath}                % include formulas
\usepackage{bm}
\usepackage{amssymb}
\emergencystretch=1em
%\usepackage{mathptmx}
\usepackage{upgreek}				% writing upright greek letters
\usepackage{siunitx}	            % writing units with good style use \SI{}{} or \si{}
\usepackage[toc,header]{appendix}   % add appendix, see end of doc
\usepackage{epstopdf}               % include .eps format for images
\usepackage{geometry}               % set layout
	\geometry{a4paper, top=20mm, left=30mm, right=25mm, bottom=17mm,
			headsep=10mm, footskip=12mm}			
\newcommand{\ra}[1]{\renewcommand{\arraystretch}{#1}}
%\DeclareMathOperator{\arcsinh}

\sloppy
\usepackage[section]{placeins}
% font style
\usepackage{lmodern}
%\renewcommand*\familydefault{\sfdefault} %% Only if the base font of the document is to be sans serif

% one and a half line spacing --> 1.3
\linespread{1.3}
\setlength\parindent{10pt}

%label
\setkomafont{labelinglabel}{~~\bfseries}

% tables 
\usepackage{longtable}
\usepackage{multirow}
\usepackage{booktabs}               % writing tables with good style
\newcolumntype{L}[1]{>{\raggedright\arraybackslash}p{#1}}
\newcolumntype{R}[1]{>{\raggedleft\arraybackslash}p{#1}}
\newcolumntype{C}[1]{>{\centering\arraybackslash}p{#1}}
\renewcommand{\tabcolsep}{10pt}

% enumerate all equations
%\newcommand{\myequation}{\begin{equation}}
%\newcommand{\myendequation}{\end{equation}}
%\let\[\myequation
%\let\]\myendequation

% math non-italic subscripts
\newcommand{\s}[1]{_{\textup{#1}}}
\newcommand{\bs}[1]{\s{\textbf{#1}}}
%\DeclareMathOperator{\arcsinh}{sinh^{-1}}
% labeling style
\setkomafont{labelinglabel}{~~\bfseries}

\usepackage{hyperref}		% for links
\usepackage{listings}		% for code
\lstset{
	basicstyle = \small\sffamily,
	breaklines = true,
	breakatwhitespace= true,
	frame = none,
	xleftmargin=1cm,
	escapeinside={*(}{)*}   % if you want to add LaTeX within your code
}


% Insert title, titlehead, subject and author here
\titlehead{\centering \textsc{\Large Technische Universität Dresden} \\
\vspace*{1cm}
Chair for Electron Devices and Integrated Circuits
\vspace{1cm}
}
\subject{Manual - v04}
\title{Flexible Virtual Source Compact Model}
\author{Dipl.-Ing. Sven Mothes \\ Florian Wolf}
\date{\today} % sets date to date of compilation


\begin{document}


\maketitle
\tableofcontents{}


\newpage 

\section{Flexible Virtual Source Compact Model equations}
The equivalent circuit of the Flexible Virtual Source Compact Model (X-VSCM) shown in Fig. \ref{fig:Eqcircuit} can be devided into an internal and external portion. The latter comprises finger and metallization represented by the resistances $R\s{sf}$, $R\s{df}$, $\bm{R\bs{g}}$ and the parasitic capacitances $C\s{gs,par}$, $C\s{gd,par}$ and $C\s{ds,par}$. The internal portion consists of the source and drain related contact resistances $R\s{sc}$ and $R\s{dc}$ as well as a nonlinear transfer current source $I\s{vs}$ and nonlinear mobile charges $Q\s{s}$ and $Q\s{d}$. The equations for the latter ones are based on the Virtual Source compact model \cite{KhaNay09} but augmented here with new material and structure related formulations as well as a few fitting parameters, that enable its application to a diverse set of process technologies. The model equations of the X-VS model are summarized below along with hints towards the physical meaning of the modifications and their parameters. Model parameters are indicated by bold faced letters.

\begin{figure}[ht]
	\centering
	\includegraphics[width=0.8\textwidth]{fig/compact_model}
	\caption{Large signal equivalent circuit for the VS model}
	\label{fig:Eqcircuit}
\end{figure}


% ------------------------------------------------------------------------------------------------------------------------------------
\subsection{Nonlinear transfer current source $I\s{vs}$}

%It is important to note that the equations sometimes differ depending on the channel structure.
%In this document nanowire/-tube transistor variables for discrete channel sections are denoted with a tilde (e.g. $\tilde{Q}\s{ix0}$).
%Variables for sheet or bulk materials that have a continuous channel are denoted with a bar (e.g. $Q\s{ix0}$).
%The discrete and continuous channels are depicted in a schematic diagram in figure \ref{fig:channelTypes}.

%\begin{figure}[ht]
 %   	\centering
  %      \includegraphics[width=0.6\textwidth]{fig/channel.png} 
   % \caption{Schematic diagrams of the two VSCM channel types.}
   % \label{fig:channelTypes}
%\end{figure}

The nonlinear transfer current depends on the intrinsic voltages $V\s{gisi}$ and $V\s{disi}$ and is given by
\begin{equation}
    I\s{vs} = \bm{W\bs{g}} Q\s{ix0} v\s{x0}  F\s{ds}
    \label{eq:I_vs}
\end{equation}
where $\bm{W\bs{g}}$ is the total gate width. $Q\s{ix0}$ is the channel charge per unit area at the point $x_0$ and $v\s{x0}$ is the carrier velocity at the virtual source point $x\s{0}$. 
The channel charge at $x_0$ is given by
\begin{equation}
    Q\s{ix0} = C\s{ch}\bm{n\bs{ss}} V\s{T} \ln \left[ 1 + \exp \left( f\s{deg} \dfrac{V\s{gisi} - V\bs{th} + \bm{\upalpha} V\s{T} F\s{f}}{\bm{n\bs{ss}} V\s{T}} \right) \right]
    \label{eq:Q_x0}
\end{equation}
where $V\s{T} = k\s{B}T/q$ is the thermal voltage, $\bm{n\bs{ss}}$ is the subthreshold slope factor and the threshold voltage $V\s{th}$ is given by

\begin{equation}
    V\s{th} = \bm{V\bs{th0}} - \bm{\updelta} V\s{disi}
    \label{eq:V_th}
\end{equation}
with the DIBL factor $\bm{\updelta}$. The function
\begin{equation}
    F\s{f} = \left[1 + \exp \left( \dfrac{V\s{gisi} - \bm{V\bs{th}} +\bm{\upalpha} V\s{T}/2 }{\bm{\upalpha} V\s{T}} \right)\right]^{-1}
    \label{eq:F_f}
\end{equation}
is of Fermi type and allows a smooth reduction of the threshold voltage in Eq.\ref{eq:Q_x0} by $\bm{\upalpha} V\s{T}$ from the subthreshold to the strong inversion region in MOSFETs. $\bm{\upalpha}$ is a fitting factor and $\bm{V\bs{th0}}$ a model parameter.

% ------------------------------------------------------------------------------------------------------------------------------------
\subsubsection{Degradation factor $f\s{deg}$}

According to \cite{wong1987modeling} the main reasons for mobility degradation in bulk silicon are surface roughness scattering, series resistances and finite inversion layer thickness. 
In the X-VSCM the series resistances are included as external parasitic elements.
However, both surface roughness scattering and finite inversion layer effects are not considered in the original model \cite{KhaNay09}.

These mechanisms are modelled in this work with a reduction of gate-source voltage control over the channel charge, if high gate-source voltages are applied to the transistor.
The degradation factor is defined as
\begin{equation}
f\s{deg} = 1 - \exp \left(- \dfrac{ 1}{\bm{\upgamma} V\s{g,eff}} \right)
\label{eq:f_deg}
\end{equation}
where $\bm{\upgamma} > 0$ is a fitting parameter.
If $\bm{\upgamma} = 0$ the compact model will disregard the mobility degradation equations. 
The effective gate voltage $ V\s{g,eff}$ is calculated with
\begin{equation}
V\s{g,eff} = \bm{n\bs{ss}} V\s{T} \ln \left[1 + \exp \left( \dfrac{V\s{gisi} - V\s{th} + \bm{\upalpha} V\s{T} F\s{f} }{\bm{n\bs{ss}} V\s{T}} \right) \right]
\label{eq:V_geff}
\end{equation}
As showhn in Fig. The exponential function will approach zero for very small voltages, e.g. below threshold, thus $f\s{deg}$ equals one and there is no control loss.

\begin{figure}[ht]
 \centering
      \includegraphics[width=0.4\textwidth]{fig/fdeg} 
 \caption{Fitting function $f\s{deg}$ for different $V\s{gisi}$, $V\s{th}=\SI{0.2}{\volt}$, $\bm{n\bs{ss}}=1$, $\bm{\upalpha}=3.5$.}
\label{fig:channelTypes}
\end{figure}

%---------------------------------------------------------------------------------------------------------------------------------------------------
\subsection{Channel capacitance}

The channel material may consist of either a bulk semiconductor or a certain number $n\s{t}$ of tubes/wires or fins of a FINFET per gate width $\bm{W\bs{g}}$ given by the tube/wire/fin density 
\begin{equation}
\bm{\rho\bs{t}}=n\s{t}/\bm{W\bs{g}}.
\label{eq:rho_t}
\end{equation}

In the original VS model the channel capacitance $C\s{ch}$ is a model parameter which describes the inversion capacitance of the MOSFET. However, the X-VS compact model uses a series combination of the quantum capacitance $\bm{C\bs{q}}$ and the oxide capacitance $C\s{ox}$ to model $C\s{ch}$:
\begin{equation}
 C\s{ch} = \dfrac{C\s{ox} \bm{C\bs{q}}} {C\s{ox} + \bm{C\bs{q}}}
\label{eq:C_ch}
\end{equation}
In case of one-dimensional channels, $\bm{C\bs{q}}$ is given by the tube/wire/fin quantum capacitance $\bm{C\bs{q,t}}$ multiplied with the tube/wire/fin density $\bm{\rho\bs{t}}$.
Different device architectures (see Fig. \ref{fig:schematicGateStru}) are being handled by the availability of the corresponding formulations for the oxide capacitance. 
There are four structures available in the X-VSCM selected with the parameter \textbf{geomod}.

\begin{figure}[ht]
	\centering
	\includegraphics[width=0.8\textwidth]{fig/Gate_structures.png}
	\caption{Schematic cross section of the different transistor structures.}
	\label{fig:schematicGateStru}
\end{figure}

%---------------------------------------------------------------------------------------------------------------------------------------------------
\subsubsection{Bulk [geomod=0]}
This structure consists of a parallel planar gate and semiconductor. The oxide capacitance follows the basic plate capacitor formula
\begin{equation}
C_\textup{ox} = \dfrac{\bm{\upepsilon\bs{ox}} \upepsilon_0}{\bm{t}\bs{ox}}
\label{eq:C_ox_0}
\end{equation}
where $ \bm{\upepsilon\bs{ox}}$ is the relative permittivity of the gate oxide, $\bm{t}\bs{ox}$ is the gate oxide thickness and $\upepsilon_0$ is the vacuum permittivity.

%---------------------------------------------------------------------------------------------------------------------------------------------------
\subsubsection{Top gated nano wires/tubes [geomod=1]}
This structure consists of a planar gate and various discrete cylindrical semiconductors. 
Screening between the wires/tubes needs to be modelled dependent on the distance between individual wires or tubes.
A simple model is proposed by Hanson et al. \cite{hanson2011analysis}

\begin{equation}
C\s{ox} =\dfrac{ 2\pi \bm{\rho\bs{t}} \bm{\upepsilon\bs{ox}} \upepsilon_0}{\ln\left[\dfrac{\sinh\left(\pi\bm{\rho\bs{t}}\left[h+\sqrt{h^2-\bm{r\bs{semi}}^2}\right]\right)}{\pi\bm{\rho\bs{t}} \bm{r\bs{semi}}}\right]}
\label{eq:C_ox_1}
\end{equation}
where $\bm{\rho\bs{t}}$ is the tube/wire density and $ \bm{r\bs{semi}}$ is the tube/wire radius. ($C\bs{ox}$ function values are shown in fig. \ref{fig:coxHanson}. $w\s{sp}$ is the spacing between adjacent tubes/nanowires/fins which can be obtained by $w\s{sp} = 1/\bm{\rho\bs{t}}$).  
The radius is a model parameter and the density can be calculated from previously used parameters:

\begin{figure}[ht] 
	\centering
	\includegraphics[width=0.45\textwidth]{fig/hanson.pdf}
	\caption{$C\s{ox}$ for a single tube with $\bm{r\bs{semi}}= \SI{0.75}{\nano\meter}$ calculated with modified Hanson's formula.}
	\label{fig:coxHanson}
\end{figure}
Figure \ref{fig:layout_cnt} demonstrates where those parameter values are found in the simplified layout of an example CNTFET.
The density $\bm{\rho\bs{t}}$ is therefore given as the number of tubes that connect source and drain divided by the width of the transistor $ \bm{W\bs{g}}$. Note that the CNTs at the end do not behave similar to the CNTs in the middle because they do not have CNTs on both sides. So the equation \ref{eq:C_ox_1} is an approximation for when the channel contains a large number of tubes or equivalently the $w\s{sp}$ is much lower compared to $\bm{W\bs{g}}$ and this effect can be neglected.

\begin{figure}[ht]
        \includegraphics[width=1\textwidth, height=0.26\textheight]{fig/CNTFET_topView2} 
    \caption{Simplified layout of a CNTFET}
    \label{fig:layout_cnt}
\end{figure}


%---------------------------------------------------------------------------------------------------------------------------------------------------
\subsubsection{Gate all around (GAA) [geomod=2]}

This structure consists of a cylindrical gate and a cylindrical semiconductor. No screening needs to be considered as the semiconductor regions are completely surrounded by the gate metal.
The oxide capacitance is calculated from the formula for a cylindrical capacitor
\begin{equation}
C\s{ox} = 2\pi\dfrac{\bm{\upepsilon\bs{ox}} \upepsilon_0\rho\s{t}}{\ln \left[\dfrac{\bm{r\bs{semi}} + \bm{t\bs{ox}}}{ \bm{r\bs{semi}}}\right]} 
\label{eq:C_ox_2}  
\end{equation}
where $\bm{r\bs{semi}}$ is the tube/wire radius and $\bm{\rho\bs{t}}$ is the tubes/wires density..

%---------------------------------------------------------------------------------------------------------------------------------------------------
\subsubsection{FinFET [geomod=3]}
This case can be used for non-planar gate structures such as double gate transistors or FinFET. 
The oxide capacitance is approximated using the plate capacitor equation
\begin{equation}
  C\s{ox} = \dfrac{\bm{\upepsilon\bs{ox}} \upepsilon_0 \bm{U} \bm{\rho\bs{t}}}{ \bm{t\bs{ox}}}
\label{eq:C_ox_3}  
\end{equation}
where $\bm{U}= W\s{fin} + 2H\s{fin}$ is the effective channel width per fin with $W\s{fin}$ and $H\s{fin}$ being the width and height of the fin respectively.
Note that this is a severe simplification, however it allows to model many different geometries.
The effective channel width for one Fin $\bm{U}$ is a design parameter.
It is defined as the width of the semiconductor surface covered by the gate as seen in a cross section.


%---------------------------------------------------------------------------------------------------------------------------------------------------
\subsection{Drain-source voltage dependency}

The drain voltage dependence of $I\s{vs}$ in Eq.\ref{eq:I_vs} is described by the function
\begin{equation}
F\s{ds} = F\s{s} F\s{a} F\s{d}.
\label{eq:F_ds}
\end{equation}    

where 

\begin{equation}
F\s{s} = \dfrac{V\s{disi}/V\s{d,sat}}{\left( 1 + (V\s{disi}/V\s{d,sat})^{\bm{\upbeta}} \right)^{1/\bm{\upbeta}}}
\label{eq:F_s}
\end{equation}    
represents the velocity-field dependence of carriers in the channel \cite{tsividis2011operation} .$\bm{\upbeta}$ is a fitting parameter and $V\s{d,sat}$ the saturation voltage. Additionally

\begin{equation}
F\s{a} = 1+\dfrac{V\s{T}}{\bm{V\bs{a}}}\ln\left(1+\exp\left(\dfrac{V\s{disi}-V\s{d,sat}}{V\s{T}}\right)\right)
\label{eq:F_a}
\end{equation}
accounts for channel length modulation (i.e. Early effect) and thus a finite output conductance. For $V\s{disi}>>V\s{d,sat}$ eq.\ref{eq:F_a} reduces to $F\s{a}=1+(V\s{disi}-V\s{d,sat})/\bm{V\bs{a}}$, otherwise $F\s{a}\approx1$.
The possible influence of source/drain Schottky barriers on the drain current characteristics is taken into account by the empirical function

\begin{equation}
F\s{d} = \left[1 +\exp\left(-\dfrac{V\s{disi} - V\s{th,d}}{\bm{n\bs{d}}V\s{T}}\right)\right]^{-1}
\label{eq:F_d}
\end{equation}
which can be used to model s-shaped output characteristics.
The saturation voltage is given by
\begin{equation}
 V\s{d,sat} = (V\s{d,sat,s} + V\s{d,sat,q}) (1-F\s{f}) + V\s{T} F\s{f}
\label{eq:V_dsat}
\end{equation}
where
\begin{equation} 
    V\s{d,sat,s} = \dfrac{v\s{x0} \bm{L\bs{ch}}}{\upmu\s{app}}
    \label{eq:v_dsats}
\end{equation}
is saturation voltage used in the original VSCM model \cite{KhaNay09}. $\upmu\s{app}$ is the apparent channel mobility and $\bm{L\bs{ch}}$ the effective channel length. The saturation voltage $V\s{d,sat,q}$ describes an increase in saturation voltage close for operation in the quantum capacitance limit (QCL) given by
\begin{equation}
    V\s{d,sat,q} = \dfrac{\bm{p\bs{qcl}}Q\s{ix0}}{C\s{ch}} 
    \label{eq:v_dsatq}
\end{equation}
where $\bm{p\bs{qcl}}$ is a transport dependent factor. For ballistic transport $\bm{p\bs{qcl}}=1$ and setting $\bm{p\bs{qcl}}=0$ recovers the original virtual source model meant for diffusive transport.

\subsection{Channel length scaling}
\subsubsection{Virtual Source mobility}

The following scaling equations for the Virtual Source mobility were proposed by Lundstrom et al. \cite{lundstrom2014compact}. 

In the paper, it is assumed that for very short channels the mobility is equal to a ballistic mobility $\upmu\s{ball}$ which is depends linearly on the channel length.
With increasing channel lengths the effective mobility $\upmu\s{eff}$ is approached that depends on the mean free path $\uplambda_0$ of the channel material.
Typically, the mean free path is bias dependent and values can be found in literature for most semiconducting materials.
The VSCM mobility is then given by
\begin{equation}
 \dfrac{1}{\upmu\s{app}} = \dfrac{1}{\upmu\s{ball}} + \dfrac{1}{\upmu\s{eff}}
 \label{eq:mu_app}
\end{equation}
where 
\begin{equation}
\upmu\s{ball} = \dfrac{\bm{v\bs{ball}} \bm{L\bs{ch}}}{2V\s{T}}
    \label{eq:mu_ball}
\end{equation}
\begin{equation}
    \upmu\s{eff} = \dfrac{\bm{v\bs{ball}} \bm{\uplambda\bs{0}} }{2V\s{T}}
    \label{eq:mu_eff} 
\end{equation}
with $q$ the electron charge, $k\s{B}$ the Boltzmann constant and $T$ the temperature. The mean free path $\uplambda_0$ is assumed a constant model parameter.
The ballistic injection velocity $\bm{v\bs{ball}}$ is a fitting parameter that is in the region of the Fermi velocity of the material in question.


% ---------------------------------
\subsubsection{Virtual Source velocity}

The virtual source velocity $v\s{x0}$ is given by:
\begin{equation}
    v\s{x0} = \bm{v\bs{ball}} \dfrac{\bm{\uplambda\bs{0}}}{\bm{\uplambda\bs{0}} + 2\bm{L\bs{fl}}}
    \label{eq:vx0}
\end{equation}
where the ballistic injection velocity $\bm{v\bs{ball}}$ and the mean free path $ \lambda_0$ are already known, and $\bm{L\bs{fl}}$ is the critical length defined by distance over which electric potential drops by $V\s{T}$ from virtual source point. 
The maximum value for the Virtual Source velocity is therefore $\bm{v\bs{ball}}$. 

% ---------------------------
\subsection{Charge}


The complete channel charge $Q\s{ch}$ follows from the integration over the channel area.
For the assumption of $Q\s{i}(x) = const.$ one gets for different channel materials
 \begin{equation}
    Q\s{ch} = \bm{W\bs{g}} \bm{L\bs{ch}} Q\bs{ix0}
    \label{eq:Qch}
\end{equation}    
where the charge is calculated as given in Eq. \ref{eq:Q_x0}.

The split into drain (d) and source (s) related charge follows an equation proposed by Wei et al. \cite{wei2012virtual}
\begin{equation}
Q\s{d} = Q\s{d,dd} (1-F\s{sat,b}^2) + Q\s{d,ball} F\s{sat,b}^2
\label{eq:Qd}
\end{equation}
\begin{equation}
    Q\s{s} = Q\s{s,dd} (1-F\s{sat,b}^2) + Q\s{s,ball} F\s{sat,b}^2 
\label{eq:Qs}
\end{equation}
with charges either described in a classical drift-diffusion approach ($Q\s{s/d,dd}$) or in a ballistic approach ($Q\s{s/d,ball}$).
$F\s{sat,b}$ allows a smooth transition.
\begin{equation}
    Q\s{s,ball} = Q\s{ch} \left[\dfrac{\textup{sinh}^{-1}\sqrt{k\s{q}}}{\sqrt{k\s{q}}} - \dfrac{\sqrt{k\s{q}+1} - 1}{k\s{q}}\right]
    \label{eq:Qs_ball}
\end{equation}
\begin{equation}
    Q\s{d,ball} = Q\s{ch} \left[ \dfrac{\sqrt{k\s{q}+1} - 1}{k\s{q}}\right]
    \label{eq:Qd_ball}    
\end{equation}
with
\begin{equation}
    k\s{q} = \dfrac{2 q V\s{disi}}{m^* v\s{x0}^2}
    \label{eq:kq}
\end{equation}
where the effective mass is $m^* = m_0 \bm{m\bs{eff}}$ with $m_0$ the electron mass and $ \bm{m\bs{eff}}$ a material dependent design parameter, and $q$ is the electron charge.  
The 'classic' drift-diffusion charge is modelled with empirical polynomial functions that are also found in long channel charge models \cite{tsividis2011operation}:
\begin{equation}
    Q\s{s,dd} = Q\s{ch} \dfrac{6 + 12 \eta + 8 \eta^2 + 4 \eta^3}{15 (1+\eta)^2}
\label{eq:Qs_dd}
\end{equation}

\begin{equation}
    Q\s{d,dd} = Q\s{ch} \dfrac{4 + 8 \eta + 12 \eta^2 + 6 \eta^3}{15 (1+\eta)^2} 
    \label{eq:Qd_dd}   
\end{equation}
where 
\begin{equation}
    \eta = 1 -F\s{sat,b}
    \label{eq:eta}
\end{equation}

\begin{equation}
    F\s{sat,b} = \bm{f\bs{b}}\dfrac{V\s{disi}/V\s{d,sat}}{\left[ 1 +  (V\s{disi}/V\s{d,sat})^{ \bm{\upbeta\bs{ch}}} \right]^{1/\bm{\upbeta\bs{ch}}}}
    \label{eq:F_satb}
\end{equation}

\begin{equation}
    V\s{sat,q} = \sqrt{F\s{f} (\upalpha V\s{T})^2 + (Q\s{ix0}/C\s{ch})^2}.
    \label{eq:V_satq}
\end{equation}
In these equation a fitting parameter $ \bm{f\bs{b}}$ has been introduced by the author in order gain an additional degree of freedom for fitting measurements.


% ---------------------------
\subsection{Contact resistance}
The contact resistances $R\s{sc}$ and $R\s{dc}$ are given by
\begin{equation}
   R\s{sc} = \bm{\uprho\bs{sc}}/\bm{W\bs{g}}
    \label{eq:R_sc}
\end{equation}
and
\begin{equation}
   R\s{dc} =\bm{\uprho\bs{dc}}/\bm{W\bs{g}}
    \label{eq:R_dc}
\end{equation}
where $\bm{\uprho\bs{sc}}$ and $\bm{\uprho\bs{sc}}$ are the width normalized source and drain contact resistances respectively. For one-dimensional channels  $\bm{\uprho\bs{sc}}$ and $\bm{\uprho\bs{dc}}$ are calculated by
\begin{equation}
   \bm{\uprho\bs{sc}} = \bm{R\bs{sc,t}}/\bm{\rho\bs{t}}
    \label{eq:R_sc}
\end{equation}
and
\begin{equation}
   \bm{\uprho\bs{dc}} = \bm{R\bs{dc,t}}/\bm{\rho\bs{t}}
    \label{eq:R_dc}
\end{equation}
where the model parameter $ \bm{R\bs{sc,t}}$ and $ \bm{R\bs{dc,t}}$ describe the contact resistance per tube/wire/fin at source and drain side respectively. For bulk semiconductors $\bm{\uprho\bs{sc}}$ and $\bm{\uprho\bs{dc}}$ are model paramters.

% ---------------------------
\subsection{Finger resistances}
The gate finger resistances for all device geometries (geomod = 0,1,2,3) are obtained by the following equations:
\begin{equation}
R\s{sf} = \bm{R'\bs{sf}} \bm{W\bs{g}}
\label{eq:R_sf}
\end{equation}

\begin{equation}
R\s{df} = \bm{R'\bs{df}} \bm{W\bs{g}}
\label{eq:R_sf}
\end{equation}
where $\bm{R'\bs{sf}}$ and $\bm{R'\bs{df}}$ are width normalized source and drain finger resistances respectively.

% ---------------------------
\subsection{Parasitic capacitances}
The parasitic gate-source and gate-drain capacitance for all devices geometries (geomod=0,1,2,3) can be found out using the following equations:
\begin{equation}
C\s{gs,par} = \bm{C'\bs{gs,par}} \bm{W\bs{g}}
\label{eq:C_gs_par}
\end{equation} 

\begin{equation}
C\s{gd,par} = \bm{C'\bs{gd,par}} \bm{W\bs{g}}
\end{equation}
where $\bm{C'\bs{gs,par}}$ and $\bm{C'\bs{gd,par}}$ are width normalized gate-source and gate-drain capacitances respectively. 

% ------------------------------------------------------
\section{Model Parameters}

{\centering
	\captionof{table}{Virtual Source model parameters and their default values.}
	\label{tab:VSparaOverview}
\begin{longtable}{c | c | c | c | L{6cm}}
    \toprule
     Parameter & Name & Unit & Default & Comment \\
     \midrule 
     \midrule   \endhead 
            -     & type & - & 1 & 1 for n-type, -1 for p-type \\
            -     & geomod & - & 0 & 0 .. Bulk, 1 .. Top Gate NW, 2 .. GAA NW, 3 .. FinFET \\
     \midrule           
        ${W\s{g}}$             & w & \si{\meter} & 1.83e-6 & total transistor width\\
        ${L\s{ch}}$  & lch & \si{\meter} & 35e-9 & channel length \\        
		${U}$  		& u & \si{\meter} & 5e-9 & effective Fin width (geomod=3)\\
		${r\s{semi}}$  & r\_semi & \si{\meter} & 0.8e-9 & wire/tube radius (geomod=1,2)\\
		${\rho\s{t}}$  		& dens & \si{\per\micro\meter} & 20 & wire/tube density (geomod=1,2,3)\\		
     \midrule  
        $p\s{qcl}$  &  pqcl & - & 0 & Transport factor (1=ballistic, 2=diffusive) \\
        ${C\s{q}}$ & {cq} & \si{\farad\per\square\meter} & {0.0089} & {quantum channel capacitance per unit area} \\
        ${C\s{q,t}}$ & {cq\_t} &  \si{\farad\per\meter} & {0.0089} & {quantum channel capacitance per unit length}\\
        ${t\s{ox}}$  & tox & \si{\meter} & 4e-9 & gate oxide thickness \\
        ${\upepsilon\s{ox}}$ & eps\_ox & - & 3.9 & gate oxide relative permittivity \\
        ${f\s{b}}$ & fb & - & 1 & fitting factor for charge distribution (0.1 for CNTs)\\   
        ${m\s{eff}}$       & meff & - & 0.2 & effective mass \\
	 \midrule
	 	${v\s{ball}}$    & vball   & \si{\meter\per\second} & 1e5 & ballistic injection velocity \\
        $\uplambda\s{0}$     & lambda0 & \si{\meter} & 50e-9 & effective mean free path \\
        ${L\s{fl}}$       & lfl     & \si{\meter} & 50e-9 & low field length \\  
     \midrule   
        ${V\s{th}}$ & vth0 & \si{\volt} & 0.3 & threshold voltage at $V\s{DS} = \SI{0}{\volt}$\\
        ${\updelta}$        & dibl &\si{\volt\per\volt} & 0.12 & DIBL effect parameter \\
        ${n\s{ss}}$ & nss & - & 1 & normalized sub-threshold slope\\
        ${V\s{a}}$  & va & \si{\volt} & 0 & Early voltage  \\
        ${\upgamma}$		   & gamma & \si{\per\volt} & 0 & mobility reduction parameter \\
        ${\upalpha}$        & alpha & - & 3.5 & transfer curve fitting parameter \\
        ${\upbeta}$         & beta & - & 1.8 & output curve fitting parameter \\
        ${\upbeta\s{ch}}$         & beta\_ch & - & 1.8 & output charge curve fitting parameter\\
        $n\s{d}$ & nd & - & 0 & nd=1 for S-shaped output characteristics and 0 otherwise \\    
        $V\s{th,d}$ & vthd & V & -5 & parameter S-shaped output characteristics\\    
     \midrule   
        $\uprho\s{sc}$  & rsc & \si{\ohm.\micro\meter} & 75 &  normalized source contact resistance (geomod=0) \\
        $\uprho\s{dc}$  & rdc & \si{\ohm.\micro\meter} & 75 & normalized drain contact resistance (geomod=0) \\
        ${R\s{sc,t}}$  & rsc\_t & \si{\ohm} & 1000 &  source contact resistance per wire/tube/fin  (geomod=1/2/3)\\
        ${R\s{dc,t}}$  & rdc\_t & \si{\ohm} & 1000 & drain contact resistance per wire/tube/fin (geomod=1/2/3) \\
        ${R\s{g}}$  & rg & \si{\ohm} & 75 & gate finger resistance \\   
        $R'\s{sf}$  & rsf & \si{\ohm\per\micro\meter} & 0 & source finger resistance \\   
        $R'\s{df}$  & rdf & \si{\ohm\per\micro\meter} & 0 & drain finger resistance \\   
        $C'\s{gs,par}$ & cgspar & \si{\farad\per\micro\meter} & 2.2e-14 & parasitic gate source capacitance 1\\
        $C'\s{gd,par}$ & cgdpar & \si{\farad\per\micro\meter} & 2.2e-14 & parasitic gate drain capacitance 1\\
        $C\s{gs,par2}$ & cgspar2 & \si{\farad} & 0 & parasitic gate source capacitance 2\\
        $C\s{gd,par2}$ & cgdpar2 & \si{\farad} & 0 & parasitic gate drain capacitance 2\\
        $C'\s{ds,par}$ & cdspar & \si{\farad\per\micro\meter} & 2e-15 & parasitic drain source capacitance\\  
     \bottomrule
\end{longtable}
}

% -------------------------------------------
\subsection{VSCM parameter extraction}

In order to use a compact model, first the model parameters have to be extracted.
The aim is to achieve the best representation of the given measurement (simulation) data with the compact model.
It is desired to calculate as many parameter values as possible directly from specific data points, as this usually increases the accuracy.
For those parameters that cannot be calculated directly, curve fitting functions are available in great variety.

In this section a method for extracting the VSCM parameters from measurements is presented.
Note that for extraction of all parameters DC (current characteristics) and AC (S-parameter) measurements are needed over a reasonable range of operation points.
It is important that the charge related model parameters are extracted before the current parameters, because the current is calculated based on the channel charge.
The following procedure is proposed:
\begin{enumerate}
    \item Design parameters should be given with the measurement data. The parameters commonly known are: 'geomod', 'type', $\bm{W\bs{g}}$, $\bm{L\bs{ch}}$, $\bm{t\bs{ox}}$, $\bm{\upepsilon\bs{ox}}$ ($\bm{U}$, $D$, $\bm{r\bs{semi}}$, $m^*$)
        \item Parasitic capacitances can be extracted from the Y-parameters (bulk contact connected to source):
    \begin{itemize}
        \item $C\s{gd,par1} = \dfrac{\Im\{\underline{Y}_{21}\}}{2 \uppi f} \bigg|_{V\s{GS} = 0}$
        \item $C\s{gs,par1} = \dfrac{\Im\{\underline{Y}_{11}\}}{2 \uppi f} \bigg|_{V\s{GS} = 0} - C\s{gd,par1}$
        \item $C\s{ds,par} = \dfrac{\Im\{\underline{Y}_{22}\}}{2 \uppi f} \bigg|_{V\s{GS} = 0} - C\s{gd,par1}$
    \end{itemize}
     to determine 'par2' values additionally a Poisson equation solver is needed to simulate the device structure.
    \item $C\s{ch}$ is the effective gate-to-channel capacitance per unit
area in strong inversion calculated as $C\s{ch} = (2\uppi f \Im\{\underline{Z}\s{in}\})^{-1} - (C\s{gd,par1} + C\s{gs,par1})$. 
$C\s{ox}$ can be determined as shown in section \ref{sec:cinv_calc}. Then
		\begin{itemize}
			\item $\bm{C\bs{q}} = \dfrac{C\s{ox} C\s{ch}}{C\s{ox} - C\s{ch}}$
			\item Threshold voltage displacement in $C\s{ch}(V\s{GS})$ graph  is modelled with $\Updelta V\s{th,q}$, this can only be determined after threshold voltage is extracted is in 4.
			\item $C\s{ch}(V\s{GS})$ for different drain voltages is adjusted with $f_{d}$ ($\bm{C\bs{q}}$ might change)
		\end{itemize}	
    \item DC measurements for output and transfer characteristics provide the following parameter values 
    	\begin{itemize}
    		\item $V\s{th0}=V\s{th,low}$ is the threshold voltage extracted from the transfer characteristic at a low drain-source voltage $V\s{DS,low}$ (e.g. \SI{0.05}{\volt})
    	    \item $n\s{ss} = \dfrac{S}{V\s{T} \ln(10)}$ with the "sub-threshold swing" (sub-threshold slope) \mbox{$S=\left(\dfrac{\textup{d} (\log_{10}(I\s{D}/\si{\ampere}))}{\textup{d} V\s{GS}}\right)^{-1}$} for $V\s{GS}<\bm{V\bs{th}}$ 
    	    \item $\updelta = \dfrac{V\s{th,high} - V\s{th,low}}{V\s{DS,high}-V\s{DS,low}}$ with the threshold voltages from transfer characteristic at the respective drain-source voltage where $V\s{DS,high}$ should be well above saturation
    	    \item $\bm{V\bs{a}}$ (Early voltage) extracted from output characteristic (linear rise in saturation region, while non-linear rise is attributed to DIBL, etc.)
    	    \item $R\s{S}$ and $R\s{D}$ can be extracted using Y-function method (sec. \ref{sec:yfunc}), but include both finger and contact resistance
    	    \item $g\s{m}$ reduction at high gate-source voltages can be fitted using $\upgamma$
    	    \item the output current shape is modelled with $\bm{\upbeta}$.
    	\end{itemize} 
    \item Least-square method curve fitting of $v_\textup{inj,B}$, $\uprho_\textup{s/d,c}$ (close to extracted values), $\lambda_0$, $L\s{lf}$, $\upgamma$ (and $\bm{\upbeta}$) using output- and transfer characteristic at the same time
\end{enumerate}
\noindent
Standard value for $\upalpha$ is 3.5 and for $\bm{\upbeta}$ approximately 1.8 for n-FET and 1.4 for p-FET.
Note that the sub-threshold swing for common transistors is limited to the minimum of approximately \SI{60}{\milli\volt\per.dec}, which implies $n\s{ss} \ge 1$.
However, tunnel FET have been demonstrated with smaller sub-threshold swings and thus $n\s{ss}$ can become smaller 1.




\newpage
%------------------------------------------------------------
%--- References
%------------------------------------------------------------
\addcontentsline{toc}{section}{Bibliography}
\bibliographystyle{unsrt}
% Bibliography file
\bibliography{manual}
  
  
\end{document}

