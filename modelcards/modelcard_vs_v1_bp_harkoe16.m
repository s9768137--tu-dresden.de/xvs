function [para] = modelcard_vs_v1_bp_harkoe16

% HarKoe16
% Black Phosphorus
para.type  = -1;
para.w     = 1e-06;
para.lg    = 300e-9;
para.mu    = 0.0066;
para.vxo   = 2.1963e+04;
para.cinv  = 0.0107;
para.vth   = 1.4550;
para.rsc   = 51.8582;
para.rdc   = 51.8582;
para.alpha = 0.7906;
para.beta  = 1.8961;
para.dibl  = 0.0026;
para.nss   = 0.7918;

% new model card
para.type  = -1;
para.w     = 1.0000e-06;
para.lg    = 3.0000e-07;
para.mu    = 0.0073;
para.vxo   = 2.1656e+04;
para.cinv  = 0.0093;
para.vth   = 1.3525;
para.rsc   = 43.9079;
para.rdc   = 43.9079;
para.alpha = 4.5000;
para.beta  = 2.2939;
para.dibl  = 0.0026;
para.nss   = 0.7918;