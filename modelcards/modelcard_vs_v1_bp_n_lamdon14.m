function [para] = modelcard_vs_v1_bp_n_lamdon14

% LamDon14
% BP n fet
para.type  = 1;
para.w     = 1e-06;
para.lg    = 20e-9;
para.mu    = 500e-4;
para.vxo   = 2.3e5;
para.cinv  = 0.105;
para.vth   = 0.3025;
para.rsc   = 0;
para.rdc   = 0;
para.alpha = 3.5;
para.beta  = 1.4;
para.dibl  = 0;
para.nss   = 1;