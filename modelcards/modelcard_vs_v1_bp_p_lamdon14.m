function [para] = modelcard_vs_v1_bp_p_lamdon14

% LamDon14
% BP p fet
para.type  = 1;
para.w     = 1e-06;
para.lg    = 20e-9;
para.mu    = 5.9944;
para.vxo   = 6.5588e+05;
para.cinv  = 0.0894;
para.vth   = 0.3096;
para.rsc   = 20.4129;
para.rdc   = 20.4129;
para.alpha = 1.2440;
para.beta  = 2.9524;
para.dibl  = 2.1875e-04;
para.nss   = 1.3427;