function [para] = modelcard_vs_v1_germanane_n_lowhua14

% LowHua14
% Germanane n fet
para.type  = 1;
para.w     = 1e-06;
para.lg    = 15e-9;
para.mu    = 500e-4;
para.vxo   = 5.214e+05;
para.cinv  = 0.0274;
para.vth   = 0.3525;
para.rsc   = 0;
para.rdc   = 0;
para.alpha = 3.0676;
para.beta  = 1.4;
para.dibl  = 0;
para.nss   = 1;