function [para] = modelcard_vs_v1_germanene_p_lowhua14

% LowHua14
% Germanene p fet
para.type  = 1;
para.w     = 1e-06;
para.lg    = 15e-9;
para.mu    = 1.4743;
para.vxo   = 2.6537e+05;
para.cinv  = 0.0895;
para.vth   = 0.3630;
para.rsc   = 17.0795;
para.rdc   = 17.0795;
para.alpha = 1.8575;
para.beta  = 2.8042;
para.dibl  = 5.8944e-04;
para.nss   = 1.2609;