function [para] = modelcard_vs_v1_mos2_chejia14

% CheJia14
% MoS2
para.type  = 1;
para.w     = 1e-06;
para.lg    = 1.16e-7;
para.mu    = 10e-4;
para.vxo   = 1.9692e+04;
para.cinv  = 0.0031;
para.vth   = -1.5315;
para.rsc   = 710.4607;
para.rdc   = 710.4607;
para.alpha = 3.5;
para.beta  = 1.8;
para.dibl  = 0.0595;
para.nss   = 2.63;
para.cgspar= 4.9e-15/20;
para.cdspar= 0.43e-15/20;
