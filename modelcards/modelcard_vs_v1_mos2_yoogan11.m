function [para] = modelcard_vs_v1_mos2_yoogan11

% YooGan11
% MoS2
para.type  = 1;
para.w     = 1e-06;
para.lg    = 19e-9;
para.mu    = 0.0089;
para.vxo   = 2.2612e+05;
para.cinv  = 0.0277;
para.vth   = 0.3211;
para.rsc   = 1;
para.rdc   = 1;
para.alpha = 3.5;
para.beta  = 1.8;
para.dibl  = 1.3580e-05;
para.nss   = 2.0;