function [para] = modelcard_vs_v1_silicane_n_lowhua14

% LowHua14
% Silicane n fet
para.type  = 1;
para.w     = 1e-06;
para.lg    = 15e-9;
para.mu    = 500e-4;
para.vxo   = 1.441e+05;
para.cinv  = 0.1339;
para.vth   = 0.3641;
para.rsc   = 0;
para.rdc   = 0;
para.alpha = 3.6640;
para.beta  = 1.4;
para.dibl  = 0;
para.nss   = 1;