function [para] = modelcard_vs_v1_silicane_p_lowhua14

% LowHua14
% Silicane p fet
para.type  = 1;
para.w     = 1e-06;
para.lg    = 15e-9;
para.mu    = 4.9958;
para.vxo   = 2.1218e+05;
para.cinv  = 0.1234;
para.vth   = 0.4075;
para.rsc   = 17.1425;
para.rdc   = 17.1425;
para.alpha = 2.9948;
para.beta  = 2.9854;
para.dibl  = 0.0339;
para.nss   = 1.3454;