function [para] = modelcard_vs_v1_silicene_p_taolin15

% TaoLin15
% Silicene p fet
para.type  = 1;
para.w     = 1e-06;
para.lg    = 1.8e-6;
para.mu    = 0.0128;
para.vxo   = 1.4987e+04;
para.cinv  = 0.0067;
para.vth   = 0.8376;
para.rsc   = 1.8116e+04;
para.rdc   = 1.8116e+04;
para.alpha = 3.1239;
para.beta  = 1.7920;
para.dibl  = 4.1481e-04;
para.nss   = 1.8084;