function [para] = modelcard_vs_v1_wse2_fanchu12

% FanChu12
% WSe2
para.type  = -1;
para.w     = 1e-06;
para.lg    = 9.4e-6;
para.mu    = 0.0360;
para.vxo   = 3.0447e+03;
para.cinv  = 0.0049;
para.vth   = 0.8341;
para.rsc   = 3.4642;
para.rdc   = 3.4642;
para.alpha = 4.4993;
para.beta  = 1.5362;
para.dibl  = 0.003;
para.nss   = 1;