function [para] = modelcard_vs_v2_cntfet_130nm

% 130nm cntfet
para.type    = 1;
para.geomod  = 1; % top gate nw

para.w       = 1.0000e-06;
para.lch     = 1.3000e-07;
para.r_semi  = 0.80e-09;   % replace with diameter
para.dens    = 50;         % tube density

para.cq      = 1.1000e-10;
para.tox     = 3.0000e-09;
para.eps_ox  = 9;

para.dvthq   = 0.2000;
para.fd      = 0.1000;

para.meff    = 0.2;
para.lambda0 = 8.7600e-08;
para.lfl     = 1.6800e-08;
para.vinjb   = 520000;

para.vth0    = 0.1800;
para.dibl    = 0.03;%560;
para.nss     = 2;
para.va      = 0;
para.gamma   = 0.8000;
para.alpha   = 3.5000;
para.beta    = 1.8000;

para.rsc     = 4.1e+03; %Ohm/tube
para.rdc     = 4.1e+03; 
para.rg      = 4.5;
para.rsf     = 0;
para.rdf     = 0;
para.cgspar  = 1e-17;
para.cgdpar  = 2e-17;
para.cdspar  = 2e-17;
para.cgspar2 = 0;
para.cgdpar2 = 0;

para.rmet    = 0;
para.t_e     = 300;
para.vthg    = 0.2;
para.nd      = 0;