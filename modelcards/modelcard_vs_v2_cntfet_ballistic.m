function [para] = modelcard_vs_v2_cntfet_ballistic

vf = 1e6;
q  = 1.60219e-19;
bk = 1.38066e-23;
T  = 300;
h  = 1.05459e-34*2*pi;
% ballistic CNTFET
para.type  = 1;
para.w     = 1;
para.lg    = 10e-9;
para.mu    = vf*para.lg*q/(2*bk*T);
para.vxo   = vf;
para.cinv  = 4*q^2/h/vf;
para.vth   = 0.4;
para.rsc   = 0e3;
para.rdc   = 0e3;
para.alpha = 3.5;
para.beta  = 1.4;
para.dibl  = 0;
para.nss   = 1;