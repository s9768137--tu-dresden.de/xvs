function [para] = modelcard_vs_v2_cntfet_franklin

% 10nm cntfet
para.type    = 1;
para.geomod  = 1; % top gate nw

para.w       = 5.0000e-06;
para.lch     = 10.0000e-09;
para.r_semi  = 0.65e-09;   % replace with diameter
para.dens    = 70;         % tube density

para.cq      = 8.6000e-11;%1.5521e-10; % F/m
para.tox     = 3e-09;
para.eps_ox  = 14;

para.dvthq   = 0.1000;
para.fd      = 0.5000;

para.meff    = 0.0490;
para.lambda0 = 1.0000e-08;
para.lfl     = 1.0001e-10;
para.vinjb   = 2.5638e+05;

para.vth0    = 0.3200;
para.dibl    = 0.1500;
para.nss     = 1.25;
para.va      = 0;
para.gamma   = 0.3; %=0 in DA
para.alpha   = 3.5000;
para.beta    = 1.8000;

para.rsc     = 3.32e3; %Ohm*um
para.rdc     = 3.32e3; 
para.rg      = 50/3;  % unbekannt
para.rsf     = 25;
para.rdf     = 25;
para.cgspar  = 1.1000e-21;
para.cgdpar  = 5.5000e-20;
para.cdspar  = 5.5000e-20;
para.cgspar2 = 0;
para.cgdpar2 = 0;

para.rmet    = 0;
para.t_e     = 300;
para.vthg    = 0.3000;
para.nd      = 0;
para.qcl=0;