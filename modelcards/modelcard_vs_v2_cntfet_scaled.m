function [para] = modelcard_vs_v2_cntfet_scaled

% 130nm cntfet
para.type    = 1;
para.geomod  = 1; % top gate nw

para.w       = 1.0000e-06;
para.lch     = 1.3000e-07;
para.r_semi  = 0.75e-09; % replace with diameter
para.dens    = 50;         % tube density

para.cq      = 1.9402e-10;%1.5521e-10; % F/m
para.tox     = 2.7300e-09;
para.eps_ox  = 3.9000e3;

para.dvthq   = 0.1000;
para.fd      = 0.1000;

para.meff    = 0.0480;
para.lambda0 = 1.8000e-08;
para.lfl     = 1.0000e-08;

para.vth0    = 0.2000;
para.dibl    = 0.2000;
para.nss     = 2.5000;
para.va      = 0;
para.gamma   = 0.3000e-6;
para.alpha   = 3.5000;
para.beta    = 1.8000;

para.rsc     = 250; %Ohm*um
para.rdc     = 250; 
para.rg      = 0;
para.rsf     = 0;
para.rdf     = 0;
para.cgspar  = 1.1000e-17;
para.cgdpar  = 1.1000e-16;
para.cdspar  = 1.1000e-16;
para.cgspar2 = 0;
para.cgdpar2 = 0;

para.rmet    = 0;
para.T_e     = 300;
para.vthg    = 0.3000;
para.nd      = 6;