function [para] = modelcard_vs_v2_coos_bte_2

% 130nm cntfet
para.type    = 1;
para.geomod  = 2; % GAA

para.lch     = 1.3000e-07;
para.r_semi  = 0.85e-09;
para.w       = 1.0000e-06;
para.dens    = 1;         % tube density

para.cq      = 5.0564e-10;
para.tox     = 3.0000e-09;
para.eps_ox  = 16;

para.dvthq   = 0.0776;
para.fd      = 1;

para.meff    = 0.2;
para.lambda0 = 22.909e-09;
para.lfl     = 16.162e-09;
para.vinjb   = 1.1240e+05;

para.vth0    = 0.3326;
para.dibl    = 0;
para.nss     = 1.1;
para.va      = 2.6919;
para.gamma   = 0.8601;
para.alpha   = 3.5000;
para.beta    = 1.8000;

para.rsc     = 4.5111e+03; %Ohm/tube
para.rdc     = 4.5111e+03; 
para.rg      = 4.5;
para.rsf     = 0;
para.rdf     = 0;
para.cgspar  = 1e-17;
para.cgdpar  = 2e-17;
para.cdspar  = 2e-17;
para.cgspar2 = 0;
para.cgdpar2 = 0;

para.rmet    = 0;
para.t_e     = 300;
para.vthg    = 0.2;
para.nd      = 0;