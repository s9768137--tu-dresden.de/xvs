function [para] = modelcard_vs_v2_ingaas_200nm_zota

% 130nm cntfet
para.type    = 1;
para.geomod  = 3; % FINFET

para.w       = 14e-06;
para.u       = 33e-9;
para.dens    = 14;
para.lch     = 200e-9;

para.cq      = 26.9e-3;
para.tox     = 2.33e-9;
para.eps_ox  = 3.9;

para.dvthq   = 0.0500;
para.fd      = 0.1;

para.meff    = 0.041;
para.lambda0 = 22.4e-9;
para.lfl     = 77.9e-9;
para.vinjb   = 12.6e5;

para.vth0    = 0.17;
para.dibl    = 0;
para.nss     = 1.12;
para.va      = 3.5;
para.gamma   = 0.65;
para.alpha   = 3.5000;
para.beta    = 1.2;

para.rsc     = 180; %Ohm per FIN
para.rdc     = 180;  %Ohm per FIN
para.rg      = 0.0600;
para.rsf     = 0;
para.rdf     = 0;
para.cgspar  = 5.0381e-16;
para.cgdpar  = 3.3929e-16;
para.cdspar  = 6.3259e-16;
para.cgspar2 = 0;
para.cgdpar2 = 0;

% not needed
para.rmet    = 0;
para.t_e     = 300;
para.vthg    = -2;
para.nd      = 0;
para.rsc     = 180; %Ohm per FIN
para.rdc     = 180;  %Ohm per FIN
para.cq      = 26.9e-3;
para.r_semi  = 0;
