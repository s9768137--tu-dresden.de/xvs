function [para] = modelcard_vs_v2_si_130nm_infineon

% 130nm cntfet
para.type    = 1;
para.geomod  = 0; % Bulk

para.w       = 8.4000e-05;
para.lch     = 1.2000e-07;
%para.r_semi  = 0.80e-09;   % replace with diameter
%para.dens    = 0;         % tube density

para.cq      = 0.0035;
para.tox     = 3.1500e-09;
para.eps_ox  = 3.9;

para.dvthq   = 0.0500;
para.fd      = 0;

para.meff    = 0.0239;
para.lambda0 = 5.5176e-08;
para.lfl     = 3.3134e-09;
para.vinjb   = 3.48e5;

para.vth0    = 0.6300;
para.dibl    = 0.0680;
para.nss     = 1.4603;
para.va      = 50;
para.gamma   = 0.4000;
para.alpha   = 3.5000;
para.beta    = 1.7902;

para.rsc     = 180.0037; %Ohm*um
para.rdc     = 180.0037; 
para.rg      = 0.0600;
para.rsf     = 0;
para.rdf     = 0;
para.cgspar  = 5.0381e-16;
para.cgdpar  = 3.3929e-16;
para.cdspar  = 6.3259e-16;
para.cgspar2 = 0;
para.cgdpar2 = 0;

para.rmet    = 0;
para.t_e     = 300;
para.vthg    = -2;
para.nd      = 0;