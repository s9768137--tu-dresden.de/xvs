function [para] = modelcard_vs_v3_cntfet_digital2

% 7nm cntfet 0.75nm spacer
para.type    = 1;
para.geomod  = 2; % coaxial

para.w       = 1.0000e-06;
para.lch     = 7.0000e-09;
para.r_semi  = 0.5e-09;   % replace with diameter
para.dens    = 1;         % tube density

para.cq      = 4.0545e-10;%1.5521e-10; % F/m
para.tox     = 0.8e-09;
para.eps_ox  = 16;

para.dvthq   = 0.0741;
para.fd      = 0.9932;

para.meff    = 0.0490;
para.lambda0 = 4.6167e-07;
para.lfl     = 5.0428e-09;
para.vinjb   = 4.7842e+05;

para.vth0    = 0.2516;
para.dibl    = 0.0;
para.nss     = 1;
para.va      = 10;
para.gamma   = 0; %=0 in DA
para.alpha   = 3.5000;
para.beta    = 1.8000;
para.beta_ch = 1.8000;

para.rsc     = 3.0162e+03; %Ohm*um
para.rdc     = 3.0162e+03; 
para.rg      = 10/3;  % unbekannt
para.rsf     = 1;
para.rdf     = 1;
para.cgspar  = 1.1000e-21;
para.cgdpar  = 5.5000e-20;
para.cdspar  = 5.5000e-20;
para.cgspar2 = 0;
para.cgdpar2 = 0;

para.rmet    = 0;
para.t_e     = 300;
para.vthg    = 0.3000;
para.nd      = 0;
para.qcl=0;