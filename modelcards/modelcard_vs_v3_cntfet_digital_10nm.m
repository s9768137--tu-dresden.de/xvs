function [para] = modelcard_vs_v3_cntfet_digital_10nm

% 10nm cntfet
para.type    = 1;
para.geomod  = 2; % GAA nw

para.w       = 1.0000e-06;
para.lch     = 10.0000e-09;
para.r_semi  = 0.5e-09;   % replace with diameter
para.dens    = 1;         % tube density

para.cq      = 4.021893103448230e-10; %1.5521e-10; % F/m
para.tox     = 0.8e-09;
para.eps_ox  = 16;

para.dvthq   = 0.066806923129860;
para.fd      = 0.980632341570279;

para.meff    = 0.0490;
para.lambda0 = 9.052784595064005e-08;
para.lfl     = 8.143790743159539e-09;
para.vinjb   = 4.915457716737121e+05;

para.vth0    = 0.260000000000000;
para.dibl    = 0.01;
para.nss     = 1.00;
para.va      = 0;
para.gamma   = 0; %=0 in DA
para.alpha   = 3.5000;
para.beta    = 2.366724312275404;
para.beta_ch = 2.376455590955578;

para.rsc     = 3.951391735165353e+03; %Ohm*um
para.rdc     = 3.951391735165353e+03; 
para.rg      = 10/3;  % unbekannt
para.rsf     = 1;
para.rdf     = 1;
para.cgspar  = 1.1000e-21;
para.cgdpar  = 5.5000e-20;
para.cdspar  = 5.5000e-20;
para.cgspar2 = 0;
para.cgdpar2 = 0;

para.rmet    = 0;
para.t_e     = 300;
para.vthg    = 0.3000;
para.nd      = 0;
