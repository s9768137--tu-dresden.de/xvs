function [para] = modelcard_vs_v3_sinw_n_LiYeo09

% 10nm cntfet
para.type    = 1;
para.geomod  = 2; % GAA nw

para.w       = 1.0000e-06;
para.lch     = 10.0000e-09;
para.r_semi  = 8e-09;   % replace with diameter
para.dens    = 1;   % tube density

para.cq      = 8.1347e-10;%1.5521e-10; % F/m
para.tox     = 2.5e-09;
para.eps_ox  = 5;

para.dvthq   = 0.1000;
para.fd      = 0.5000;

para.meff    = 0.0490;
para.lambda0 = 1.5994e-09;
para.lfl     = 3.5409e-10;
para.vinjb   = 1.6609e+05;

para.vth0    = 0.4506;
para.dibl    = 0.090;
para.nss     = 1.5;
para.va      = 0;
para.gamma   = 0; 
para.alpha   = 3.5000;
para.beta    = 1.8000;

para.rsc     = 4.997e+03; %Ohm*um
para.rdc     = 4.997e+03; 
para.rg      = 50/3;  % unbekannt
para.rsf     = 1;
para.rdf     = 1;
para.cgspar  = 1.1000e-21;
para.cgdpar  = 5.5000e-20;
para.cdspar  = 5.5000e-20;
para.cgspar2 = 0;
para.cgdpar2 = 0;

para.rmet    = 0;
para.t_e     = 300;
para.vthg    = 0.3000;
para.nd      = 0;
para.qcl=0;