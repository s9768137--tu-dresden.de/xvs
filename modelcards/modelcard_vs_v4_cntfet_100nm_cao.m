function [para] = modelcard_vs_v4_cntfet_100nm_cao

% 130nm cntfet
para.type    = 1;
para.geomod  = 1; % top gate nw

para.w       = 20e-06;
para.lch     = 100e-09;
para.dens    = 40;         % tube density
para.r_semi  = 0.75e-09;   % replace with diameter
para.tox     = 3e-09;
para.eps_ox  = 9;

para.cq_t    = 1.0938e-10;

para.vth0    = 0.2209;
para.nss     = 3.88;
para.alpha   = 3.5000;
para.dibl    = 0.288;%560;
para.gamma   = 0.4533;

para.va      = 0;
para.beta    = 1.8628;
para.pqcl    = 0;
para.vball   = 3.3099e+05;
para.lambda0 = 2.8643e-08;
para.lfl     = 2.3579e-08;
para.vthd    = 0.2994;
para.nd      = 5.1649;

para.fb      = 0.1;
para.meff    = 0.0481;


para.rsc_t   = 3.1305e+04; %Ohm/tube
para.rdc_t   = 3.1305e+04; 
para.rg      = 140;
para.rsf     = 0;
para.rdf     = 0;
para.cgspar  = 0.11e-15; % in F/um
para.cgdpar  = 0.11e-15;
para.cdspar  = 2e-17;
para.cgspar2 = 0;
para.cgdpar2 = 0;

para.rmet    = 0;
para.rsc     = 0; 
para.rdc     = 0; 