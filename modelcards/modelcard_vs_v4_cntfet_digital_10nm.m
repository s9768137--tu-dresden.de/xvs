function [para] = modelcard_vs_v4_cntfet_digital_10nm

% 10nm cntfet
para.type    = 1;
para.geomod  = 2; % GAA nw

para.w       = 5e-9;
para.lch     = 10.0000e-09;
para.r_semi  = 0.5e-09;   % replace with diameter
para.dens    = 200;         % tube density

para.cq_t    = 6.3199e-10; 
para.tox     = 0.8e-09;
para.eps_ox  = 16;

para.fb      = 1;

para.meff    = 0.0490;
para.lambda0 = 5.8812e-08;
para.lfl     = 6.9788e-09;
para.vball   = 4.7463e+05;

para.vth0    = 0.265;
para.dibl    = 0.0185;
para.nss     = 1.00;
para.va      = 0;
para.gamma   = 4; 
para.alpha   = 3.5000;
para.beta    = 2.3073;
para.beta_ch = 3;
para.vthd    = 0.3000;
para.nd      = 0;
para.dvthq   = 0.0426;

para.rsc_t   = 3.4009e+03;
para.rdc_t   = 3.4009e+03; 
para.rg      = 10/3;  
para.rsf     = 1;
para.rdf     = 1;
para.cgspar  = 0.895e-15;
para.cgdpar  = 0.895e-15;
para.cdspar  = 0.73e-21;
para.cgspar2 = 0;
para.cgdpar2 = 0;

para.rmet  = 0;
para.rsc   = 0;
para.rdc   = 0; 
