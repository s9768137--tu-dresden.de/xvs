function [para] = modelcard_vs_v4_cntfet_digital_10nm

% 10nm cntfet
para.type    = 1;
para.geomod  = 2; % GAA nw

para.w       = 1.0000e-06;
para.lch     = 10.0000e-09;
para.r_semi  = 0.5e-09;   % replace with diameter
para.dens    = 1;         % tube density

para.cq_t    = 9.5187e-10; %1.5521e-10; % F/m
para.tox     = 0.8e-09;
para.eps_ox  = 16;

para.fb      = 0.8800;

para.meff    = 0.03;
para.lambda0 = 1.2159e-08;
para.lfl     = 9.9993e-09;
para.vball   = 7.6263e+05;

para.vth0    = 0.260000000000000;
para.dibl    = 0.0389;
para.nss     = 1.00;
para.va      = 0;
para.gamma   = 3; %=0 in DA
para.alpha   = 3.5000;
para.beta    = 2.9991;
para.beta_ch = 7;
para.vthd    = 0.3000;
para.nd      = 0;

para.rsc_t   = 3941.69309225322; %Ohm*um
para.rdc_t   = 3941.69309225322; 
para.rg      = 10/3;  % unbekannt
para.rsf     = 1;
para.rdf     = 1;
para.cgspar  = 1.1000e-21;
para.cgdpar  = 5.5000e-20;
para.cdspar  = 5.5000e-20;
para.cgspar2 = 0;
para.cgdpar2 = 0;
para.pqcl    =0;
para.rmet    = 0;
para.rsc   = 3.951391735165353e+03; %Ohm*um
para.rdc   = 3.951391735165353e+03; 
