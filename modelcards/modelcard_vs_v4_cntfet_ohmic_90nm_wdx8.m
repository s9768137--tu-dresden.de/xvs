function [para] = modelcard_vs_v4_cntfet_ohmic_90nm_wdx8

% 
para.type    = 1;
para.geomod  = 1; % TG nw

para.w       = 13.6e-9;
para.lch     = 70.0000e-09;
para.r_semi  = 0.85e-09;   % replace with diameter
para.dens    = 73.5294;         % tube density

para.cq_t    = 3.9124e-10; 
para.tox     = 2e-09;
para.eps_ox  = 3.9;

para.fb      = 1;

para.meff    = 0.0490;
para.lambda0 = 2.9279e-07;
para.lfl     = 70e-09;
para.vball   = 2.8428e+05;

para.vth0    = 0.3189;
para.dibl    = 4.2698e-13;
para.nss     = 1.0000;
para.va      = 0;
para.gamma   = 0; 
para.alpha   = 3.8111;
para.beta    = 1.3464;
para.beta_ch = 3;
para.vthd    = 0.3000;
para.nd      = 0;
para.dvthq   = 0.0;

para.rsc_t   = 3.3e+03;
para.rdc_t   = 3.3e+03; 
para.rg      = 10/3;  
para.rsf     = 1;
para.rdf     = 1;
para.cgspar  = 0.895e-9;
para.cgdpar  = 0.895e-9;
para.cdspar  = 0.73e-15;
para.cgspar2 = 0;
para.cgdpar2 = 0;

para.rmet  = 0;
para.rsc   = 0;
para.rdc   = 0; 
