function [para] = modelcard_vs_v4_cntfet_reconfig_digital_20nm

% 10nm cntfet
para.type    = 1;
para.geomod  = 2; % GAA nw

para.w       = 5e-9;
para.lch     = 20.0000e-09;
para.r_semi  = 0.5e-09;   % replace with diameter
para.dens    = 200;         % tube density

para.cq_t    = 7.1595e-10; 
para.tox     = 0.8e-09;
para.eps_ox  = 16;

para.fb      = 1;

para.meff    = 0.0490;
para.lambda0 = 1.6774e-08;
para.lfl     = 4.5622e-09;
para.vball   = 1.0148e+05;

para.vth0    =  0.2896;
para.dibl    = 0.0;
para.nss     = 1.0493;
para.va      = 0.2262;
para.gamma   = 2.8631; 
para.alpha   = 3.5000;
para.beta    = 2.3200;
para.beta_ch = 1.4378;
para.vthd    = 0.3000;
para.nd      = 0;
para.dvthq   = 0.1000;

para.rsc_t   = 1.1166e+04;
para.rdc_t   = 1.1166e+04; 
para.rg      = 10/3;  
para.rsf     = 1;
para.rdf     = 1;
para.cgspar  = 1.1000e-21;
para.cgdpar  = 5.5000e-20;
para.cdspar  = 5.5000e-20;
para.cgspar2 = 0;
para.cgdpar2 = 0;

para.rmet  = 0;
para.rsc   = 0;
para.rdc   = 0; 
