function [para] = modelcard_vs_v4_cntfet_reconfig_digital_20nm2

% 10nm cntfet
para.type    = 1;
para.geomod  = 2; % GAA nw

para.w       = 5e-9;
para.lch     = 20.0000e-09;
para.r_semi  = 0.5e-09;   % replace with diameter
para.dens    = 200;         % tube density

para.cq_t    = 6.0691e-10; 
para.tox     = 0.8e-09;
para.eps_ox  = 16;

para.fb      = 1;

para.meff    = 0.0490;
para.lambda0 = 2.2225e-08;
para.lfl     = 4.5116e-09;
para.vball   = 1.0862e+05;

para.vth0    = 0.2907;
para.dibl    = 0.0;
para.nss     = 1.0486;
para.va      = 0.1927;
para.gamma   = 2.9090; 
para.alpha   = 3.5000;
para.beta    = 2.4302;
para.beta_ch = 1.5730;
para.vthd    = 0.3000;
para.nd      = 0;
para.dvthq   = 0.1190;

para.rsc_t   = 1.2409e+04;
para.rdc_t   = 1.2409e+04; 
para.rg      = 10/3;  
para.rsf     = 1;
para.rdf     = 1;
para.cgspar  = 0.895e-9;
para.cgdpar  = 0.895e-9;
para.cdspar  = 0.73e-15;
para.cgspar2 = 0;
para.cgdpar2 = 0;

para.rmet  = 0;
para.rsc   = 0;
para.rdc   = 0; 
