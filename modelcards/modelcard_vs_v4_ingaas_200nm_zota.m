function [para] = modelcard_vs_v4_ingaas_200nm_zota

% 130nm cntfet
para.type    = 1;
para.geomod  = 3; % FINFET

para.w       = 14e-06;
para.u       = 33e-9;
para.dens    = 14.3;
para.lch     = 200e-9;

para.cq_t    = 3.0155e-09;
para.tox     = 2.33e-9;
para.eps_ox  = 3.9;

para.dvthq   = 0.00;
para.fb      = 1;

para.meff    = 0.041;
para.lambda0 = 3.0022e-08;
para.lfl     = 2.0222e-08;
para.vball   = 3.6789e+05;

para.vth0    = 0.1627;
para.dibl    =  0.0018;
para.nss     =  1.3303;
para.va      = 2.0349;
para.gamma   = 0.7302;
para.alpha   = 3.5000;
para.beta    = 2.3;

para.rsc_t   = 2.8736e+03; %Ohm per FIN
para.rdc_t   = 2.8736e+03;  %Ohm per FIN
para.rg      = 100;
para.rsf     = 0;
para.rdf     = 0;
para.cgspar  = 5.0381e-16;
para.cgdpar  = 3.3929e-16;
para.cdspar  = 6.3259e-16;
para.cgspar2 = 0;
para.cgdpar2 = 0;

% not needed
para.rmet    = 0;
para.vthd    = -2;
para.nd      = 0;
para.rsc     = 180; %Ohm per FIN
para.rdc     = 180;  %Ohm per FIN
para.cq      = 26.9e-3;
para.r_semi  = 0;
