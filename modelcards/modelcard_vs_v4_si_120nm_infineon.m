function [para] = modelcard_vs_v4_si_120nm_infineon

para.type    = 1;
para.geomod  = 0;

para.w       = 8.4000e-05;
para.lch     = 1.2000e-07;

para.cq      = 0.0035;
para.tox     = 3.1500e-09;
para.eps_ox  = 3.9;

para.dvthq   = 0.0500;
para.fb      = 1;

para.meff    = 0.0239;
para.lambda0 = 5.5176e-08;
para.lfl     = 3.3134e-09;
para.vball   = 3.48e5;

para.vth0    = 0.6300;
para.dibl    = 0.0680;
para.nss     = 1.4603;
para.va      = 24.8;
para.gamma   = 0.4000;
para.alpha   = 3.5000;
para.beta    = 1.7902;

para.rsc     = 180.0037; %Ohm*um
para.rdc     = 180.0037; 
para.rg      = 10;
para.rsf     = 0;
para.rdf     = 0;
para.cgspar  = 5.0381e-16;
para.cgdpar  = 3.3929e-16;
para.cdspar  = 6.3259e-16;
para.cgspar2 = 0;
para.cgdpar2 = 0;

% not needed
para.rmet    = 0;
para.vthd    = -2;
para.nd      = 0;
para.rsc_t   = 0;
para.rdc_t   = 0;